INSERT INTO Expense VALUES (100);
INSERT INTO Expense VALUES (200);

INSERT INTO SystemNotification VALUES ('Planowane wylaczenie systemu','2022-05-26 19:37','2022-06-03 19:37','Prace serwisowe','info');
INSERT INTO SystemNotification VALUES ('Long term issue','2022-05-26 19:37','2022-05-27 19:37','Long term','info');
INSERT INTO SystemNotification VALUES ('current date issue','2022-04-25 19:37','2022-12-25 19:37','current date','info');
INSERT INTO SystemNotification VALUES ('future date issue','2022-09-27 19:37','2022-12-28 19:37','future date','info');
INSERT INTO SystemNotification VALUES ('Passed date issue','2022-03-27 19:37','2022-04-28 19:37','passed date','info');

INSERT INTO Flag VALUES ('https://www.youtube.com/watch?v=dQw4w9WgXcQ');
INSERT INTO Flag VALUES ('https://www.youtube.com/watch?v=dQw4w9WgXcQ');
INSERT INTO Flag VALUES ('https://www.youtube.com/watch?v=dQw4w9WgXcQ');
INSERT INTO Flag VALUES ('https://www.youtube.com/watch?v=dQw4w9WgXcQ');

INSERT INTO ServicePhone VALUES (0123456789);
INSERT INTO ServicePhone VALUES (1234567890);
INSERT INTO ServicePhone VALUES (234567890);
INSERT INTO ServicePhone VALUES (345678901);

INSERT INTO Country VALUES ('AF','Afghanistan','AFG',1,1);
INSERT INTO Country VALUES ('AL','Albania','LEK',2,2);
INSERT INTO Country VALUES ('DZ','Algeria','DA',3,4);
INSERT INTO Country VALUES ('AS','American Samoa','USD',4,3);

INSERT INTO Alert VALUES ('Nie czytaj tego','2022-05-26 20:11','2022-12-29 00:00:00.000',1);
INSERT INTO Alert VALUES ('Tu serio nic nie ma','2022-05-26 20:11','2022-12-29 00:00:00.000',2);
INSERT INTO Alert VALUES ('A Ty dalej swoje','2022-05-26 20:11','2022-12-29 00:00:00.000',3);
INSERT INTO Alert VALUES ('Uparciuch','2022-05-26 20:11','2022-12-29 00:00:00.000',2);
INSERT INTO Alert VALUES ('A teraz bardzo dlugi tekst: diouwfuwrhfuhwefuhweuifhweufhuwehfuiehfuiehfuwehfu','2022-05-26 20:11','2022-12-29 00:00:00.000',3);

INSERT INTO "User" VALUES ('Adam','Testowy','123456789','adam.testowy@gmail.com','someHash','2022-05-22 17:36','2022-05-22 17:36');

INSERT INTO OweSinglePayment VALUES ('Andrzej',100,1,'2022-06-28 21:37',1,1);
INSERT INTO OweSinglePayment VALUES ('Adam',50,1,'2022-06-28 21:37',1,2);
INSERT INTO OweSinglePayment VALUES ('Ada',50,1,'2022-06-28 21:37',1,2);

INSERT INTO Travel VALUES ('Trip to Andorra','Andorra','2022-06-28','2022-07-28',NULL,'Fajnie bedzie',1,1000,NULL,NULL,'2022-05-22 18:03',1,2,'Not Null Hotel');

INSERT INTO VisitDate VALUES ('2022-06-28 21:15:00','Dzien 1',1);

INSERT INTO Spot VALUES ('Wystawa aut',0,'strase 1','27','12','02-654',1,1);
INSERT INTO Spot VALUES ('Wystawa obrazow',0,'strase 2','18','12','02-654',1,2);