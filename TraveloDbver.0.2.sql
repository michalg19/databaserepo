CREATE DATABASE TraveloDb 

use TraveloDb
go

CREATE TABLE "User"(
	UserId INT IDENTITY (1,1) NOT NULL ,
	"Name" VARCHAR(30) NOT NULL,
	Surname VARCHAR(60) NOT NULL,
	PhoneNumber VARCHAR(14),
	Email VARCHAR(30) NOT NULL,
	Password VARCHAR(64) NOT NULL,
	PasswordDateUpdated DateTime NOT NULL,
	DateCreated DateTime NOT NULL,
	LastLogin DateTime,
	CONSTRAINT PK_User_UserId PRIMARY KEY CLUSTERED (UserId),
	CONSTRAINT NAME_CHECK CHECK (Name NOT LIKE '%[^A-Z]%'),
	CONSTRAINT SURNAME_CHECK CHECK (Surname NOT LIKE '%[^A-Z]%'),
	CONSTRAINT EMAIL_CKECH CHECK (Email LIKE '%__@__%.__%'),
	CONSTRAINT Unique_User_Email Unique(Email),
	CONSTRAINT Unique_User_Password Unique(Password),
	CONSTRAINT PHONE_CKECH CHECK (PhoneNumber NOT LIKE '%[^0-9]%')
);

use TraveloDb
go


Create Procedure Registration
@Name as Varchar(30),
@Surname as Varchar(60),
@PhoneNumber as Varchar(14),
@Email as Varchar(30),
@Password as Varchar(64)
AS
BEGIN
Set NOCOUNT ON;
DECLARE @actDate DateTime;
set @actDate = Convert(datetime, GETDATE())
Insert into "User"(Name,Surname,PhoneNumber,Email,Password,PasswordDateUpdated,DateCreated)
values(@Name,@Surname,@PhoneNumber,@Email,@Password,@actDate,@actDate)
END;


go

Create Procedure ChangePassword
@Email as Varchar(30),
@Password as Varchar(64),
@NEW_Password as Varchar(64)
as
begin
Set NOCOUNT ON;
DECLARE @actDate DateTime;
set @actDate = Convert(datetime, GETDATE()) 
Update "User"
set PasswordDateUpdated = @actDate, Password = @NEW_Password
Where Email = @Email And Password = @Password
end;

go

Create Procedure LoginUser
@Email as Varchar(30),
@Password as Varchar(64)
as
begin
Set NOCOUNT ON;
Select Email, Password from "User"
Where Email = @Email And Password = @Password
end;
