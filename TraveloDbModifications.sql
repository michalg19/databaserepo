use TraveloDb;
go

ALTER TABLE Spots
DROP CONSTRAINT FK_Places_Spots
GO

DROP TABLE Places;

ALTER TABLE OweSinglePayments
DROP CONSTRAINT FK_Expenses_OweSinglePayments
GO

ALTER TABLE Spots
DROP CONSTRAINT FK_Expenses_Spots
GO

ALTER TABLE Spots
DROP CONSTRAINT FK_VisitDates_Spots
GO

ALTER TABLE VisitDates
DROP CONSTRAINT FK_Travels_VisitDates
GO

ALTER TABLE Travels
DROP CONSTRAINT FK_Users_Travels
GO

ALTER TABLE Travels
DROP CONSTRAINT FK_Countries_Travels
GO

ALTER TABLE CountriesAlerts
DROP CONSTRAINT FK_Countries_CountriesAlerts
GO

ALTER TABLE CountriesAlerts
DROP CONSTRAINT FK_Alerts_CountriesAlerts
GO

ALTER TABLE DictionaryWords
DROP CONSTRAINT FK_Dictionaries_DictionaryWords
GO

DROP TABLE CountriesAlerts;

EXEC sp_rename 'OweSinglePayments','OweSinglePayment';

EXEC sp_rename 'Expenses','Expense';

EXEC sp_rename 'Spots','Spot';

EXEC sp_rename 'VisitDates','VisitDate';

EXEC sp_rename 'Travels','Travel';

EXEC sp_rename 'Users','User';

EXEC sp_rename 'Dictionaries','Dictionary';

EXEC sp_rename 'DictionaryWords','DictionaryWord';

EXEC sp_rename 'SystemNotifications','SystemNotification';

EXEC sp_rename 'Countries','Country';

EXEC sp_rename 'Alerts','Alert';

ALTER TABLE OweSinglePayment
DROP CONSTRAINT PK_OweSinglePayments
GO

ALTER TABLE OweSinglePayment
ALTER COLUMN ExpenseId INT NOT NULL;

ALTER TABLE OweSinglePayment
ALTER COLUMN PaymentDate DateTime NULL;

ALTER TABLE OweSinglePayment ADD CONSTRAINT
PK_OweSinglePayment PRIMARY KEY CLUSTERED (OweSinglePaymentId)

ALTER TABLE OweSinglePayment
DROP CONSTRAINT PERSONNAME_CHECK
GO

ALTER TABLE OweSinglePayment ADD CONSTRAINT
PERSONNAME_CHECK CHECK (PersonName NOT LIKE '%[0-9]%')

ALTER TABLE Expense
DROP CONSTRAINT PK_Expenses
GO

ALTER TABLE Expense
DROP Column PersonPaymentId
GO

ALTER TABLE Expense ADD CONSTRAINT
PK_Expense PRIMARY KEY CLUSTERED (ExpenseId)

ALTER TABLE Spot
DROP CONSTRAINT PK_Spots
GO

ALTER TABLE Spot
DROP CONSTRAINT ZIPCODE_CHECK
GO

ALTER TABLE Spot
ALTER COLUMN ExpenseId INT NOT NULL;

ALTER TABLE Spot
DROP Column PlaceId
GO

EXEC sp_rename 'Spot.TravelDateId', 'VisitDateId', 'COLUMN'

ALTER TABLE Spot ADD CONSTRAINT
PK_Spot PRIMARY KEY CLUSTERED (SpotId)

ALTER TABLE VisitDate
DROP CONSTRAINT PK_VisitDates
GO

ALTER TABLE VisitDate
ALTER COLUMN TravelId INT NOT NULL;

EXEC sp_rename 'VisitDate.TravelDateId', 'VisitDateId', 'COLUMN'

ALTER TABLE VisitDate ADD CONSTRAINT
PK_VisitDate PRIMARY KEY CLUSTERED (VisitDateId)

ALTER TABLE Travel
DROP CONSTRAINT PK_Travels
GO

EXEC sp_rename 'Travel.IdFav', 'IsFav', 'COLUMN'

ALTER TABLE Travel
ADD HotelName varchar(50) NOT NULL;

ALTER TABLE Travel
ALTER COLUMN UserId int NOT NULL;

ALTER TABLE Travel
ALTER COLUMN PlannedBudget decimal NOT NULL;

EXEC sp_rename 'Travel.ParticipatNumber', 'ParticipantNumber', 'COLUMN'

ALTER TABLE Travel ADD CONSTRAINT
PK_Travel PRIMARY KEY CLUSTERED (TravelId)

ALTER TABLE "User"
DROP CONSTRAINT PK_Users
GO

ALTER TABLE "User"
DROP CONSTRAINT SURNAME_CHECK
GO

ALTER TABLE "User"
DROP CONSTRAINT NAME_CHECK
GO

ALTER TABLE "User"
DROP COLUMN LastLogin
GO

ALTER TABLE "User" ADD CONSTRAINT
PK_User PRIMARY KEY CLUSTERED (UserId)

ALTER TABLE Dictionary
DROP CONSTRAINT PK_Dictionaries
GO

ALTER TABLE Dictionary
DROP CONSTRAINT DICTIONARY_NAME_CHECK
GO

ALTER TABLE Dictionary ADD CONSTRAINT
PK_Dictionary PRIMARY KEY CLUSTERED (DictionaryId)

ALTER TABLE DictionaryWord
DROP CONSTRAINT PK_DictionaryWords
GO

ALTER TABLE DictionaryWord
DROP CONSTRAINT WORD_CHECK
GO

ALTER TABLE DictionaryWord
DROP CONSTRAINT WORDTRANSLATED_CHECK
GO

ALTER TABLE DictionaryWord ADD CONSTRAINT
PK_DictionaryWord PRIMARY KEY CLUSTERED (DictionaryWordId)

ALTER TABLE Country
DROP CONSTRAINT PK_Countries
GO

ALTER TABLE Country
DROP CONSTRAINT CODE_CHECK
GO

ALTER TABLE Country
DROP CONSTRAINT COUNTRY_NAME_CHECK
GO

ALTER TABLE Country
DROP CONSTRAINT CURRENCY_CHECK
GO

ALTER TABLE Country
ADD FlagId int NOT NULL;

ALTER TABLE Country
ADD ServicePhoneId int NOT NULL;

ALTER TABLE Country ADD CONSTRAINT
PK_Country PRIMARY KEY CLUSTERED (CountryId)

ALTER TABLE Alert
DROP CONSTRAINT PK_Alerts
GO

ALTER TABLE Alert
ADD CountryId int NOT NULL;

ALTER TABLE Alert ADD CONSTRAINT
PK_Alert PRIMARY KEY CLUSTERED (AlertId)

ALTER TABLE SystemNotification
DROP CONSTRAINT PK_SystemNotifications
GO

ALTER TABLE SystemNotification ADD CONSTRAINT
PK_SystemNotification PRIMARY KEY CLUSTERED (SystemNotificationId)

ALTER TABLE Country WITH CHECK ADD CONSTRAINT FK_ServicePhone_Country FOREIGN KEY (ServicePhoneId)
REFERENCES ServicePhone (ServicePhoneId)

ALTER TABLE Country WITH CHECK ADD CONSTRAINT FK_Flag_Country FOREIGN KEY (FlagId)
REFERENCES Flag (FlagId)

ALTER TABLE Alert WITH CHECK ADD CONSTRAINT FK_Country_Alert FOREIGN KEY (CountryId)
REFERENCES Country (CountryId)

ALTER TABLE DictionaryWord WITH CHECK ADD CONSTRAINT FK_Dictionary_DictionaryWord FOREIGN KEY (DictionaryId)
REFERENCES Dictionary (DictionaryId)

ALTER TABLE Travel WITH CHECK ADD CONSTRAINT FK_User_Travel FOREIGN KEY (UserId)
REFERENCES "User" (UserId)

ALTER TABLE Travel WITH CHECK ADD CONSTRAINT FK_Country_Travel FOREIGN KEY (CountryId)
REFERENCES Country (CountryId)

ALTER TABLE VisitDate WITH CHECK ADD CONSTRAINT FK_Travel_VisitDate FOREIGN KEY (TravelId)
REFERENCES Travel (TravelId)

ALTER TABLE Spot WITH CHECK ADD CONSTRAINT FK_VisitDate_Spot FOREIGN KEY (VisitDateId)
REFERENCES VisitDate (VisitDateId)

ALTER TABLE Spot WITH CHECK ADD CONSTRAINT FK_Expense_Spot FOREIGN KEY (ExpenseId)
REFERENCES Expense (ExpenseId)

ALTER TABLE OweSinglePayment WITH CHECK ADD CONSTRAINT FK_Expense_OweSinglePayment FOREIGN KEY (ExpenseId)
REFERENCES Expense (ExpenseId)


drop procedure FindCountry
drop procedure CheckBudget
drop procedure RecoveryPasswordChange

use TraveloDb
go

Create Procedure FindCountry
@Phrase as Varchar(255)
AS
BEGIN
Set NOCOUNT ON;
DECLARE @arg Varchar(255)
set @arg = '%'+@Phrase+'%'
Select 'CountryId'=CountryId,'CodeABC'=CodeABC,'CodeAB'=CodeAB,'Name'=LOWER (Name), 'Currency'=Currency, 'FlagId'=FlagId, 'ServicePhoneId'=ServicePhoneId from Country
Where Name like LOWER(@arg)
END;

GO

use TraveloDb
go
Create Procedure CheckBudget
@TravelId INT,
@Result float output
AS
BEGIN
Set NOCOUNT ON;
DECLARE @CostSum float;
DECLARE @OwePaymentSum float;

CREATE TABLE #Expenses(
	ExpenseId int
	)

Insert into #Expenses(ExpenseId)
	select s.ExpenseId from Spot s where VisitDateId IN 
	(Select VisitDateId From VisitDate v where TravelId = @TravelId)

	Select @OwePaymentSum = SUM(PaymentAmount) from OweSinglePayment ow
	Where ExpenseId IN 
	(SELECT t.ExpenseId FROM #Expenses t)

Select @CostSum=SUM(Cost) from Expense e
Where e.ExpenseId IN(
Select s.ExpenseId from Spot s
Where  VisitDateId IN 
(Select VisitDateId From VisitDate vd 
Where vd.TravelId like @TravelId));

Select @Result = coalesce(@CostSum + @OwePaymentSum, @CostSum, @OwePaymentSum, 0);

Select 'Result'= @Result;

Return;
END;

GO

use TraveloDb
go

Create Procedure RecoveryPasswordChange
@Email as Varchar(30),
@NEWPassword as Varchar(64)
AS
BEGIN
Set NOCOUNT ON;
DECLARE @actDate DateTime;
set @actDate = Convert(datetime, GETDATE()) 
Update "User"
set PasswordDateUpdated = @actDate, Password = @NEWPassword
Where Email = @Email
END;

GO

use TraveloDb
go

Alter table DictionaryWord
drop constraint FK_Dictionary_DictionaryWord
go

Alter table DictionaryWord
drop column DictionaryId
go

Alter table Dictionary
drop column "Name"
go

Alter table Dictionary
add "Name" NVarchar(255) NOT NULL
go

Alter table DictionaryWord
drop column Word
go

Alter table DictionaryWord
add Word NVarchar(255) NOT NULL
go

Alter table DictionaryWord
drop column WordTranslated
go

Alter table DictionaryWord
add WordTranslated NVarchar(255) NOT NULL
go

Alter table DictionaryWord
add DictionaryId int NOT NULL
go


Alter table DictionaryWord WITH CHECK ADD CONSTRAINT FK_Dictionary_DictionaryWord FOREIGN KEY (DictionaryId)
REFERENCES Dictionary (DictionaryId) ON UPDATE NO ACTION ON DELETE NO ACTION

drop Table Country

CREATE TABLE Country(
	CountryId INT IDENTITY (1,1) NOT NULL,
	CodeABC VARCHAR(3) NOT NULL,
	CodeAB VARCHAR(2) NOT NULL,
	Name VARCHAR(30) NOT NULL,
	Currency VARCHAR(3) NOT NULL,
	FlagId INT NOT NULL,
	ServicePhoneId INT NOT NULL,
	CONSTRAINT COUNTRY_NAME_CHECK CHECK (Name NOT LIKE '%[^A-Z]%'),
	CONSTRAINT CODEABC_CHECK CHECK (CodeABC NOT LIKE '%[^A-Z]%'),
	CONSTRAINT CODEAB_CHECK CHECK (CodeAB NOT LIKE '%[^A-Z]%'),
	CONSTRAINT CURRENCY_CHECK CHECK (Currency NOT LIKE '%[^A-Z]%'),
	CONSTRAINT PK_Countries PRIMARY KEY CLUSTERED (CountryId)
);

drop procedure AddCountry

use TraveloDb
go
Create Procedure AddCountry
@CodeABC VARCHAR(3),
@CodeAB VARCHAR(2),
@Name VARCHAR(30),
@Currency VARCHAR(3),
@FlagId INT,
@ServicePhoneId INT
AS
Set NOCOUNT ON;

BEGIN TRY
	BEGIN TRAN;
	INSERT INTO Country(CodeABC,CodeAB,Name,Currency,FlagId,ServicePhoneId)
	VALUES (@CodeABC,@CodeAB,@Name,@Currency,@FlagId,@ServicePhoneId);
	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('Flag and Service Phone that are connected with Alert must exist');
END CATCH;

GO

Alter Table Travel
drop constraint FK_Country_Travel

Alter Table Travel
drop constraint FK_User_Travel

Alter Table VisitDate
drop constraint FK_Travel_VisitDate

truncate Table Travel

Alter Table Travel
add PickedCurrency Varchar(3) NOT NULL

ALTER TABLE Travel
drop column IsFav

Alter Table Travel
add HotelStreet VARCHAR(255) NOT NULL

Alter Table Travel
add HotelBuildingNo int NOT NULL

Alter Table Travel
add HotelFlatNo int NOT NULL

Alter Table Travel
add HotelZipCode VARCHAR(255) NOT NULL

Alter Table Travel
add HotelCity VARCHAR(255) NOT NULL

ALTER Table Travel
DROP COLUMN ParticipantNumber

ALTER Table Travel
DROP COLUMN LinkUrl

ALTER Table Travel
DROP COLUMN LinkExpirationDate

DROP Procedure AddTravel

use TraveloDb
go
Create Procedure AddTravel
@Name as Varchar(30),
@Destination as Varchar(255),
@StartDate as Date,
@EndDate as Date,
@Note as Varchar(100),
@PlannedBudget as decimal,
@UserId as int,
@CountryId as int,
@HotelName as Varchar(50),
@PickedCurrency as Varchar(3),
@HotelStreet Varchar(255),
@HotelBuildingNo int,
@HotelFlatNo int,
@HotelZipCode Varchar(255),
@HotelCity Varchar(255)
AS
Set NOCOUNT ON;
BEGIN TRY
	BEGIN TRAN;
	DECLARE @actDate DateTime;
	set @actDate = Convert(datetime, GETDATE())
	Insert into Travel(Name,Destination,StartDate,EndDate,Note,PlannedBudget,CreatedDate,UserId,CountryId,HotelName,PickedCurrency,HotelStreet,HotelBuildingNo,HotelFlatNo,HotelZipCode,HotelCity)
	values(@Name,@Destination,@StartDate,@EndDate,@Note,@PlannedBudget,@actDate,@UserId,@CountryId,@HotelName,@PickedCurrency,@HotelStreet,@HotelBuildingNo,@HotelFlatNo,@HotelZipCode,@HotelCity)
	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('User and Country connected with Travel must exist');
END CATCH;

GO

ALTER Table Travel
ALTER COLUMN 
HotelName VARCHAR(50) NULL

ALTER Table Travel
ALTER COLUMN 
HotelStreet VARCHAR(255) NULL

ALTER Table Travel
ALTER COLUMN 
HotelBuildingNo INT NULL

ALTER Table Travel
ALTER COLUMN 
HotelFlatNo INT NULL

ALTER Table Travel
ALTER COLUMN 
HotelZipCode VARCHAR(255) NULL

ALTER Table Travel
ALTER COLUMN 
HotelCity VARCHAR(255) NULL


ALTER TABLE Travel WITH CHECK ADD CONSTRAINT FK_User_Travel FOREIGN KEY (UserId)
REFERENCES "User" (UserId)

ALTER TABLE Travel WITH CHECK ADD CONSTRAINT FK_Country_Travel FOREIGN KEY (CountryId)
REFERENCES Country (CountryId)

ALTER TABLE VisitDate WITH CHECK ADD CONSTRAINT FK_Travel_VisitDate FOREIGN KEY (TravelId)
REFERENCES Travel (TravelId)

ALTER Table VisitDate
ALTER COLUMN
Title VARCHAR(30) NULL

ALTER TABLE Spot
drop constraint FK_Expense_Spot

ALTER TABLE Spot
drop constraint FK_VisitDate_Spot

truncate table Spot

ALTER TABLE Spot
ADD CoordinateX DECIMAL(10,7) NOT NULL

ALTER TABLE Spot
ADD CoordinateY DECIMAL(10,7) NOT NULL

ALTER TABLE Spot
ADD "Name" VARCHAR(255) NOT NULL

ALTER Table Spot
ALTER COLUMN 
"Order" INT NOT NULL

ALTER Table Spot
ALTER COLUMN 
Street VARCHAR(30) NULL

ALTER Table Spot
ALTER COLUMN 
BuildingNo VARCHAR(10) NULL

ALTER Table Spot
ALTER COLUMN 
ZipCode VARCHAR(10) NULL

ALTER Table Spot
ALTER COLUMN 
CoordinateX DECIMAL(10,7) NULL

ALTER Table Spot
ALTER COLUMN 
CoordinateY DECIMAL(10,7) NULL

ALTER Table Spot
ALTER COLUMN 
"Name" VARCHAR(255) NULL

EXEC sp_rename 'Spot.Street', 'Adress', 'COLUMN'

ALTER Table Spot
ALTER COLUMN 
Adress Varchar(255) NULL

ALTER Table Spot
drop Column BuildingNo

ALTER Table Spot
drop Column FlatNo

ALTER Table Spot
drop Column ZipCode

drop procedure AddSpot

use TraveloDb
go
Create Procedure AddSpot
@Note VARCHAR(255),
@Order INT,
@Adress VARCHAR(255),
@VisitDateId INT,
@ExpenseId INT,
@CoordinateX DECIMAL(10,7),
@CoordinateY DECIMAL(10,7),
@Name VARCHAR(255)
AS
Set NOCOUNT ON;
BEGIN TRY
	BEGIN TRAN;
	Insert into Spot(Note,"Order",Adress,VisitDateId,ExpenseId,CoordinateX,CoordinateY,"Name")
	values(@Note,@Order,@Adress,@VisitDateId,@ExpenseId,@CoordinateX,@CoordinateY,@Name)
	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('VisitDate and Expense connected with Spot must exist');
END CATCH;

GO

ALTER TABLE Spot WITH CHECK ADD CONSTRAINT FK_VisitDate_Spot FOREIGN KEY (VisitDateId)
REFERENCES VisitDate (VisitDateId)

ALTER TABLE Spot WITH CHECK ADD CONSTRAINT FK_Expense_Spot FOREIGN KEY (ExpenseId)
REFERENCES Expense (ExpenseId)

ALTER Table OweSinglePayment
ALTER COLUMN 
PaymentDate DATETIME NOT NULL

use TraveloDb
go

Alter Table "User"
Alter Column "Password" Varchar(1024) NOT NULL

select * from "User"

use TraveloDb
go
Alter Procedure RecoveryPasswordChange
@Email as Varchar(30),
@NEWPassword as Varchar(1024)
AS
BEGIN
Set NOCOUNT ON;
DECLARE @actDate DateTime;
set @actDate = Convert(datetime, GETDATE()) 
Update "User"
set PasswordDateUpdated = @actDate, Password = @NEWPassword
Where Email = @Email
END;

GO


ALTER TABLE VisitDate
drop Constraint FK_Travel_VisitDate

ALTER TABLE Spot
drop Constraint FK_VisitDate_Spot

ALTER TABLE Spot
drop Constraint FK_Expense_Spot

ALTER TABLE OweSinglePayment
drop Constraint FK_Expense_OweSinglePayment

ALTER TABLE VisitDate WITH CHECK ADD CONSTRAINT FK_Travel_VisitDate FOREIGN KEY (TravelId)
REFERENCES Travel (TravelId) ON UPDATE CASCADE ON DELETE CASCADE

ALTER TABLE Spot WITH CHECK ADD CONSTRAINT FK_VisitDate_Spot FOREIGN KEY (VisitDateId)
REFERENCES VisitDate (VisitDateId) ON UPDATE CASCADE ON DELETE CASCADE

ALTER TABLE Spot WITH CHECK ADD CONSTRAINT FK_Expense_Spot FOREIGN KEY (ExpenseId)
REFERENCES Expense (ExpenseId) ON UPDATE CASCADE ON DELETE CASCADE

ALTER TABLE OweSinglePayment WITH CHECK ADD CONSTRAINT FK_Expense_OweSinglePayment FOREIGN KEY (ExpenseId)
REFERENCES Expense (ExpenseId) ON UPDATE CASCADE ON DELETE CASCADE


Use TraveloDb
go
Create Procedure RemoveTravel
@TravelId INT
AS
Set NOCOUNT ON;
BEGIN TRY
	BEGIN TRAN;

	CREATE TABLE #TempExpenses(
	ExpenseId int
	)

	Insert into #TempExpenses(ExpenseId)
	select s.ExpenseId from Spot s where VisitDateId IN 
	(Select VisitDateId From VisitDate v where TravelId = 1)

	DELETE FROM Travel WHERE TravelId=@TravelId;
	DELETE FROM Expense WHERE ExpenseId IN
	(SELECT t.ExpenseId FROM #TempExpenses t);

	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('Could not delete all travel info');
END CATCH;

GO



GRANT INSERT, UPDATE, DELETE ON TraveloDb.dbo.Expense TO api_user;
GRANT INSERT, UPDATE, DELETE ON TraveloDb.dbo.OweSinglePayment TO api_user;
GRANT INSERT, UPDATE, DELETE ON TraveloDb.dbo.Spot TO api_user;
GRANT INSERT, UPDATE, DELETE ON TraveloDb.dbo.Travel TO api_user;
GRANT INSERT, UPDATE, DELETE ON TraveloDb.dbo."User" TO api_user;
GRANT INSERT, UPDATE, DELETE ON TraveloDb.dbo.VisitDate TO api_user;

GRANT SELECT ON TraveloDb.dbo.Dictionary TO api_user;
GRANT SELECT ON TraveloDb.dbo.DictionaryWord TO api_user;
GRANT SELECT ON TraveloDb.dbo.Flag TO api_user;
GRANT SELECT ON TraveloDb.dbo.ServicePhone TO api_user;
GRANT SELECT ON TraveloDb.dbo.SystemNotification TO api_user;
GRANT SELECT ON TraveloDb.dbo.Alert TO api_user;
GRANT SELECT ON TraveloDb.dbo.Country TO api_user;
GRANT SELECT ON TraveloDb.dbo.Travel TO api_user;
GRANT SELECT ON TraveloDb.dbo.VisitDate TO api_user;
GRANT SELECT ON TraveloDb.dbo.OweSinglePayment TO api_user;
GRANT SELECT ON TraveloDb.dbo.Expense TO api_user;
GRANT SELECT ON TraveloDb.dbo.Spot TO api_user;
GRANT SELECT ON TraveloDb.dbo."User" TO api_user;

GRANT EXEC ON TraveloDb.dbo.CheckBudget TO api_user;
GRANT EXEC ON TraveloDb.dbo.FindCountry TO api_user;
GRANT EXEC ON TraveloDb.dbo.RecoveryPasswordChange TO api_user;
GRANT EXEC ON TraveloDb.dbo.AddOweSinglePayment TO api_user;
GRANT EXEC ON TraveloDb.dbo.AddSpot TO api_user;
GRANT EXEC ON TraveloDb.dbo.AddTravel TO api_user;
GRANT EXEC ON TraveloDb.dbo.AddVisitDate TO api_user;
GRANT EXEC ON TraveloDb.dbo.RemoveTravel TO api_user;