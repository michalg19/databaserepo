/****** Object:  Database [TraveloDb]    Script Date: 04.02.2023 21:37:04 ******/
CREATE DATABASE [TraveloDb]  (EDITION = 'Standard', SERVICE_OBJECTIVE = 'S0', MAXSIZE = 250 GB) WITH CATALOG_COLLATION = SQL_Latin1_General_CP1_CI_AS;
GO
ALTER DATABASE [TraveloDb] SET COMPATIBILITY_LEVEL = 150
GO
ALTER DATABASE [TraveloDb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [TraveloDb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [TraveloDb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [TraveloDb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [TraveloDb] SET ARITHABORT OFF 
GO
ALTER DATABASE [TraveloDb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [TraveloDb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [TraveloDb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [TraveloDb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [TraveloDb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [TraveloDb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [TraveloDb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [TraveloDb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [TraveloDb] SET ALLOW_SNAPSHOT_ISOLATION ON 
GO
ALTER DATABASE [TraveloDb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [TraveloDb] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [TraveloDb] SET  MULTI_USER 
GO
ALTER DATABASE [TraveloDb] SET ENCRYPTION ON
GO
ALTER DATABASE [TraveloDb] SET QUERY_STORE = ON
GO
ALTER DATABASE [TraveloDb] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 100, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
/*** The scripts of database scoped configurations in Azure should be executed inside the target database connection. ***/
GO
-- ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 8;
GO
/****** Object:  User [Api]    Script Date: 04.02.2023 21:37:04 ******/
CREATE USER [Api] FOR LOGIN [TraveloUserApi] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  DatabaseRole [api_user]    Script Date: 04.02.2023 21:37:04 ******/
CREATE ROLE [api_user]
GO
sys.sp_addrolemember @rolename = N'api_user', @membername = N'Api'
GO
/****** Object:  Table [dbo].[Alert]    Script Date: 04.02.2023 21:37:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Alert](
	[AlertId] [int] IDENTITY(1,1) NOT NULL,
	[Content] [varchar](100) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ValidDate] [datetime] NULL,
	[CountryId] [int] NOT NULL,
 CONSTRAINT [PK_Alert] PRIMARY KEY CLUSTERED 
(
	[AlertId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 04.02.2023 21:37:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[CountryId] [int] IDENTITY(1,1) NOT NULL,
	[CodeABC] [varchar](3) NOT NULL,
	[CodeAB] [varchar](2) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Currency] [varchar](3) NOT NULL,
	[FlagId] [int] NOT NULL,
	[ServicePhoneId] [int] NOT NULL,
 CONSTRAINT [PK_Countries] PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dictionary]    Script Date: 04.02.2023 21:37:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dictionary](
	[DictionaryId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_Dictionary] PRIMARY KEY CLUSTERED 
(
	[DictionaryId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DictionaryWord]    Script Date: 04.02.2023 21:37:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DictionaryWord](
	[DictionaryWordId] [int] IDENTITY(1,1) NOT NULL,
	[Word] [nvarchar](255) NOT NULL,
	[WordTranslated] [nvarchar](255) NOT NULL,
	[DictionaryId] [int] NOT NULL,
 CONSTRAINT [PK_DictionaryWord] PRIMARY KEY CLUSTERED 
(
	[DictionaryWordId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Expense]    Script Date: 04.02.2023 21:37:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Expense](
	[ExpenseId] [int] IDENTITY(1,1) NOT NULL,
	[Cost] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_Expense] PRIMARY KEY CLUSTERED 
(
	[ExpenseId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Flag]    Script Date: 04.02.2023 21:37:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Flag](
	[FlagId] [int] IDENTITY(1,1) NOT NULL,
	[URL] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Flag] PRIMARY KEY CLUSTERED 
(
	[FlagId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OweSinglePayment]    Script Date: 04.02.2023 21:37:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OweSinglePayment](
	[OweSinglePaymentId] [int] IDENTITY(1,1) NOT NULL,
	[PersonName] [varchar](30) NOT NULL,
	[PaymentAmount] [decimal](18, 0) NOT NULL,
	[PaymentStatus] [bit] NOT NULL,
	[PaymentDate] [datetime] NOT NULL,
	[IsPayer] [bit] NOT NULL,
	[ExpenseId] [int] NOT NULL,
 CONSTRAINT [PK_OweSinglePayment] PRIMARY KEY CLUSTERED 
(
	[OweSinglePaymentId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ServicePhone]    Script Date: 04.02.2023 21:37:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServicePhone](
	[ServicePhoneId] [int] IDENTITY(1,1) NOT NULL,
	[Number] [varchar](12) NOT NULL,
 CONSTRAINT [PK_ServicePhone] PRIMARY KEY CLUSTERED 
(
	[ServicePhoneId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Spot]    Script Date: 04.02.2023 21:37:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Spot](
	[SpotId] [int] IDENTITY(1,1) NOT NULL,
	[Note] [varchar](255) NULL,
	[Adress] [varchar](255) NULL,
	[VisitDateId] [int] NOT NULL,
	[ExpenseId] [int] NOT NULL,
	[CoordinateX] [decimal](10, 7) NULL,
	[CoordinateY] [decimal](10, 7) NULL,
	[Name] [varchar](255) NULL,
 CONSTRAINT [PK_Spot] PRIMARY KEY CLUSTERED 
(
	[SpotId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SystemNotification]    Script Date: 04.02.2023 21:37:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SystemNotification](
	[SystemNotificationId] [int] IDENTITY(1,1) NOT NULL,
	[Content] [varchar](100) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ValidDate] [datetime] NOT NULL,
	[Title] [varchar](30) NOT NULL,
	[Type] [varchar](30) NOT NULL,
 CONSTRAINT [PK_SystemNotification] PRIMARY KEY CLUSTERED 
(
	[SystemNotificationId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Travel]    Script Date: 04.02.2023 21:37:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Travel](
	[TravelId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](30) NOT NULL,
	[Destination] [varchar](255) NOT NULL,
	[StartDate] [date] NOT NULL,
	[EndDate] [date] NOT NULL,
	[Note] [varchar](100) NULL,
	[PlannedBudget] [decimal](18, 0) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UserId] [int] NOT NULL,
	[CountryId] [int] NOT NULL,
	[HotelName] [varchar](50) NULL,
	[PickedCurrency] [varchar](3) NOT NULL,
	[HotelStreet] [varchar](255) NULL,
	[HotelBuildingNo] [int] NULL,
	[HotelFlatNo] [int] NULL,
	[HotelZipCode] [varchar](255) NULL,
	[HotelCity] [varchar](255) NULL,
 CONSTRAINT [PK_Travel] PRIMARY KEY CLUSTERED 
(
	[TravelId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 04.02.2023 21:37:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](30) NOT NULL,
	[Surname] [varchar](60) NOT NULL,
	[PhoneNumber] [varchar](14) NULL,
	[Email] [varchar](30) NOT NULL,
	[Password] [varchar](1024) NOT NULL,
	[PasswordDateUpdated] [datetime] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VisitDate]    Script Date: 04.02.2023 21:37:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VisitDate](
	[VisitDateId] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Title] [varchar](30) NULL,
	[TravelId] [int] NOT NULL,
 CONSTRAINT [PK_VisitDate] PRIMARY KEY CLUSTERED 
(
	[VisitDateId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Alert] ON 

INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (1, N'Terrorism threats', CAST(N'2023-01-28T22:18:54.343' AS DateTime), CAST(N'2023-02-04T22:18:54.343' AS DateTime), 1)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (2, N'Earthquakes', CAST(N'2023-01-28T22:19:28.523' AS DateTime), CAST(N'2023-02-04T22:19:28.523' AS DateTime), 2)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (3, N'Tsunami', CAST(N'2023-01-28T22:19:53.073' AS DateTime), CAST(N'2023-02-04T22:19:53.073' AS DateTime), 3)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (4, N'Wildfires', CAST(N'2023-01-28T22:20:27.917' AS DateTime), CAST(N'2023-02-04T22:20:27.917' AS DateTime), 4)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (5, N'Floods', CAST(N'2023-01-28T22:21:01.650' AS DateTime), CAST(N'2023-02-04T22:21:01.650' AS DateTime), 3)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (6, N'Heatwaves', CAST(N'2023-01-28T22:21:19.057' AS DateTime), CAST(N'2023-02-04T22:21:19.057' AS DateTime), 5)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (7, N'Riots', CAST(N'2023-01-28T22:25:09.623' AS DateTime), CAST(N'2023-02-04T22:25:09.623' AS DateTime), 6)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (8, N'Threat of war', CAST(N'2023-01-28T22:26:17.640' AS DateTime), CAST(N'2023-02-04T22:26:17.640' AS DateTime), 6)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (9, N'Tornadoes', CAST(N'2023-01-28T22:27:52.690' AS DateTime), CAST(N'2023-02-04T22:27:52.690' AS DateTime), 7)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (10, N'Strong winds', CAST(N'2023-01-28T22:30:44.283' AS DateTime), CAST(N'2023-02-04T22:30:44.283' AS DateTime), 8)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (11, N'Volcanic eruptions', CAST(N'2023-01-28T22:31:42.677' AS DateTime), CAST(N'2023-02-04T22:31:42.677' AS DateTime), 9)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (12, N'Snowstorms', CAST(N'2023-01-28T22:33:01.680' AS DateTime), CAST(N'2023-02-04T22:33:01.680' AS DateTime), 10)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (13, N'Hailstorms', CAST(N'2023-01-28T22:42:51.183' AS DateTime), CAST(N'2023-02-04T22:42:51.183' AS DateTime), 11)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (14, N'Avalanche', CAST(N'2023-01-28T22:45:45.617' AS DateTime), CAST(N'2023-02-04T22:45:45.617' AS DateTime), 12)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (15, N'Cyclone', CAST(N'2023-01-28T22:47:53.563' AS DateTime), CAST(N'2023-02-04T22:47:53.563' AS DateTime), 13)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (16, N'Storms', CAST(N'2023-01-28T22:48:44.130' AS DateTime), CAST(N'2023-02-04T22:48:44.130' AS DateTime), 14)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (17, N'Nuclear accident', CAST(N'2023-01-28T22:50:06.060' AS DateTime), CAST(N'2023-02-04T22:50:06.060' AS DateTime), 15)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (18, N'Pandemic', CAST(N'2023-01-28T22:51:29.820' AS DateTime), CAST(N'2023-02-04T22:51:29.820' AS DateTime), 16)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (19, N'Terrorism threats', CAST(N'2023-01-28T23:15:04.337' AS DateTime), CAST(N'2023-02-04T23:15:04.337' AS DateTime), 31)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (20, N'Earthquakes', CAST(N'2023-01-28T23:15:04.367' AS DateTime), CAST(N'2023-02-04T23:15:04.367' AS DateTime), 32)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (21, N'Tsunami', CAST(N'2023-01-28T23:15:04.373' AS DateTime), CAST(N'2023-02-04T23:15:04.373' AS DateTime), 33)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (22, N'Wildfires', CAST(N'2023-01-28T23:15:04.380' AS DateTime), CAST(N'2023-02-04T23:15:04.380' AS DateTime), 34)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (23, N'Floods', CAST(N'2023-01-28T23:15:04.390' AS DateTime), CAST(N'2023-02-04T23:15:04.390' AS DateTime), 33)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (24, N'Heatwaves', CAST(N'2023-01-28T23:15:04.397' AS DateTime), CAST(N'2023-02-04T23:15:04.397' AS DateTime), 35)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (25, N'Riots', CAST(N'2023-01-28T23:15:04.400' AS DateTime), CAST(N'2023-02-04T23:15:04.400' AS DateTime), 36)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (26, N'Threat of war', CAST(N'2023-01-28T23:15:04.410' AS DateTime), CAST(N'2023-02-04T23:15:04.410' AS DateTime), 36)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (27, N'Tornadoes', CAST(N'2023-01-28T23:15:04.420' AS DateTime), CAST(N'2023-02-04T23:15:04.420' AS DateTime), 37)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (28, N'Strong winds', CAST(N'2023-01-28T23:15:04.440' AS DateTime), CAST(N'2023-02-04T23:15:04.440' AS DateTime), 38)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (29, N'Volcanic eruptions', CAST(N'2023-01-28T23:15:04.447' AS DateTime), CAST(N'2023-02-04T23:15:04.447' AS DateTime), 39)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (30, N'Snowstorms', CAST(N'2023-01-28T23:15:04.453' AS DateTime), CAST(N'2023-02-04T23:15:04.453' AS DateTime), 40)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (31, N'Hailstorms', CAST(N'2023-01-28T23:15:04.460' AS DateTime), CAST(N'2023-02-04T23:15:04.460' AS DateTime), 41)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (32, N'Avalanche', CAST(N'2023-01-28T23:15:04.467' AS DateTime), CAST(N'2023-02-04T23:15:04.467' AS DateTime), 42)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (33, N'Cyclone', CAST(N'2023-01-28T23:15:04.473' AS DateTime), CAST(N'2023-02-04T23:15:04.473' AS DateTime), 43)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (34, N'Storms', CAST(N'2023-01-28T23:15:04.480' AS DateTime), CAST(N'2023-02-04T23:15:04.480' AS DateTime), 44)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (35, N'Nuclear accident', CAST(N'2023-01-28T23:15:04.487' AS DateTime), CAST(N'2023-02-04T23:15:04.487' AS DateTime), 45)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (36, N'Pandemic', CAST(N'2023-01-28T23:15:04.493' AS DateTime), CAST(N'2023-02-04T23:15:04.493' AS DateTime), 46)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (37, N'Terrorism threats', CAST(N'2023-01-28T23:16:34.103' AS DateTime), CAST(N'2023-02-04T23:16:34.103' AS DateTime), 61)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (38, N'Earthquakes', CAST(N'2023-01-28T23:16:34.133' AS DateTime), CAST(N'2023-02-04T23:16:34.133' AS DateTime), 62)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (39, N'Tsunami', CAST(N'2023-01-28T23:16:34.140' AS DateTime), CAST(N'2023-02-04T23:16:34.140' AS DateTime), 63)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (40, N'Wildfires', CAST(N'2023-01-28T23:16:34.147' AS DateTime), CAST(N'2023-02-04T23:16:34.147' AS DateTime), 64)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (41, N'Floods', CAST(N'2023-01-28T23:16:34.150' AS DateTime), CAST(N'2023-02-04T23:16:34.150' AS DateTime), 63)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (42, N'Heatwaves', CAST(N'2023-01-28T23:16:34.157' AS DateTime), CAST(N'2023-02-04T23:16:34.157' AS DateTime), 65)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (43, N'Riots', CAST(N'2023-01-28T23:16:34.163' AS DateTime), CAST(N'2023-02-04T23:16:34.163' AS DateTime), 66)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (44, N'Threat of war', CAST(N'2023-01-28T23:16:34.170' AS DateTime), CAST(N'2023-02-04T23:16:34.170' AS DateTime), 66)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (45, N'Tornadoes', CAST(N'2023-01-28T23:16:34.177' AS DateTime), CAST(N'2023-02-04T23:16:34.177' AS DateTime), 67)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (46, N'Strong winds', CAST(N'2023-01-28T23:16:34.183' AS DateTime), CAST(N'2023-02-04T23:16:34.183' AS DateTime), 68)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (47, N'Volcanic eruptions', CAST(N'2023-01-28T23:16:34.190' AS DateTime), CAST(N'2023-02-04T23:16:34.190' AS DateTime), 69)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (48, N'Snowstorms', CAST(N'2023-01-28T23:16:34.197' AS DateTime), CAST(N'2023-02-04T23:16:34.197' AS DateTime), 70)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (49, N'Hailstorms', CAST(N'2023-01-28T23:16:34.200' AS DateTime), CAST(N'2023-02-04T23:16:34.200' AS DateTime), 71)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (50, N'Avalanche', CAST(N'2023-01-28T23:16:34.207' AS DateTime), CAST(N'2023-02-04T23:16:34.207' AS DateTime), 72)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (51, N'Cyclone', CAST(N'2023-01-28T23:16:34.213' AS DateTime), CAST(N'2023-02-04T23:16:34.213' AS DateTime), 73)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (52, N'Storms', CAST(N'2023-01-28T23:16:34.220' AS DateTime), CAST(N'2023-02-04T23:16:34.220' AS DateTime), 74)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (53, N'Nuclear accident', CAST(N'2023-01-28T23:16:34.227' AS DateTime), CAST(N'2023-02-04T23:16:34.227' AS DateTime), 75)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (54, N'Pandemic', CAST(N'2023-01-28T23:16:34.230' AS DateTime), CAST(N'2023-02-04T23:16:34.230' AS DateTime), 76)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (55, N'Terrorism threats', CAST(N'2023-01-28T23:18:07.200' AS DateTime), CAST(N'2023-02-04T23:18:07.200' AS DateTime), 101)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (56, N'Earthquakes', CAST(N'2023-01-28T23:18:07.220' AS DateTime), CAST(N'2023-02-04T23:18:07.220' AS DateTime), 102)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (57, N'Tsunami', CAST(N'2023-01-28T23:18:07.227' AS DateTime), CAST(N'2023-02-04T23:18:07.227' AS DateTime), 103)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (58, N'Wildfires', CAST(N'2023-01-28T23:18:07.233' AS DateTime), CAST(N'2023-02-04T23:18:07.233' AS DateTime), 104)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (59, N'Floods', CAST(N'2023-01-28T23:18:07.240' AS DateTime), CAST(N'2023-02-04T23:18:07.240' AS DateTime), 103)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (60, N'Heatwaves', CAST(N'2023-01-28T23:18:07.243' AS DateTime), CAST(N'2023-02-04T23:18:07.243' AS DateTime), 105)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (61, N'Riots', CAST(N'2023-01-28T23:18:07.250' AS DateTime), CAST(N'2023-02-04T23:18:07.250' AS DateTime), 106)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (62, N'Threat of war', CAST(N'2023-01-28T23:18:07.253' AS DateTime), CAST(N'2023-02-04T23:18:07.253' AS DateTime), 106)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (63, N'Tornadoes', CAST(N'2023-01-28T23:18:07.260' AS DateTime), CAST(N'2023-02-04T23:18:07.260' AS DateTime), 107)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (64, N'Strong winds', CAST(N'2023-01-28T23:18:07.267' AS DateTime), CAST(N'2023-02-04T23:18:07.267' AS DateTime), 108)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (65, N'Volcanic eruptions', CAST(N'2023-01-28T23:18:07.270' AS DateTime), CAST(N'2023-02-04T23:18:07.270' AS DateTime), 109)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (66, N'Snowstorms', CAST(N'2023-01-28T23:18:07.277' AS DateTime), CAST(N'2023-02-04T23:18:07.277' AS DateTime), 110)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (67, N'Hailstorms', CAST(N'2023-01-28T23:18:07.283' AS DateTime), CAST(N'2023-02-04T23:18:07.283' AS DateTime), 111)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (68, N'Avalanche', CAST(N'2023-01-28T23:18:07.310' AS DateTime), CAST(N'2023-02-04T23:18:07.310' AS DateTime), 112)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (69, N'Cyclone', CAST(N'2023-01-28T23:18:07.317' AS DateTime), CAST(N'2023-02-04T23:18:07.317' AS DateTime), 113)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (70, N'Storms', CAST(N'2023-01-28T23:18:07.323' AS DateTime), CAST(N'2023-02-04T23:18:07.323' AS DateTime), 114)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (71, N'Nuclear accident', CAST(N'2023-01-28T23:18:07.330' AS DateTime), CAST(N'2023-02-04T23:18:07.330' AS DateTime), 115)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (72, N'Pandemic', CAST(N'2023-01-28T23:18:07.333' AS DateTime), CAST(N'2023-02-04T23:18:07.333' AS DateTime), 116)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (73, N'Terrorism threats', CAST(N'2023-01-28T23:19:33.530' AS DateTime), CAST(N'2023-02-04T23:19:33.530' AS DateTime), 151)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (74, N'Earthquakes', CAST(N'2023-01-28T23:19:33.560' AS DateTime), CAST(N'2023-02-04T23:19:33.560' AS DateTime), 152)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (75, N'Tsunami', CAST(N'2023-01-28T23:19:33.567' AS DateTime), CAST(N'2023-02-04T23:19:33.567' AS DateTime), 153)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (76, N'Wildfires', CAST(N'2023-01-28T23:19:33.573' AS DateTime), CAST(N'2023-02-04T23:19:33.573' AS DateTime), 154)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (77, N'Floods', CAST(N'2023-01-28T23:19:33.580' AS DateTime), CAST(N'2023-02-04T23:19:33.580' AS DateTime), 153)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (78, N'Heatwaves', CAST(N'2023-01-28T23:19:33.587' AS DateTime), CAST(N'2023-02-04T23:19:33.587' AS DateTime), 155)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (79, N'Riots', CAST(N'2023-01-28T23:19:33.593' AS DateTime), CAST(N'2023-02-04T23:19:33.593' AS DateTime), 156)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (80, N'Threat of war', CAST(N'2023-01-28T23:19:33.600' AS DateTime), CAST(N'2023-02-04T23:19:33.600' AS DateTime), 156)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (81, N'Tornadoes', CAST(N'2023-01-28T23:19:33.607' AS DateTime), CAST(N'2023-02-04T23:19:33.607' AS DateTime), 157)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (82, N'Strong winds', CAST(N'2023-01-28T23:19:33.617' AS DateTime), CAST(N'2023-02-04T23:19:33.617' AS DateTime), 158)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (83, N'Volcanic eruptions', CAST(N'2023-01-28T23:19:33.627' AS DateTime), CAST(N'2023-02-04T23:19:33.627' AS DateTime), 159)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (84, N'Snowstorms', CAST(N'2023-01-28T23:19:33.633' AS DateTime), CAST(N'2023-02-04T23:19:33.633' AS DateTime), 160)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (85, N'Hailstorms', CAST(N'2023-01-28T23:19:33.640' AS DateTime), CAST(N'2023-02-04T23:19:33.640' AS DateTime), 161)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (86, N'Avalanche', CAST(N'2023-01-28T23:19:33.647' AS DateTime), CAST(N'2023-02-04T23:19:33.647' AS DateTime), 162)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (87, N'Cyclone', CAST(N'2023-01-28T23:19:33.653' AS DateTime), CAST(N'2023-02-04T23:19:33.653' AS DateTime), 163)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (88, N'Storms', CAST(N'2023-01-28T23:19:33.660' AS DateTime), CAST(N'2023-02-04T23:19:33.660' AS DateTime), 164)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (89, N'Nuclear accident', CAST(N'2023-01-28T23:19:33.667' AS DateTime), CAST(N'2023-02-04T23:19:33.667' AS DateTime), 165)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (90, N'Pandemic', CAST(N'2023-01-28T23:19:33.673' AS DateTime), CAST(N'2023-02-04T23:19:33.673' AS DateTime), 166)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (91, N'Terrorism threats', CAST(N'2023-01-28T23:21:12.187' AS DateTime), CAST(N'2023-02-04T23:21:12.187' AS DateTime), 191)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (92, N'Earthquakes', CAST(N'2023-01-28T23:21:12.247' AS DateTime), CAST(N'2023-02-04T23:21:12.247' AS DateTime), 192)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (93, N'Tsunami', CAST(N'2023-01-28T23:21:12.253' AS DateTime), CAST(N'2023-02-04T23:21:12.253' AS DateTime), 193)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (94, N'Wildfires', CAST(N'2023-01-28T23:21:12.260' AS DateTime), CAST(N'2023-02-04T23:21:12.260' AS DateTime), 194)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (95, N'Floods', CAST(N'2023-01-28T23:21:12.267' AS DateTime), CAST(N'2023-02-04T23:21:12.267' AS DateTime), 193)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (96, N'Heatwaves', CAST(N'2023-01-28T23:21:12.273' AS DateTime), CAST(N'2023-02-04T23:21:12.273' AS DateTime), 195)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (97, N'Riots', CAST(N'2023-01-28T23:21:12.280' AS DateTime), CAST(N'2023-02-04T23:21:12.280' AS DateTime), 196)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (98, N'Threat of war', CAST(N'2023-01-28T23:21:12.287' AS DateTime), CAST(N'2023-02-04T23:21:12.287' AS DateTime), 196)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (99, N'Tornadoes', CAST(N'2023-01-28T23:21:12.293' AS DateTime), CAST(N'2023-02-04T23:21:12.293' AS DateTime), 197)
GO
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (100, N'Strong winds', CAST(N'2023-01-28T23:21:12.300' AS DateTime), CAST(N'2023-02-04T23:21:12.300' AS DateTime), 198)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (101, N'Volcanic eruptions', CAST(N'2023-01-28T23:21:12.307' AS DateTime), CAST(N'2023-02-04T23:21:12.307' AS DateTime), 199)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (102, N'Snowstorms', CAST(N'2023-01-28T23:21:12.313' AS DateTime), CAST(N'2023-02-04T23:21:12.313' AS DateTime), 200)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (103, N'Hailstorms', CAST(N'2023-01-28T23:21:12.320' AS DateTime), CAST(N'2023-02-04T23:21:12.320' AS DateTime), 201)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (104, N'Avalanche', CAST(N'2023-01-28T23:21:12.323' AS DateTime), CAST(N'2023-02-04T23:21:12.323' AS DateTime), 202)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (105, N'Cyclone', CAST(N'2023-01-28T23:21:12.330' AS DateTime), CAST(N'2023-02-04T23:21:12.330' AS DateTime), 203)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (106, N'Storms', CAST(N'2023-01-28T23:21:12.337' AS DateTime), CAST(N'2023-02-04T23:21:12.337' AS DateTime), 204)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (107, N'Nuclear accident', CAST(N'2023-01-28T23:21:12.343' AS DateTime), CAST(N'2023-02-04T23:21:12.343' AS DateTime), 205)
INSERT [dbo].[Alert] ([AlertId], [Content], [CreatedDate], [ValidDate], [CountryId]) VALUES (108, N'Pandemic', CAST(N'2023-01-28T23:21:12.350' AS DateTime), CAST(N'2023-02-04T23:21:12.350' AS DateTime), 206)
SET IDENTITY_INSERT [dbo].[Alert] OFF
GO
SET IDENTITY_INSERT [dbo].[Country] ON 

INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (1, N'AFG', N'AF', N'Afghanistan', N'AFA', 1, 1)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (2, N'AND', N'AD', N'Andorra', N'EUR', 8, 2)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (3, N'CMR', N'CM', N'Cameroon', N'XAF', 2, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (4, N'BGR', N'BG', N'Bulgaria', N'BGN', 3, 8)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (5, N'CAN', N'CA', N'Canada', N'CAD', 4, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (6, N'COK', N'CK', N'Cook Islands', N'NZD', 5, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (7, N'NIC', N'NI', N'Nicaragua', N'NIO', 6, 6)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (8, N'NOR', N'NO', N'Norway', N'NOK', 7, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (9, N'ALB', N'AL', N'Albania', N'ALL', 9, 9)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (10, N'DZA', N'DZ', N'Algeria', N'DZD', 10, 10)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (11, N'AGO', N'AO', N'Angola', N'AOA', 11, 2)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (12, N'ATG', N'AG', N'Antigua And Barbuda', N'XCD', 12, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (13, N'ARG', N'AR', N'Argentina', N'ARS', 13, 11)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (14, N'ARM', N'AM', N'Armenia', N'AMD', 14, 12)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (15, N'AUS', N'AU', N'Australia', N'AUD', 15, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (16, N'AUT', N'AT', N'Austria', N'EUR', 16, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (17, N'AZE', N'AZ', N'Azerbaijan', N'AZN', 17, 13)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (18, N'BHS', N'BS', N'Bahamas', N'BSD', 18, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (19, N'BHR', N'BH', N'Bahrain', N'BHD', 19, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (20, N'BGD', N'BD', N'Bangladesh', N'BDT', 20, 7)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (21, N'BRB', N'BB', N'Barbados', N'BBD', 21, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (22, N'BLR', N'BY', N'Belarus', N'BYR', 22, 13)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (23, N'BEL', N'BE', N'Belgium', N'EUR', 23, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (24, N'BLZ', N'BZ', N'Belize', N'BZD', 24, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (25, N'BEN', N'BJ', N'Benin', N'XOF', 25, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (26, N'BTN', N'BT', N'Bhutan', N'BTN', 26, 14)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (27, N'BOL', N'BO', N'Bolivia', N'BOB', 27, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (28, N'BIH', N'BA', N'Bosnia And Herzegovina', N'BAM', 28, 15)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (29, N'BWA', N'BW', N'Botswana', N'BWP', 29, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (30, N'BRA', N'BR', N'Brazil', N'BRL', 30, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (31, N'BRN', N'BN', N'Brunei', N'BND', 31, 16)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (32, N'BFA', N'BF', N'Burkina Faso', N'XOF', 32, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (33, N'BDI', N'BI', N'Burundi', N'BIF', 33, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (34, N'KHM', N'KH', N'Cambodia', N'KHR', 34, 17)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (35, N'CPV', N'CV', N'Cape Verde', N'CVE', 35, 18)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (36, N'CAF', N'CF', N'Central African Republic', N'XAF', 36, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (37, N'TCD', N'TD', N'Chad', N'XAF', 37, 10)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (38, N'CHL', N'CL', N'Chile', N'CLP', 38, 19)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (39, N'CHN', N'CN', N'China', N'RMB', 39, 2)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (40, N'COL', N'CO', N'Colombia', N'COP', 40, 1)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (41, N'COM', N'KM', N'Comoros', N'KMF', 41, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (42, N'COG', N'CG', N'Republic Of The Congo', N'XAF', 42, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (43, N'CRI', N'CR', N'Costa Rica', N'CRC', 43, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (44, N'CIV', N'CI', N'Ivory Coast', N'XOF', 44, 20)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (45, N'HRV', N'HR', N'Croatia', N'HRK', 45, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (46, N'CUB', N'CU', N'Cuba', N'CUP', 46, 21)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (47, N'CYP', N'CY', N'Cyprus', N'EUR', 47, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (48, N'CZE', N'CZ', N'Czech Republic', N'CZK', 48, 22)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (49, N'COD', N'CD', N'Democratic Republic Of The Congo', N'CDF', 49, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (50, N'DNK', N'DK', N'Denmark', N'DKK', 50, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (51, N'DJI', N'DJ', N'Djibouti', N'DJF', 51, 10)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (52, N'DMA', N'DM', N'Dominica', N'XCD', 52, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (53, N'DOM', N'DO', N'Dominican Republic', N'DOP', 53, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (54, N'ECU', N'EC', N'Ecuador', N'USD', 54, 11)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (55, N'EGY', N'EG', N'Egypt', N'EGP', 55, 15)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (56, N'SLV', N'SV', N'El Salvador', N'USD', 56, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (57, N'GNQ', N'GQ', N'Equatorial Guinea', N'XAF', 57, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (58, N'ERI', N'ER', N'Eritrea', N'ERN', 58, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (59, N'EST', N'EE', N'Estonia', N'EUR', 59, 2)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (60, N'SWZ', N'SZ', N'Eswatini', N'SZL', 60, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (61, N'ETH', N'ET', N'Ethiopia', N'ETB', 61, 23)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (62, N'FJI', N'FJ', N'Fiji', N'FJD', 62, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (63, N'FIN', N'FI', N'Finland', N'EUR', 63, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (64, N'FRA', N'FR', N'France', N'EUR', 64, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (65, N'GAB', N'GA', N'Gabon', N'XAF', 65, 24)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (66, N'GMB', N'GM', N'Gambia', N'GMD', 66, 10)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (67, N'GEO', N'GE', N'Georgia', N'GEL', 67, 25)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (68, N'DEU', N'DE', N'Germany', N'EUR', 68, 2)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (69, N'GHA', N'GH', N'Ghana', N'GHC', 69, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (70, N'GRC', N'GR', N'Greece', N'EUR', 70, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (71, N'GRD', N'GD', N'Grenada', N'XCD', 71, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (72, N'GTM', N'GT', N'Guatemala', N'GTQ', 72, 2)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (73, N'GIN', N'GN', N'Guinea', N'GNF', 73, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (74, N'GNB', N'GW', N'Guinea-Bissau', N'XOF', 74, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (75, N'GUY', N'GY', N'Guyana', N'GYD', 75, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (76, N'HTI', N'HT', N'Haiti', N'HTG', 76, 26)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (77, N'HND', N'HN', N'Honduras', N'HNL', 77, 1)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (78, N'HUN', N'HU', N'Hungary', N'HUF', 78, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (79, N'ISL', N'IS', N'Iceland', N'ISK', 79, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (80, N'IND', N'IN', N'India', N'INR', 80, 27)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (81, N'IDN', N'ID', N'Indonesia', N'IDR', 81, 27)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (82, N'IRN', N'IR', N'Iran', N'IRR', 82, 27)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (83, N'IRQ', N'IQ', N'Iraq', N'IQD', 83, 27)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (84, N'IRL', N'IE', N'Ireland', N'EUR', 84, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (85, N'ISR', N'IL', N'Israel', N'ILS', 85, 27)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (86, N'ITA', N'IT', N'Italy', N'EUR', 86, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (87, N'JAM', N'JM', N'Jamaica', N'JMD', 87, 1)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (88, N'JPN', N'JP', N'Japan', N'JPY', 88, 27)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (89, N'JOR', N'JO', N'Jordan', N'JOD', 89, 28)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (90, N'KAZ', N'KZ', N'Kazakhstan', N'KZT', 90, 29)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (91, N'KEN', N'KE', N'Kenya', N'KES', 91, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (92, N'KIR', N'KI', N'Kiribati', N'AUD', 92, 30)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (93, N'KWT', N'KW', N'Kuwait', N'KD', 93, 31)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (94, N'KGZ', N'KG', N'Kyrgyzstan', N'KGS', 94, 12)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (95, N'LAO', N'LA', N'Laos', N'LAK', 95, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (96, N'LVA', N'LV', N'Latvia', N'EUR', 96, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (97, N'LBN', N'LB', N'Lebanon', N'LBP', 97, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (98, N'LSO', N'LS', N'Lesotho', N'LSL', 98, 32)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (99, N'LBR', N'LR', N'Liberia', N'LRD', 99, 4)
GO
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (100, N'LBY', N'LY', N'Libya', N'LYD', 100, 33)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (101, N'LIE', N'LI', N'Liechtenstein', N'CHF', 101, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (102, N'LTU', N'LT', N'Lithuania', N'LTL', 102, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (103, N'LUX', N'LU', N'Luxembourg', N'EUR', 103, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (104, N'MKD', N'MK', N'Macedonia', N'MKD', 104, 34)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (105, N'MDG', N'MG', N'Madagascar', N'MGA', 105, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (106, N'MWI', N'MW', N'Malawi', N'MWK', 106, 35)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (107, N'MYS', N'MY', N'Malaysia', N'MYR', 107, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (108, N'MDV', N'MV', N'Maldives', N'MVR', 108, 1)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (109, N'MLI', N'ML', N'Mali', N'XOF', 109, 36)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (110, N'MLT', N'MT', N'Malta', N'EUR', 110, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (111, N'MHL', N'MH', N'Marshall Islands', N'USD', 111, 37)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (112, N'MRT', N'MR', N'Mauritania', N'MRO', 112, 17)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (113, N'MUS', N'MU', N'Mauritius', N'MUR', 113, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (114, N'MEX', N'MX', N'Mexico', N'MXN', 114, 38)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (115, N'FSM', N'FM', N'Micronesia', N'USD', 115, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (116, N'MDA', N'MD', N'Moldova', N'MDL', 116, 39)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (117, N'MCO', N'MC', N'Monaco', N'EUR', 117, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (118, N'MNG', N'MN', N'Mongolia', N'MNT', 118, 40)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (119, N'MNE', N'ME', N'Montenegro', N'EUR', 119, 41)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (120, N'MAR', N'MA', N'Morocco', N'MAD', 120, 9)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (121, N'MOZ', N'MZ', N'Mozambique', N'MZM', 121, 1)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (122, N'MMR', N'MM', N'Myanmar', N'MMK', 122, 42)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (123, N'NAM', N'NA', N'Namibia', N'NAD', 123, 43)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (124, N'NRU', N'NR', N'Nauru', N'AUD', 124, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (125, N'NPL', N'NP', N'Nepal', N'NPR', 125, 27)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (126, N'NLD', N'NL', N'Netherlands', N'EUR', 126, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (127, N'NZL', N'NZ', N'New Zealand', N'NZD', 127, 44)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (128, N'NER', N'NE', N'Niger', N'XOF', 128, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (129, N'NGA', N'NG', N'Nigeria', N'NGN', 129, 42)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (130, N'PRK', N'KP', N'North Korea', N'KPW', 130, 45)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (131, N'OMN', N'OM', N'Oman', N'OMR', 131, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (132, N'PAK', N'PK', N'Pakistan', N'PKR', 132, 46)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (133, N'PLW', N'PW', N'Palau', N'USD', 133, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (134, N'PAN', N'PA', N'Panama', N'PAB', 134, 47)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (135, N'PNG', N'PG', N'Papua New Guinea', N'PGK', 135, 48)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (136, N'PRY', N'PY', N'Paraguay', N'PYG', 136, 49)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (137, N'PER', N'PE', N'Peru', N'PEN', 137, 50)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (138, N'PHL', N'PH', N'Philippines', N'PHP', 138, 8)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (139, N'PLN', N'PL', N'Poland', N'PLN', 139, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (140, N'PRT', N'PT', N'Portugal', N'EUR', 140, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (141, N'QAT', N'QA', N'Qatar', N'QAR', 141, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (142, N'ROU', N'RO', N'Romania', N'RON', 142, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (143, N'RUS', N'RU', N'Russia', N'RUB', 143, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (144, N'RWA', N'RW', N'Rwanda', N'RWF', 144, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (145, N'KNA', N'KN', N'Saint Kitts And Nevis', N'XCD', 145, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (146, N'LCA', N'LC', N'Saint Lucia', N'XCD', 146, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (147, N'WSM', N'WS', N'Samoa', N'SAT', 147, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (148, N'SMR', N'SM', N'San Marino', N'EUR', 148, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (149, N'STP', N'ST', N'Sao Tome And Principe', N'STD', 149, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (150, N'SAU', N'SA', N'Saudi Arabia', N'SAR', 150, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (151, N'SEN', N'SN', N'Senegal', N'XOF', 151, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (152, N'SRB', N'RS', N'Serbia', N'RSD', 152, 41)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (153, N'SYC', N'SC', N'Seychelles', N'SCR', 153, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (154, N'SLE', N'SL', N'Sierra Leone', N'SLL', 154, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (155, N'SGP', N'SG', N'Singapore', N'SGD', 155, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (156, N'SVK', N'SK', N'Slovakia', N'EUR', 156, 22)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (157, N'SVN', N'SI', N'Slovenia', N'EUR', 157, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (158, N'SLB', N'SB', N'Solomon Islands', N'SBD', 158, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (159, N'SOM', N'SO', N'Somalia', N'SOS', 159, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (160, N'ZAF', N'ZA', N'South Africa', N'ZAR', 160, 51)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (161, N'KOR', N'KR', N'South Korea', N'KRW', 161, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (162, N'SSD', N'SS', N'South Sudan', N'SSP', 162, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (163, N'ESP', N'ES', N'Spain', N'EUR', 163, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (164, N'LKA', N'LK', N'Sri Lanka', N'LKR', 164, 52)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (165, N'VCT', N'VC', N'Saint Vincent And The Grenadines', N'XCD', 165, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (166, N'SDN', N'SD', N'Sudan', N'SDG', 166, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (167, N'SUR', N'SR', N'Suriname', N'SRD', 167, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (168, N'SWE', N'SE', N'Sweden', N'SEK', 168, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (169, N'CHE', N'CH', N'Switzerland', N'CHF', 169, 17)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (170, N'SYR', N'SY', N'Syria', N'SYP', 170, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (171, N'TJK', N'TJ', N'Tajikistan', N'TJS', 171, 29)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (172, N'TZA', N'TZ', N'Tanzania', N'TZS', 172, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (173, N'THA', N'TH', N'Thailand', N'THB', 173, 53)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (174, N'TLS', N'TL', N'East Timor', N'USD', 174, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (175, N'TGO', N'TG', N'Togo', N'XOF', 175, 11)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (176, N'TON', N'TO', N'Tonga', N'TOP', 176, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (177, N'TTO', N'TT', N'Trinidad And Tobago', N'TTD', 177, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (178, N'TUN', N'TN', N'Tunisia', N'TND', 178, 54)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (179, N'TUR', N'TR', N'Turkey', N'TRY', 179, 27)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (180, N'TKM', N'TM', N'Turkmenistan', N'TMM', 180, 29)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (181, N'TUV', N'TV', N'Tuvalu', N'TVD', 181, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (182, N'UGA', N'UG', N'Uganda', N'UGX', 182, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (183, N'UKR', N'UA', N'Ukraine', N'UAH', 183, 13)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (184, N'ARE', N'AE', N'United Arab Emirates', N'AED', 184, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (185, N'GBR', N'GB', N'United Kingdom', N'GBP', 185, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (186, N'USA', N'US', N'United States', N'USD', 186, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (187, N'URY', N'UY', N'Uruguay', N'UYU', 187, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (188, N'UZB', N'UZ', N'Uzbekistan', N'UZS', 188, 29)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (189, N'VUT', N'VU', N'Vanuatu', N'VUV', 189, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (190, N'VEN', N'VE', N'Venezuela', N'VEB', 190, 55)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (191, N'VNM', N'VN', N'Vietnam', N'VND', 191, 29)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (192, N'YEM', N'YE', N'Yemen', N'YER', 192, 56)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (193, N'ZMB', N'ZM', N'Zambia', N'ZMK', 193, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (194, N'ZWE', N'ZW', N'Zimbabwe', N'ZWD', 194, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (195, N'PSE', N'PS', N'Palestine', N'NIS', 195, 27)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (196, N'VAT', N'VA', N'Vatican', N'EUR', 196, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (197, N'ASM', N'AS', N'American Samoa', N'USD', 197, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (198, N'AIA', N'AI', N'Anguilla', N'XCD', 198, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (199, N'ABW', N'AW', N'Aruba', N'AWG', 199, 4)
GO
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (200, N'BMU', N'BM', N'Bermuda', N'BMD', 200, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (201, N'VGB', N'VG', N'British Virgin Islands', N'USD', 201, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (202, N'CYM', N'KY', N'Cayman Islands', N'KYD', 202, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (203, N'FLK', N'FK', N'Falkland Islands', N'FKP', 203, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (204, N'GUF', N'GF', N'French Guiana', N'EUR', 204, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (205, N'PYF', N'PF', N'French Polynesia', N'XPF', 205, 10)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (206, N'GIB', N'GI', N'Gibraltar', N'GIP', 206, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (207, N'GRL', N'GL', N'Greenland', N'DKK', 207, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (208, N'GLP', N'GP', N'Guadeloupe', N'EUR', 208, 10)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (209, N'GUM', N'GU', N'Guam', N'USD', 209, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (210, N'HKG', N'HK', N'Hong Kong', N'HKD', 210, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (211, N'MAC', N'MO', N'Macao', N'MOP', 211, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (212, N'MSR', N'MS', N'Montserrat', N'XCD', 212, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (213, N'NCL', N'NC', N'New Caledonia', N'XPF', 213, 10)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (214, N'NIU', N'NU', N'Niue', N'NZD', 214, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (215, N'NFK', N'NF', N'Norfolk Island', N'AUD', 215, 57)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (216, N'MNP', N'MP', N'Northern Mariana Islands', N'USD', 216, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (217, N'PCN', N'PN', N'Pitcairn', N'NZD', 217, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (218, N'PRI', N'PR', N'Puerto Rico', N'USD', 218, 3)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (219, N'REU', N'RE', N'Reunion', N'EUR', 219, 10)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (220, N'TWN', N'TW', N'Taiwan', N'TWD', 220, 2)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (221, N'TKL', N'TK', N'Tokelau', N'NZD', 221, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (222, N'TCA', N'TC', N'Turks And Caicos Islands', N'USD', 222, 5)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (223, N'VIR', N'VI', N'U.S. Virgin Islands', N'USD', 223, 4)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (224, N'WLF', N'WF', N'Wallis And Futuna', N'XPF', 224, 46)
INSERT [dbo].[Country] ([CountryId], [CodeABC], [CodeAB], [Name], [Currency], [FlagId], [ServicePhoneId]) VALUES (225, N'ESH', N'EH', N'Western Sahara', N'MAD', 225, 58)
SET IDENTITY_INSERT [dbo].[Country] OFF
GO
SET IDENTITY_INSERT [dbo].[Dictionary] ON 

INSERT [dbo].[Dictionary] ([DictionaryId], [Name]) VALUES (1, N'English')
INSERT [dbo].[Dictionary] ([DictionaryId], [Name]) VALUES (2, N'Deutsh')
INSERT [dbo].[Dictionary] ([DictionaryId], [Name]) VALUES (3, N'Français')
INSERT [dbo].[Dictionary] ([DictionaryId], [Name]) VALUES (4, N'Español')
INSERT [dbo].[Dictionary] ([DictionaryId], [Name]) VALUES (5, N'日本語')
INSERT [dbo].[Dictionary] ([DictionaryId], [Name]) VALUES (6, N'Русский')
SET IDENTITY_INSERT [dbo].[Dictionary] OFF
GO
SET IDENTITY_INSERT [dbo].[DictionaryWord] ON 

INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (1, N'Dzien dobry, jak dostac sie do ...', N'Good morning, how to get to the ...', 1)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (2, N'Dzien dobry, jak dostac sie do ...', N'Guten Tag, wie komme ich zum ...', 2)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (3, N'Dzien dobry, jak dostac sie do ...', N'Bonjour, comment se rendre chez le ...', 3)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (4, N'Dzien dobry, jak dostac sie do ...', N'Buenos dias como llegar a la ...', 4)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (5, N'Dzien dobry, jak dostac sie do ...', N'こんにちは、...への行き方 ', 5)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (6, N'Dzien dobry, jak dostac sie do ...', N'Добрый день, как пройти к  ...', 6)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (7, N'Ile to kosztuje?', N'How much is it?', 1)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (8, N'Ile to kosztuje?', N'Wie viel kostet das?', 2)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (9, N'Ile to kosztuje?', N'Combien ça coûte?', 3)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (10, N'Ile to kosztuje?', N'¿Cuánto cuesta?', 4)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (11, N'Ile to kosztuje?', N'いくらですか？', 5)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (12, N'Ile to kosztuje?', N'Сколько это стоит?', 6)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (13, N'Potrzebuje pomocy!', N'I need help!', 1)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (14, N'Potrzebuje pomocy!', N'Ich brauche Hilfe!', 2)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (15, N'Potrzebuje pomocy!', N'J''ai besoin d''aide!', 3)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (16, N'Potrzebuje pomocy!', N'¡Necesito ayuda!', 4)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (17, N'Potrzebuje pomocy!', N'私は助けが必要です！', 5)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (18, N'Potrzebuje pomocy!', N'мне нужна помощь!', 6)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (19, N'Czy moge postawic tu namiot?', N'Can I put a tent here?', 1)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (20, N'Czy moge postawic tu namiot?', N'Kann ich hier ein Zelt aufstellen?', 2)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (21, N'Czy moge postawic tu namiot?', N'Puis-je mettre une tente ici?', 3)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (22, N'Czy moge postawic tu namiot?', N'¿Puedo poner una carpa aquí?', 4)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (23, N'Czy moge postawic tu namiot?', N'ここにテントを張ってもいいですか？', 5)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (24, N'Czy moge postawic tu namiot?', N'Можно ли поставить здесь палатку?', 6)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (25, N'Zgubilem sie', N'I got lost', 1)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (26, N'Zgubilem sie', N'Ich habe mich verlaufen', 2)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (27, N'Zgubilem sie', N'Je me suis perdu', 3)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (28, N'Zgubilem sie', N'Me perdí', 4)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (29, N'Zgubilem sie', N'迷子になりました', 5)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (30, N'Zgubilem sie', N'я потерял', 6)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (31, N'Bankomat', N'Cash machine', 1)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (32, N'Bankomat', N'Geldautomat', 2)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (33, N'Bankomat', N'Distributeur de billets de banque', 3)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (34, N'Bankomat', N'Cajero automático', 4)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (35, N'Bankomat', N'現金自動預け払い機', 5)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (36, N'Bankomat', N'банкомат', 6)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (37, N'Szpital', N'Hospital', 1)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (38, N'Szpital', N'Krankenhaus', 2)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (39, N'Szpital', N'Hôpital', 3)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (40, N'Szpital', N'Hospital', 4)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (41, N'Szpital', N'病院', 5)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (42, N'Szpital', N'Больница', 6)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (43, N'Apteka', N'Pharmacy', 1)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (44, N'Apteka', N'Apotheke', 2)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (45, N'Apteka', N'Pharmacie', 3)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (46, N'Apteka', N'Farmacia', 4)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (47, N'Apteka', N'薬局', 5)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (48, N'Apteka', N'Аптека', 6)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (49, N'Policja', N'Police', 1)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (50, N'Policja', N'Polizei', 2)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (51, N'Policja', N'La police', 3)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (52, N'Policja', N'Policía', 4)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (53, N'Policja', N'警察', 5)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (54, N'Policja', N'Полиция', 6)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (55, N'Nocleg', N'Accommodation', 1)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (56, N'Nocleg', N'Unterkunft', 2)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (57, N'Nocleg', N'Logement', 3)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (58, N'Nocleg', N'Alojamiento', 4)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (59, N'Nocleg', N'宿泊施設', 5)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (60, N'Nocleg', N'Проживание', 6)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (61, N'Dokad jedziesz?', N'Where are you going?', 1)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (62, N'Dokad jedziesz?', N'Wo gehst du hin?', 2)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (63, N'Dokad jedziesz?', N'Où allez-vous?', 3)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (64, N'Dokad jedziesz?', N'¿A dónde vas?', 4)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (65, N'Dokad jedziesz?', N'どこに行くの？', 5)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (66, N'Dokad jedziesz?', N'Куда ты идешь?', 6)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (67, N'Autobus', N'Bus', 1)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (68, N'Autobus', N'Der Bus', 2)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (69, N'Autobus', N'Le bus', 3)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (70, N'Autobus', N'Autobús', 4)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (71, N'Autobus', N'バス', 5)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (72, N'Autobus', N'Автобус', 6)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (73, N'Pociag', N'Train', 1)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (74, N'Pociag', N'Zug', 2)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (75, N'Pociag', N'Former', 3)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (76, N'Pociag', N'Tren', 4)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (77, N'Pociag', N'訓練', 5)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (78, N'Pociag', N'Тренироваться', 6)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (79, N'Samolot', N'Plane', 1)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (80, N'Samolot', N'Flugzeug', 2)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (81, N'Samolot', N'Avion', 3)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (82, N'Samolot', N'Plano', 4)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (83, N'Samolot', N'飛行機', 5)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (84, N'Samolot', N'Самолет', 6)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (85, N'Dworzec', N'Station', 1)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (86, N'Dworzec', N'Bahnhof', 2)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (87, N'Dworzec', N'La gare', 3)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (88, N'Dworzec', N'Estación', 4)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (89, N'Dworzec', N'駅', 5)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (90, N'Dworzec', N'Станция', 6)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (91, N'Lotnisko', N'Airport', 1)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (92, N'Lotnisko', N'Flughafen', 2)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (93, N'Lotnisko', N'Aéroport', 3)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (94, N'Lotnisko', N'Aeropuerto', 4)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (95, N'Lotnisko', N'空港', 5)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (96, N'Lotnisko', N'аэропорт', 6)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (97, N'Bilet', N'Ticket', 1)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (98, N'Bilet', N'Fahrkarte', 2)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (99, N'Bilet', N'Billet', 3)
GO
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (100, N'Bilet', N'Boleto', 4)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (101, N'Bilet', N'チケット', 5)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (102, N'Bilet', N'Проездной билет', 6)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (103, N'Prysznic', N'Shower', 1)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (104, N'Prysznic', N'Dusche', 2)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (105, N'Prysznic', N'Douche', 3)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (106, N'Prysznic', N'Ducha', 4)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (107, N'Prysznic', N'シャワー', 5)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (108, N'Prysznic', N'Душ', 6)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (109, N'Toaleta', N'Toilet', 1)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (110, N'Toaleta', N'Toilette', 2)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (111, N'Toaleta', N'Toilette', 3)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (112, N'Toaleta', N'Inodoro', 4)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (113, N'Toaleta', N'トイレ', 5)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (114, N'Toaleta', N'Туалет', 6)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (115, N'WC', N'Toilets', 1)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (116, N'WC', N'Toiletten', 2)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (117, N'WC', N'Toilettes', 3)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (118, N'WC', N'Baños', 4)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (119, N'WC', N'トイレ', 5)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (120, N'WC', N'Туалеты', 6)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (121, N'Papier toaletowy', N'Toilet paper', 1)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (122, N'Papier toaletowy', N'Toilettenpapier', 2)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (123, N'Papier toaletowy', N'Papier toilette', 3)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (124, N'Papier toaletowy', N'Papel higiénico', 4)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (125, N'Papier toaletowy', N'トイレットペーパー', 5)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (126, N'Papier toaletowy', N'Туалетная бумага', 6)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (127, N'Biegunka', N'Diarrhea', 1)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (128, N'Biegunka', N'Durchfall', 2)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (129, N'Biegunka', N'Diarrhée', 3)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (130, N'Biegunka', N'Diarrea', 4)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (131, N'Biegunka', N'下痢', 5)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (132, N'Biegunka', N'Диарея', 6)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (133, N'Sklep spozywczy', N'Grocery store', 1)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (134, N'Sklep spozywczy', N'Lebensmittelmarkt', 2)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (135, N'Sklep spozywczy', N'Épicerie', 3)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (136, N'Sklep spozywczy', N'Tienda de comestibles', 4)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (137, N'Sklep spozywczy', N'食料品店', 5)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (138, N'Sklep spozywczy', N'Продуктовый магазин', 6)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (139, N'Woda do picia', N'Drinking water', 1)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (140, N'Woda do picia', N'Wasser trinken', 2)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (141, N'Woda do picia', N'Boire de l''eau', 3)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (142, N'Woda do picia', N'Agua potable', 4)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (143, N'Woda do picia', N'水を飲んでいる', 5)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (144, N'Woda do picia', N'Питьевая вода', 6)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (145, N'Pieniadze', N'Money', 1)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (146, N'Pieniadze', N'Geld', 2)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (147, N'Pieniadze', N'Argent', 3)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (148, N'Pieniadze', N'Dinero', 4)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (149, N'Pieniadze', N'お金', 5)
INSERT [dbo].[DictionaryWord] ([DictionaryWordId], [Word], [WordTranslated], [DictionaryId]) VALUES (150, N'Pieniadze', N'Деньги', 6)
SET IDENTITY_INSERT [dbo].[DictionaryWord] OFF
GO
SET IDENTITY_INSERT [dbo].[Expense] ON 

INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (3, CAST(234000 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (4, CAST(20 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (5, CAST(20 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (6, CAST(20 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (7, CAST(20 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (8, CAST(20 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (9, CAST(20 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (10, CAST(20 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (76, CAST(123 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (77, CAST(130 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (83, CAST(9000 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (85, CAST(200 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (86, CAST(400 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (87, CAST(20 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (88, CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (89, CAST(30 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (90, CAST(50 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (91, CAST(35 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (92, CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (93, CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (94, CAST(100 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (95, CAST(100 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (96, CAST(100 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (97, CAST(50 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (98, CAST(50 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (99, CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (100, CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (101, CAST(10 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (102, CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (132, CAST(50 AS Decimal(18, 0)))
INSERT [dbo].[Expense] ([ExpenseId], [Cost]) VALUES (133, CAST(50 AS Decimal(18, 0)))
SET IDENTITY_INSERT [dbo].[Expense] OFF
GO
SET IDENTITY_INSERT [dbo].[Flag] ON 

INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (1, N'https://www.worldatlas.com/img/flag/af-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (2, N'https://www.worldatlas.com/img/flag/cm-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (3, N'https://www.worldatlas.com/img/flag/bg-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (4, N'https://www.worldatlas.com/img/flag/ca-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (5, N'https://www.worldatlas.com/img/flag/ck-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (6, N'https://www.worldatlas.com/img/flag/ni-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (7, N'https://www.worldatlas.com/img/flag/no-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (8, N'https://www.worldatlas.com/img/flag/ad-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (9, N'https://www.worldatlas.com/img/flag/al-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (10, N'https://www.worldatlas.com/img/flag/dz-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (11, N'https://www.worldatlas.com/img/flag/ao-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (12, N'https://www.worldatlas.com/img/flag/ag-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (13, N'https://www.worldatlas.com/img/flag/ar-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (14, N'https://www.worldatlas.com/img/flag/am-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (15, N'https://www.worldatlas.com/img/flag/au-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (16, N'https://www.worldatlas.com/img/flag/at-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (17, N'https://www.worldatlas.com/img/flag/az-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (18, N'https://www.worldatlas.com/img/flag/bs-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (19, N'https://www.worldatlas.com/img/flag/bh-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (20, N'https://www.worldatlas.com/img/flag/bd-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (21, N'https://www.worldatlas.com/img/flag/bb-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (22, N'https://www.worldatlas.com/img/flag/by-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (23, N'https://www.worldatlas.com/img/flag/be-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (24, N'https://www.worldatlas.com/img/flag/bz-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (25, N'https://www.worldatlas.com/img/flag/bj-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (26, N'https://www.worldatlas.com/img/flag/bt-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (27, N'https://www.worldatlas.com/img/flag/bo-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (28, N'https://www.worldatlas.com/img/flag/ba-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (29, N'https://www.worldatlas.com/img/flag/bw-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (30, N'https://www.worldatlas.com/img/flag/br-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (31, N'https://www.worldatlas.com/img/flag/bn-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (32, N'https://www.worldatlas.com/img/flag/bf-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (33, N'https://www.worldatlas.com/img/flag/bi-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (34, N'https://www.worldatlas.com/img/flag/kh-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (35, N'https://www.worldatlas.com/img/flag/cv-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (36, N'https://www.worldatlas.com/img/flag/cf-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (37, N'https://www.worldatlas.com/img/flag/td-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (38, N'https://www.worldatlas.com/img/flag/cl-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (39, N'https://www.worldatlas.com/img/flag/cn-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (40, N'https://www.worldatlas.com/img/flag/co-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (41, N'https://www.worldatlas.com/img/flag/km-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (42, N'https://www.worldatlas.com/img/flag/cg-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (43, N'https://www.worldatlas.com/img/flag/cr-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (44, N'https://www.worldatlas.com/img/flag/ci-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (45, N'https://www.worldatlas.com/img/flag/hr-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (46, N'https://www.worldatlas.com/img/flag/cu-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (47, N'https://www.worldatlas.com/img/flag/cy-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (48, N'https://www.worldatlas.com/img/flag/cz-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (49, N'https://www.worldatlas.com/img/flag/cd-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (50, N'https://www.worldatlas.com/img/flag/dk-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (51, N'https://www.worldatlas.com/img/flag/dj-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (52, N'https://www.worldatlas.com/img/flag/dm-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (53, N'https://www.worldatlas.com/img/flag/do-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (54, N'https://www.worldatlas.com/img/flag/ec-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (55, N'https://www.worldatlas.com/img/flag/eg-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (56, N'https://www.worldatlas.com/img/flag/sv-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (57, N'https://www.worldatlas.com/img/flag/gq-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (58, N'https://www.worldatlas.com/img/flag/er-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (59, N'https://www.worldatlas.com/img/flag/ee-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (60, N'https://www.worldatlas.com/img/flag/sz-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (61, N'https://www.worldatlas.com/img/flag/et-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (62, N'https://www.worldatlas.com/img/flag/fj-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (63, N'https://www.worldatlas.com/img/flag/fi-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (64, N'https://www.worldatlas.com/img/flag/fr-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (65, N'https://www.worldatlas.com/img/flag/ga-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (66, N'https://www.worldatlas.com/img/flag/gm-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (67, N'https://www.worldatlas.com/img/flag/ge-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (68, N'https://www.worldatlas.com/img/flag/de-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (69, N'https://www.worldatlas.com/img/flag/gh-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (70, N'https://www.worldatlas.com/img/flag/gr-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (71, N'https://www.worldatlas.com/img/flag/gd-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (72, N'https://www.worldatlas.com/img/flag/gt-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (73, N'https://www.worldatlas.com/img/flag/gn-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (74, N'https://www.worldatlas.com/img/flag/gw-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (75, N'https://www.worldatlas.com/img/flag/gy-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (76, N'https://www.worldatlas.com/img/flag/ht-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (77, N'https://www.worldatlas.com/img/flag/hn-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (78, N'https://www.worldatlas.com/img/flag/hu-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (79, N'https://www.worldatlas.com/img/flag/is-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (80, N'https://www.worldatlas.com/img/flag/in-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (81, N'https://www.worldatlas.com/img/flag/id-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (82, N'https://www.worldatlas.com/img/flag/ir-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (83, N'https://www.worldatlas.com/img/flag/iq-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (84, N'https://www.worldatlas.com/img/flag/ie-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (85, N'https://www.worldatlas.com/img/flag/il-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (86, N'https://www.worldatlas.com/img/flag/it-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (87, N'https://www.worldatlas.com/img/flag/jm-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (88, N'https://www.worldatlas.com/img/flag/jp-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (89, N'https://www.worldatlas.com/img/flag/jo-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (90, N'https://www.worldatlas.com/img/flag/kz-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (91, N'https://www.worldatlas.com/img/flag/ke-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (92, N'https://www.worldatlas.com/img/flag/ki-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (93, N'https://www.worldatlas.com/img/flag/kw-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (94, N'https://www.worldatlas.com/img/flag/kg-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (95, N'https://www.worldatlas.com/img/flag/la-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (96, N'https://www.worldatlas.com/img/flag/lv-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (97, N'https://www.worldatlas.com/img/flag/lb-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (98, N'https://www.worldatlas.com/img/flag/ls-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (99, N'https://www.worldatlas.com/img/flag/lr-flag.jpg')
GO
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (100, N'https://www.worldatlas.com/img/flag/ly-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (101, N'https://www.worldatlas.com/img/flag/li-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (102, N'https://www.worldatlas.com/img/flag/lt-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (103, N'https://www.worldatlas.com/img/flag/lu-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (104, N'https://www.worldatlas.com/img/flag/mk-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (105, N'https://www.worldatlas.com/img/flag/mg-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (106, N'https://www.worldatlas.com/img/flag/mw-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (107, N'https://www.worldatlas.com/img/flag/my-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (108, N'https://www.worldatlas.com/img/flag/mv-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (109, N'https://www.worldatlas.com/img/flag/ml-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (110, N'https://www.worldatlas.com/img/flag/mt-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (111, N'https://www.worldatlas.com/img/flag/mh-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (112, N'https://www.worldatlas.com/img/flag/mr-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (113, N'https://www.worldatlas.com/img/flag/mu-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (114, N'https://www.worldatlas.com/img/flag/mx-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (115, N'https://www.worldatlas.com/img/flag/fm-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (116, N'https://www.worldatlas.com/img/flag/md-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (117, N'https://www.worldatlas.com/img/flag/mc-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (118, N'https://www.worldatlas.com/img/flag/mn-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (119, N'https://www.worldatlas.com/img/flag/me-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (120, N'https://www.worldatlas.com/img/flag/ma-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (121, N'https://www.worldatlas.com/img/flag/mz-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (122, N'https://www.worldatlas.com/img/flag/mm-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (123, N'https://www.worldatlas.com/img/flag/na-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (124, N'https://www.worldatlas.com/img/flag/nr-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (125, N'https://www.worldatlas.com/img/flag/np-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (126, N'https://www.worldatlas.com/img/flag/nl-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (127, N'https://www.worldatlas.com/img/flag/nz-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (128, N'https://www.worldatlas.com/img/flag/ne-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (129, N'https://www.worldatlas.com/img/flag/ng-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (130, N'https://www.worldatlas.com/img/flag/kp-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (131, N'https://www.worldatlas.com/img/flag/om-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (132, N'https://www.worldatlas.com/img/flag/pk-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (133, N'https://www.worldatlas.com/img/flag/pw-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (134, N'https://www.worldatlas.com/img/flag/pa-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (135, N'https://www.worldatlas.com/img/flag/pg-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (136, N'https://www.worldatlas.com/img/flag/py-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (137, N'https://www.worldatlas.com/img/flag/pe-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (138, N'https://www.worldatlas.com/img/flag/ph-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (139, N'https://www.worldatlas.com/img/flag/pl-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (140, N'https://www.worldatlas.com/img/flag/pt-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (141, N'https://www.worldatlas.com/img/flag/qa-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (142, N'https://www.worldatlas.com/img/flag/ro-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (143, N'https://www.worldatlas.com/img/flag/ru-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (144, N'https://www.worldatlas.com/img/flag/rw-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (145, N'https://www.worldatlas.com/img/flag/kn-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (146, N'https://www.worldatlas.com/img/flag/lc-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (147, N'https://www.worldatlas.com/img/flag/ws-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (148, N'https://www.worldatlas.com/img/flag/sm-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (149, N'https://www.worldatlas.com/img/flag/st-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (150, N'https://www.worldatlas.com/img/flag/sa-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (151, N'https://www.worldatlas.com/img/flag/sn-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (152, N'https://www.worldatlas.com/img/flag/rs-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (153, N'https://www.worldatlas.com/img/flag/sc-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (154, N'https://www.worldatlas.com/img/flag/sl-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (155, N'https://www.worldatlas.com/img/flag/sg-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (156, N'https://www.worldatlas.com/img/flag/sk-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (157, N'https://www.worldatlas.com/img/flag/si-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (158, N'https://www.worldatlas.com/img/flag/sb-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (159, N'https://www.worldatlas.com/img/flag/so-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (160, N'https://www.worldatlas.com/img/flag/za-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (161, N'https://www.worldatlas.com/img/flag/kr-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (162, N'https://www.worldatlas.com/img/flag/ss-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (163, N'https://www.worldatlas.com/img/flag/es-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (164, N'https://www.worldatlas.com/img/flag/lk-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (165, N'https://www.worldatlas.com/img/flag/vc-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (166, N'https://www.worldatlas.com/img/flag/sd-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (167, N'https://www.worldatlas.com/img/flag/sr-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (168, N'https://www.worldatlas.com/img/flag/se-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (169, N'https://www.worldatlas.com/img/flag/ch-flag.png')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (170, N'https://www.worldatlas.com/img/flag/sy-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (171, N'https://www.worldatlas.com/img/flag/tj-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (172, N'https://www.worldatlas.com/img/flag/tz-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (173, N'https://www.worldatlas.com/img/flag/th-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (174, N'https://www.worldatlas.com/img/flag/tl-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (175, N'https://www.worldatlas.com/img/flag/tg-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (176, N'https://www.worldatlas.com/img/flag/to-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (177, N'https://www.worldatlas.com/img/flag/tt-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (178, N'https://www.worldatlas.com/img/flag/tn-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (179, N'https://www.worldatlas.com/img/flag/tr-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (180, N'https://www.worldatlas.com/img/flag/tm-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (181, N'https://www.worldatlas.com/img/flag/tv-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (182, N'https://www.worldatlas.com/img/flag/ug-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (183, N'https://www.worldatlas.com/img/flag/ua-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (184, N'https://www.worldatlas.com/img/flag/ae-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (185, N'https://www.worldatlas.com/img/flag/gb-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (186, N'https://www.worldatlas.com/img/flag/us-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (187, N'https://www.worldatlas.com/img/flag/uy-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (188, N'https://www.worldatlas.com/img/flag/uz-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (189, N'https://www.worldatlas.com/img/flag/vu-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (190, N'https://www.worldatlas.com/img/flag/ve-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (191, N'https://www.worldatlas.com/img/flag/vn-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (192, N'https://www.worldatlas.com/img/flag/ye-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (193, N'https://www.worldatlas.com/img/flag/zm-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (194, N'https://www.worldatlas.com/img/flag/zw-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (195, N'https://www.worldatlas.com/img/flag/ps-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (196, N'https://www.worldatlas.com/img/flag/va-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (197, N'https://www.worldatlas.com/img/flag/as-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (198, N'https://www.worldatlas.com/img/flag/ai-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (199, N'https://www.worldatlas.com/img/flag/aw-flag.jpg')
GO
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (200, N'https://www.worldatlas.com/img/flag/bm-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (201, N'https://www.worldatlas.com/img/flag/vg-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (202, N'https://www.worldatlas.com/img/flag/ky-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (203, N'https://www.worldatlas.com/img/flag/fk-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (204, N'https://www.worldatlas.com/img/flag/gf-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (205, N'https://www.worldatlas.com/img/flag/pf-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (206, N'https://www.worldatlas.com/img/flag/gi-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (207, N'https://www.worldatlas.com/img/flag/gl-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (208, N'https://www.worldatlas.com/img/flag/gp-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (209, N'https://www.worldatlas.com/img/flag/gu-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (210, N'https://www.worldatlas.com/img/flag/hk-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (211, N'https://www.worldatlas.com/img/flag/mo-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (212, N'https://www.worldatlas.com/img/flag/ms-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (213, N'https://www.worldatlas.com/img/flag/nc-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (214, N'https://www.worldatlas.com/img/flag/nu-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (215, N'https://www.worldatlas.com/img/flag/nf-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (216, N'https://www.worldatlas.com/img/flag/mp-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (217, N'https://www.worldatlas.com/img/flag/pn-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (218, N'https://www.worldatlas.com/img/flag/pr-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (219, N'https://www.worldatlas.com/img/flag/re-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (220, N'https://www.worldatlas.com/img/flag/tw-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (221, N'https://www.worldatlas.com/img/flag/tk-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (222, N'https://www.worldatlas.com/img/flag/tc-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (223, N'https://www.worldatlas.com/img/flag/vi-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (224, N'https://www.worldatlas.com/img/flag/wf-flag.jpg')
INSERT [dbo].[Flag] ([FlagId], [URL]) VALUES (225, N'https://www.worldatlas.com/img/flag/eh-flag.jpg')
SET IDENTITY_INSERT [dbo].[Flag] OFF
GO
SET IDENTITY_INSERT [dbo].[OweSinglePayment] ON 

INSERT [dbo].[OweSinglePayment] ([OweSinglePaymentId], [PersonName], [PaymentAmount], [PaymentStatus], [PaymentDate], [IsPayer], [ExpenseId]) VALUES (6, N'string', CAST(20 AS Decimal(18, 0)), 1, CAST(N'2022-11-29T19:37:22.000' AS DateTime), 1, 4)
INSERT [dbo].[OweSinglePayment] ([OweSinglePaymentId], [PersonName], [PaymentAmount], [PaymentStatus], [PaymentDate], [IsPayer], [ExpenseId]) VALUES (7, N'string', CAST(20 AS Decimal(18, 0)), 1, CAST(N'2022-11-29T19:37:22.000' AS DateTime), 1, 5)
INSERT [dbo].[OweSinglePayment] ([OweSinglePaymentId], [PersonName], [PaymentAmount], [PaymentStatus], [PaymentDate], [IsPayer], [ExpenseId]) VALUES (8, N'string', CAST(20 AS Decimal(18, 0)), 1, CAST(N'2022-11-29T19:37:22.000' AS DateTime), 1, 6)
INSERT [dbo].[OweSinglePayment] ([OweSinglePaymentId], [PersonName], [PaymentAmount], [PaymentStatus], [PaymentDate], [IsPayer], [ExpenseId]) VALUES (9, N'string', CAST(20 AS Decimal(18, 0)), 1, CAST(N'2022-11-29T19:37:22.000' AS DateTime), 1, 7)
INSERT [dbo].[OweSinglePayment] ([OweSinglePaymentId], [PersonName], [PaymentAmount], [PaymentStatus], [PaymentDate], [IsPayer], [ExpenseId]) VALUES (10, N'string', CAST(20 AS Decimal(18, 0)), 1, CAST(N'2022-11-29T19:37:22.000' AS DateTime), 1, 8)
INSERT [dbo].[OweSinglePayment] ([OweSinglePaymentId], [PersonName], [PaymentAmount], [PaymentStatus], [PaymentDate], [IsPayer], [ExpenseId]) VALUES (11, N'string', CAST(20 AS Decimal(18, 0)), 1, CAST(N'2022-11-29T19:37:22.000' AS DateTime), 1, 9)
INSERT [dbo].[OweSinglePayment] ([OweSinglePaymentId], [PersonName], [PaymentAmount], [PaymentStatus], [PaymentDate], [IsPayer], [ExpenseId]) VALUES (12, N'string', CAST(20 AS Decimal(18, 0)), 1, CAST(N'2022-11-29T19:37:22.000' AS DateTime), 1, 10)
INSERT [dbo].[OweSinglePayment] ([OweSinglePaymentId], [PersonName], [PaymentAmount], [PaymentStatus], [PaymentDate], [IsPayer], [ExpenseId]) VALUES (55, N'John', CAST(15 AS Decimal(18, 0)), 0, CAST(N'2023-01-28T10:59:26.540' AS DateTime), 1, 91)
INSERT [dbo].[OweSinglePayment] ([OweSinglePaymentId], [PersonName], [PaymentAmount], [PaymentStatus], [PaymentDate], [IsPayer], [ExpenseId]) VALUES (57, N'Janek', CAST(47 AS Decimal(18, 0)), 1, CAST(N'2023-01-28T23:48:42.243' AS DateTime), 1, 96)
INSERT [dbo].[OweSinglePayment] ([OweSinglePaymentId], [PersonName], [PaymentAmount], [PaymentStatus], [PaymentDate], [IsPayer], [ExpenseId]) VALUES (58, N'Jacek', CAST(50 AS Decimal(18, 0)), 1, CAST(N'2023-01-28T23:48:42.243' AS DateTime), 1, 97)
INSERT [dbo].[OweSinglePayment] ([OweSinglePaymentId], [PersonName], [PaymentAmount], [PaymentStatus], [PaymentDate], [IsPayer], [ExpenseId]) VALUES (59, N'Franek', CAST(50 AS Decimal(18, 0)), 1, CAST(N'2023-01-28T23:48:42.243' AS DateTime), 1, 98)
INSERT [dbo].[OweSinglePayment] ([OweSinglePaymentId], [PersonName], [PaymentAmount], [PaymentStatus], [PaymentDate], [IsPayer], [ExpenseId]) VALUES (60, N'Annn', CAST(10 AS Decimal(18, 0)), 0, CAST(N'2023-01-29T13:44:41.837' AS DateTime), 1, 101)
SET IDENTITY_INSERT [dbo].[OweSinglePayment] OFF
GO
SET IDENTITY_INSERT [dbo].[ServicePhone] ON 

INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (1, N'119')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (2, N'110')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (3, N'112')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (4, N'911')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (5, N'999')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (6, N'118')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (7, N'866 551-3')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (8, N'166')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (9, N'19')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (10, N'17')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (11, N'101')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (12, N'103')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (13, N'02')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (14, N'113')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (15, N'122')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (16, N'993')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (17, N'117')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (18, N'132')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (19, N'133')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (20, N'180')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (21, N'26811')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (22, N'158')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (23, N'91')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (24, N'1730')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (25, N'022')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (26, N'114')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (27, N'100')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (28, N'192')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (29, N'03')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (30, N'994')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (31, N'777')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (32, N'123')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (33, N'193')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (34, N'92')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (35, N'997')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (36, N'18')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (37, N'625 8666')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (38, N'060')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (39, N'902')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (40, N'102')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (41, N'94')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (42, N'199')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (43, N'1011')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (44, N'111')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (45, N'8117')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (46, N'15')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (47, N'104')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (48, N'000')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (49, N'00')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (50, N'011')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (51, N'10111')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (52, N'699935')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (53, N'191')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (54, N'197')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (55, N'171')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (56, N'194')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (57, N'977')
INSERT [dbo].[ServicePhone] ([ServicePhoneId], [Number]) VALUES (58, N'150')
SET IDENTITY_INSERT [dbo].[ServicePhone] OFF
GO
SET IDENTITY_INSERT [dbo].[Spot] ON 

INSERT [dbo].[Spot] ([SpotId], [Note], [Adress], [VisitDateId], [ExpenseId], [CoordinateX], [CoordinateY], [Name]) VALUES (79, N'dzyn dzyn', N'Big Ben, Londyn, Wielka Brytania', 194, 76, CAST(-0.1200000 AS Decimal(10, 7)), CAST(51.5000000 AS Decimal(10, 7)), N'BigBen')
INSERT [dbo].[Spot] ([SpotId], [Note], [Adress], [VisitDateId], [ExpenseId], [CoordinateX], [CoordinateY], [Name]) VALUES (80, N'aaaa', N'Miami, Floryda, Stany Zjednoczone', 219, 77, CAST(-80.1900000 AS Decimal(10, 7)), CAST(25.7600000 AS Decimal(10, 7)), N'aaaa')
INSERT [dbo].[Spot] ([SpotId], [Note], [Adress], [VisitDateId], [ExpenseId], [CoordinateX], [CoordinateY], [Name]) VALUES (86, N'', N'Yoyogi, Shibuya, Tokio, Japonia', 239, 83, CAST(139.6900000 AS Decimal(10, 7)), CAST(35.6800000 AS Decimal(10, 7)), N'Yoyoki')
INSERT [dbo].[Spot] ([SpotId], [Note], [Adress], [VisitDateId], [ExpenseId], [CoordinateX], [CoordinateY], [Name]) VALUES (88, N'dd', N'Eiffel Tower, Avenue Anatole France, Paryz, Francja', 250, 85, CAST(2.2900000 AS Decimal(10, 7)), CAST(48.8600000 AS Decimal(10, 7)), N'Old Town day 1')
INSERT [dbo].[Spot] ([SpotId], [Note], [Adress], [VisitDateId], [ExpenseId], [CoordinateX], [CoordinateY], [Name]) VALUES (89, N'', N'Luwr, Rue de Rivoli, Paryz, Francja', 250, 86, CAST(2.3400000 AS Decimal(10, 7)), CAST(48.8600000 AS Decimal(10, 7)), N'Museum')
INSERT [dbo].[Spot] ([SpotId], [Note], [Adress], [VisitDateId], [ExpenseId], [CoordinateX], [CoordinateY], [Name]) VALUES (90, N'PKiN', N'Palac Kultury i Nauki, plac Defilad, Warsaw, Poland', 258, 87, CAST(21.0100000 AS Decimal(10, 7)), CAST(52.2300000 AS Decimal(10, 7)), N'Zwiedzanie')
INSERT [dbo].[Spot] ([SpotId], [Note], [Adress], [VisitDateId], [ExpenseId], [CoordinateX], [CoordinateY], [Name]) VALUES (91, N'Zwiedzanie', N'Stare Miasto, Warszawa, Poland', 258, 88, CAST(21.0100000 AS Decimal(10, 7)), CAST(52.2500000 AS Decimal(10, 7)), N'Stare Miasto')
INSERT [dbo].[Spot] ([SpotId], [Note], [Adress], [VisitDateId], [ExpenseId], [CoordinateX], [CoordinateY], [Name]) VALUES (92, N'', N'PKiN, plac Defilad, Warszawa, Polska', 264, 89, CAST(21.0100000 AS Decimal(10, 7)), CAST(52.2300000 AS Decimal(10, 7)), N'Pekin')
INSERT [dbo].[Spot] ([SpotId], [Note], [Adress], [VisitDateId], [ExpenseId], [CoordinateX], [CoordinateY], [Name]) VALUES (93, N'', N'Centrum Nauki Kopernik, Wybrzeze Kosciuszkowskie, Warszawa, Polska', 264, 90, CAST(21.0300000 AS Decimal(10, 7)), CAST(52.2400000 AS Decimal(10, 7)), N'Kopernik')
INSERT [dbo].[Spot] ([SpotId], [Note], [Adress], [VisitDateId], [ExpenseId], [CoordinateX], [CoordinateY], [Name]) VALUES (94, N'', N'Multikino Zlote Tarasy, Zlote Tarasy, Zlota, Warszawa, Polska', 264, 91, CAST(21.0000000 AS Decimal(10, 7)), CAST(52.2300000 AS Decimal(10, 7)), N'Kino')
INSERT [dbo].[Spot] ([SpotId], [Note], [Adress], [VisitDateId], [ExpenseId], [CoordinateX], [CoordinateY], [Name]) VALUES (95, N'', N'Stare Miasto, Poznan, Poland', 265, 92, CAST(0.0000000 AS Decimal(10, 7)), CAST(0.0000000 AS Decimal(10, 7)), N'')
INSERT [dbo].[Spot] ([SpotId], [Note], [Adress], [VisitDateId], [ExpenseId], [CoordinateX], [CoordinateY], [Name]) VALUES (96, N'', N'Kraków, Poland', 269, 93, CAST(19.9400000 AS Decimal(10, 7)), CAST(50.0600000 AS Decimal(10, 7)), N'')
INSERT [dbo].[Spot] ([SpotId], [Note], [Adress], [VisitDateId], [ExpenseId], [CoordinateX], [CoordinateY], [Name]) VALUES (97, N'efef', N'Eiffel Tower Bahria Town Lahore, Bahria Town Main Boulevard, Violet Block Bahria Town, Lahaur, Pakistan', 250, 96, CAST(74.1900000 AS Decimal(10, 7)), CAST(31.3600000 AS Decimal(10, 7)), N'efef')
INSERT [dbo].[Spot] ([SpotId], [Note], [Adress], [VisitDateId], [ExpenseId], [CoordinateX], [CoordinateY], [Name]) VALUES (98, N'', N'Sukiennice, Rynek Glówny, Kraków, Polska', 271, 97, CAST(19.9400000 AS Decimal(10, 7)), CAST(50.0600000 AS Decimal(10, 7)), N'First')
INSERT [dbo].[Spot] ([SpotId], [Note], [Adress], [VisitDateId], [ExpenseId], [CoordinateX], [CoordinateY], [Name]) VALUES (99, N'test2', N'Rynek Glówny, Rynek Glówny, Kraków, Polska', 271, 98, CAST(19.9400000 AS Decimal(10, 7)), CAST(50.0600000 AS Decimal(10, 7)), N'test2')
INSERT [dbo].[Spot] ([SpotId], [Note], [Adress], [VisitDateId], [ExpenseId], [CoordinateX], [CoordinateY], [Name]) VALUES (100, N'', N'', 265, 99, CAST(0.0000000 AS Decimal(10, 7)), CAST(0.0000000 AS Decimal(10, 7)), N'')
INSERT [dbo].[Spot] ([SpotId], [Note], [Adress], [VisitDateId], [ExpenseId], [CoordinateX], [CoordinateY], [Name]) VALUES (101, N'M', N'Melbourne VIC, Australia', 224, 100, CAST(144.9600000 AS Decimal(10, 7)), CAST(-37.8100000 AS Decimal(10, 7)), N'Muzea')
INSERT [dbo].[Spot] ([SpotId], [Note], [Adress], [VisitDateId], [ExpenseId], [CoordinateX], [CoordinateY], [Name]) VALUES (102, N'T', N'Dolphin Mall, Northwest 12th Street, Miami, FL, USA', 225, 101, CAST(-80.3800000 AS Decimal(10, 7)), CAST(25.7900000 AS Decimal(10, 7)), N'Test')
INSERT [dbo].[Spot] ([SpotId], [Note], [Adress], [VisitDateId], [ExpenseId], [CoordinateX], [CoordinateY], [Name]) VALUES (103, N'', N'Lódz, Poland', 263, 102, CAST(19.4600000 AS Decimal(10, 7)), CAST(51.7600000 AS Decimal(10, 7)), N'')
INSERT [dbo].[Spot] ([SpotId], [Note], [Adress], [VisitDateId], [ExpenseId], [CoordinateX], [CoordinateY], [Name]) VALUES (133, N'', N'Hotel Narvil Conference & Spa, Czeslawa Milosza, Serock, Polska', 301, 132, CAST(21.0700000 AS Decimal(10, 7)), CAST(52.5000000 AS Decimal(10, 7)), N'Hotel')
INSERT [dbo].[Spot] ([SpotId], [Note], [Adress], [VisitDateId], [ExpenseId], [CoordinateX], [CoordinateY], [Name]) VALUES (134, N'', N'Biedronka, Warszawska, Serock, Polska', 301, 133, CAST(21.0700000 AS Decimal(10, 7)), CAST(52.5100000 AS Decimal(10, 7)), N'Sklep')
SET IDENTITY_INSERT [dbo].[Spot] OFF
GO
SET IDENTITY_INSERT [dbo].[SystemNotification] ON 

INSERT [dbo].[SystemNotification] ([SystemNotificationId], [Content], [CreatedDate], [ValidDate], [Title], [Type]) VALUES (1, N'Planning system shutdown due to backing up data', CAST(N'2022-05-26T19:37:00.000' AS DateTime), CAST(N'2023-01-12T00:00:00.000' AS DateTime), N'Maintenance work', N'info')
INSERT [dbo].[SystemNotification] ([SystemNotificationId], [Content], [CreatedDate], [ValidDate], [Title], [Type]) VALUES (2, N'Please change your account password due to utmost importance of security', CAST(N'2022-05-26T19:37:00.000' AS DateTime), CAST(N'2023-02-12T00:00:00.000' AS DateTime), N'Security breach', N'info')
INSERT [dbo].[SystemNotification] ([SystemNotificationId], [Content], [CreatedDate], [ValidDate], [Title], [Type]) VALUES (3, N'Planning system shutdown due to modification of database', CAST(N'2022-04-25T19:37:00.000' AS DateTime), CAST(N'2023-02-12T00:00:00.000' AS DateTime), N'Maintenance work', N'info')
INSERT [dbo].[SystemNotification] ([SystemNotificationId], [Content], [CreatedDate], [ValidDate], [Title], [Type]) VALUES (4, N'Planning system shutdown to ensure that data is consistent', CAST(N'2022-09-27T19:37:00.000' AS DateTime), CAST(N'2023-06-12T00:00:00.000' AS DateTime), N'Maintenance work', N'info')
SET IDENTITY_INSERT [dbo].[SystemNotification] OFF
GO
SET IDENTITY_INSERT [dbo].[Travel] ON 

INSERT [dbo].[Travel] ([TravelId], [Name], [Destination], [StartDate], [EndDate], [Note], [PlannedBudget], [CreatedDate], [UserId], [CountryId], [HotelName], [PickedCurrency], [HotelStreet], [HotelBuildingNo], [HotelFlatNo], [HotelZipCode], [HotelCity]) VALUES (123, N'TestUK', N'London', CAST(N'2022-12-31' AS Date), CAST(N'2023-01-01' AS Date), NULL, CAST(2000 AS Decimal(18, 0)), CAST(N'2023-01-03T00:21:16.720' AS DateTime), 1, 185, N'Hyatt', N'EUR', N'London', 11, 91, NULL, N'London')
INSERT [dbo].[Travel] ([TravelId], [Name], [Destination], [StartDate], [EndDate], [Note], [PlannedBudget], [CreatedDate], [UserId], [CountryId], [HotelName], [PickedCurrency], [HotelStreet], [HotelBuildingNo], [HotelFlatNo], [HotelZipCode], [HotelCity]) VALUES (126, N'Test', N'USA', CAST(N'2023-01-03' AS Date), CAST(N'2023-01-06' AS Date), NULL, CAST(3000 AS Decimal(18, 0)), CAST(N'2023-01-03T00:52:18.327' AS DateTime), 1, 186, N'', N'USD', N'', 0, 0, NULL, N'')
INSERT [dbo].[Travel] ([TravelId], [Name], [Destination], [StartDate], [EndDate], [Note], [PlannedBudget], [CreatedDate], [UserId], [CountryId], [HotelName], [PickedCurrency], [HotelStreet], [HotelBuildingNo], [HotelFlatNo], [HotelZipCode], [HotelCity]) VALUES (128, N'Trip', N'Warszawa', CAST(N'2023-01-30' AS Date), CAST(N'2023-01-31' AS Date), NULL, CAST(0 AS Decimal(18, 0)), CAST(N'2023-01-04T04:42:35.213' AS DateTime), 32, 139, N'Marriott', N'EUR', N'al. Jerozolimskie ', 65, 79, N'00-697', N'Warszawa')
INSERT [dbo].[Travel] ([TravelId], [Name], [Destination], [StartDate], [EndDate], [Note], [PlannedBudget], [CreatedDate], [UserId], [CountryId], [HotelName], [PickedCurrency], [HotelStreet], [HotelBuildingNo], [HotelFlatNo], [HotelZipCode], [HotelCity]) VALUES (133, N'Japan', N'Tokyo', CAST(N'2023-01-11' AS Date), CAST(N'2023-01-18' AS Date), NULL, CAST(10000 AS Decimal(18, 0)), CAST(N'2023-01-09T15:46:25.733' AS DateTime), 1, 88, N'Hyatt', N'JPY', N'rgrg', 13, 3, NULL, N'Tokyo')
INSERT [dbo].[Travel] ([TravelId], [Name], [Destination], [StartDate], [EndDate], [Note], [PlannedBudget], [CreatedDate], [UserId], [CountryId], [HotelName], [PickedCurrency], [HotelStreet], [HotelBuildingNo], [HotelFlatNo], [HotelZipCode], [HotelCity]) VALUES (135, N'Greece', N'Athens', CAST(N'2023-07-17' AS Date), CAST(N'2023-07-22' AS Date), NULL, CAST(3000 AS Decimal(18, 0)), CAST(N'2023-01-21T12:02:16.727' AS DateTime), 1, 70, N'Hyatt', N'EUR', N'Athens Central', 30, 1, N'01-123', N'Athens')
INSERT [dbo].[Travel] ([TravelId], [Name], [Destination], [StartDate], [EndDate], [Note], [PlannedBudget], [CreatedDate], [UserId], [CountryId], [HotelName], [PickedCurrency], [HotelStreet], [HotelBuildingNo], [HotelFlatNo], [HotelZipCode], [HotelCity]) VALUES (138, N'Wycieczka', N'Warszawa', CAST(N'2023-01-19' AS Date), CAST(N'2023-01-27' AS Date), NULL, CAST(1000 AS Decimal(18, 0)), CAST(N'2023-01-24T21:24:42.480' AS DateTime), 32, 139, N'Holiday', N'PLN', N'Al. Jerozolimskie', 5, 1, NULL, N'Warszawa')
INSERT [dbo].[Travel] ([TravelId], [Name], [Destination], [StartDate], [EndDate], [Note], [PlannedBudget], [CreatedDate], [UserId], [CountryId], [HotelName], [PickedCurrency], [HotelStreet], [HotelBuildingNo], [HotelFlatNo], [HotelZipCode], [HotelCity]) VALUES (140, N'njknnkj', N'njknjk', CAST(N'2023-01-23' AS Date), CAST(N'2023-01-28' AS Date), NULL, CAST(880 AS Decimal(18, 0)), CAST(N'2023-01-25T00:34:58.023' AS DateTime), 32, 8, N'Jakis', N'BGN', N'Piotrkowska', 1, 1, NULL, N'Lódz')
INSERT [dbo].[Travel] ([TravelId], [Name], [Destination], [StartDate], [EndDate], [Note], [PlannedBudget], [CreatedDate], [UserId], [CountryId], [HotelName], [PickedCurrency], [HotelStreet], [HotelBuildingNo], [HotelFlatNo], [HotelZipCode], [HotelCity]) VALUES (141, N'Stolica', N'Warsaw', CAST(N'2023-01-29' AS Date), CAST(N'2023-01-29' AS Date), NULL, CAST(100 AS Decimal(18, 0)), CAST(N'2023-01-28T11:02:47.920' AS DateTime), 1, 139, N'Marriott', N'PLN', N'Jerozolimskie', 12, 0, N'02-654', N'Warsaw')
INSERT [dbo].[Travel] ([TravelId], [Name], [Destination], [StartDate], [EndDate], [Note], [PlannedBudget], [CreatedDate], [UserId], [CountryId], [HotelName], [PickedCurrency], [HotelStreet], [HotelBuildingNo], [HotelFlatNo], [HotelZipCode], [HotelCity]) VALUES (142, N'bbb', N'bb', CAST(N'2023-01-26' AS Date), CAST(N'2023-01-29' AS Date), NULL, CAST(0 AS Decimal(18, 0)), CAST(N'2023-01-28T17:08:16.713' AS DateTime), 32, 4, N'', N'AOA', N'', 0, 0, N'', N'')
INSERT [dbo].[Travel] ([TravelId], [Name], [Destination], [StartDate], [EndDate], [Note], [PlannedBudget], [CreatedDate], [UserId], [CountryId], [HotelName], [PickedCurrency], [HotelStreet], [HotelBuildingNo], [HotelFlatNo], [HotelZipCode], [HotelCity]) VALUES (143, N'bbb', N'Kraków', CAST(N'2023-01-28' AS Date), CAST(N'2023-01-29' AS Date), NULL, CAST(0 AS Decimal(18, 0)), CAST(N'2023-01-28T20:56:28.017' AS DateTime), 32, 139, N'Hello', N'PLN', N'Warszawska', 10, 0, N'', N'Kraków')
INSERT [dbo].[Travel] ([TravelId], [Name], [Destination], [StartDate], [EndDate], [Note], [PlannedBudget], [CreatedDate], [UserId], [CountryId], [HotelName], [PickedCurrency], [HotelStreet], [HotelBuildingNo], [HotelFlatNo], [HotelZipCode], [HotelCity]) VALUES (144, N'test', N'Kraków', CAST(N'2023-01-28' AS Date), CAST(N'2023-01-28' AS Date), NULL, CAST(100 AS Decimal(18, 0)), CAST(N'2023-01-29T00:12:52.373' AS DateTime), 1, 139, N'Hyatt', N'PLN', N'Street', 1, 1, N'01-987', N'Krakow')
INSERT [dbo].[Travel] ([TravelId], [Name], [Destination], [StartDate], [EndDate], [Note], [PlannedBudget], [CreatedDate], [UserId], [CountryId], [HotelName], [PickedCurrency], [HotelStreet], [HotelBuildingNo], [HotelFlatNo], [HotelZipCode], [HotelCity]) VALUES (148, N'DD23', N'Serock', CAST(N'2023-02-03' AS Date), CAST(N'2023-02-04' AS Date), NULL, CAST(100 AS Decimal(18, 0)), CAST(N'2023-01-30T09:42:48.207' AS DateTime), 1, 139, N'Narvil', N'PLN', N'Jakas', 1, 100, N'02-345', N'Serock')
INSERT [dbo].[Travel] ([TravelId], [Name], [Destination], [StartDate], [EndDate], [Note], [PlannedBudget], [CreatedDate], [UserId], [CountryId], [HotelName], [PickedCurrency], [HotelStreet], [HotelBuildingNo], [HotelFlatNo], [HotelZipCode], [HotelCity]) VALUES (159, N'Podróz 1', N'Tokyo', CAST(N'2023-02-04' AS Date), CAST(N'2023-02-28' AS Date), NULL, CAST(40000 AS Decimal(18, 0)), CAST(N'2023-02-04T10:09:12.023' AS DateTime), 193, 88, N'Mariott', N'ALL', N'Warsaw', 23, 222, N'03-232', N'japan')
SET IDENTITY_INSERT [dbo].[Travel] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([UserId], [Name], [Surname], [PhoneNumber], [Email], [Password], [PasswordDateUpdated], [DateCreated]) VALUES (1, N'Adam', N'Testowy', N'123456789', N'adam.testowy@gmail.com', N'47-32-87-F8-29-8D-BA-71-63-A8-97-90-89-58-F7-C0-EA-E7-33-E2-5D-2E-02-79-92-EA-2E-DC-9B-ED-2F-A8', CAST(N'2022-09-20T13:27:18.767' AS DateTime), CAST(N'2022-05-22T17:36:00.000' AS DateTime))
INSERT [dbo].[User] ([UserId], [Name], [Surname], [PhoneNumber], [Email], [Password], [PasswordDateUpdated], [DateCreated]) VALUES (32, N'string', N'string', N'123456789', N'coyal72958@cosaxu.com', N'65-E8-4B-E3-35-32-FB-78-4C-48-12-96-75-F9-EF-F3-A6-82-B2-71-68-C0-EA-74-4B-2C-F5-8E-E0-23-37-C5', CAST(N'2022-12-10T19:05:47.363' AS DateTime), CAST(N'2022-12-10T19:05:47.363' AS DateTime))
INSERT [dbo].[User] ([UserId], [Name], [Surname], [PhoneNumber], [Email], [Password], [PasswordDateUpdated], [DateCreated]) VALUES (170, N'Prywatny', N'Testowy', N'123456789', N'ma.pluta356@gmail.com', N'D0-4B-98-F4-8E-8F-8B-CC-15-C6-AE-5A-C0-50-80-1C-D6-DC-FD-42-8F-B5-F9-E6-5C-4E-16-E7-80-73-40-FA', CAST(N'2023-01-25T23:47:15.217' AS DateTime), CAST(N'2023-01-25T23:47:15.217' AS DateTime))
INSERT [dbo].[User] ([UserId], [Name], [Surname], [PhoneNumber], [Email], [Password], [PasswordDateUpdated], [DateCreated]) VALUES (171, N'Gg', N'Kk', N'', N'kkk@kkkk.com', N'52-C7-E2-49-6D-48-8C-5F-56-39-61-02-AB-E6-A5-BC-EB-12-1C-C9-F4-72-00-42-46-A8-38-AB-D8-FD-DF-F8', CAST(N'2023-01-26T21:51:47.147' AS DateTime), CAST(N'2023-01-26T21:51:47.147' AS DateTime))
INSERT [dbo].[User] ([UserId], [Name], [Surname], [PhoneNumber], [Email], [Password], [PasswordDateUpdated], [DateCreated]) VALUES (172, N'Gg', N'Kk', N'', N'kkk1@kkkk.com', N'52-C7-E2-49-6D-48-8C-5F-56-39-61-02-AB-E6-A5-BC-EB-12-1C-C9-F4-72-00-42-46-A8-38-AB-D8-FD-DF-F8', CAST(N'2023-01-26T21:54:26.910' AS DateTime), CAST(N'2023-01-26T21:54:26.910' AS DateTime))
INSERT [dbo].[User] ([UserId], [Name], [Surname], [PhoneNumber], [Email], [Password], [PasswordDateUpdated], [DateCreated]) VALUES (175, N'mm', N'ms', N'', N'kkk2@kkkk.com', N'52-C7-E2-49-6D-48-8C-5F-56-39-61-02-AB-E6-A5-BC-EB-12-1C-C9-F4-72-00-42-46-A8-38-AB-D8-FD-DF-F8', CAST(N'2023-01-26T21:55:00.710' AS DateTime), CAST(N'2023-01-26T21:55:00.710' AS DateTime))
INSERT [dbo].[User] ([UserId], [Name], [Surname], [PhoneNumber], [Email], [Password], [PasswordDateUpdated], [DateCreated]) VALUES (176, N'mm', N'kk', N'', N'kkk3@kkkk.com', N'52-C7-E2-49-6D-48-8C-5F-56-39-61-02-AB-E6-A5-BC-EB-12-1C-C9-F4-72-00-42-46-A8-38-AB-D8-FD-DF-F8', CAST(N'2023-01-26T22:35:40.927' AS DateTime), CAST(N'2023-01-26T22:35:40.927' AS DateTime))
INSERT [dbo].[User] ([UserId], [Name], [Surname], [PhoneNumber], [Email], [Password], [PasswordDateUpdated], [DateCreated]) VALUES (177, N'Adam', N'Afam', N'', N'eae@gg.gg', N'E8-02-C5-EE-D6-E4-AA-F6-77-85-0B-BD-C2-31-E7-E4-B5-70-D1-04-CF-BF-DA-74-B0-00-56-60-16-48-07-36', CAST(N'2023-01-26T22:43:38.467' AS DateTime), CAST(N'2023-01-26T22:43:38.467' AS DateTime))
INSERT [dbo].[User] ([UserId], [Name], [Surname], [PhoneNumber], [Email], [Password], [PasswordDateUpdated], [DateCreated]) VALUES (178, N'mmm', N'kmm', N'', N'mm@mmmma.com', N'52-C7-E2-49-6D-48-8C-5F-56-39-61-02-AB-E6-A5-BC-EB-12-1C-C9-F4-72-00-42-46-A8-38-AB-D8-FD-DF-F8', CAST(N'2023-01-26T23:32:37.750' AS DateTime), CAST(N'2023-01-26T23:32:37.750' AS DateTime))
INSERT [dbo].[User] ([UserId], [Name], [Surname], [PhoneNumber], [Email], [Password], [PasswordDateUpdated], [DateCreated]) VALUES (180, N'mmmm', N'uuu', N'', N'xdx@gvv.com', N'52-C7-E2-49-6D-48-8C-5F-56-39-61-02-AB-E6-A5-BC-EB-12-1C-C9-F4-72-00-42-46-A8-38-AB-D8-FD-DF-F8', CAST(N'2023-01-27T14:49:26.037' AS DateTime), CAST(N'2023-01-27T14:49:26.037' AS DateTime))
INSERT [dbo].[User] ([UserId], [Name], [Surname], [PhoneNumber], [Email], [Password], [PasswordDateUpdated], [DateCreated]) VALUES (181, N'Test', N'ttt', N'', N'gyggyu22@huhu.com', N'A9-48-5A-96-04-B4-CC-C7-C4-C8-97-9F-4D-70-89-31-7F-72-78-45-95-AD-15-7D-5F-E8-86-29-93-C2-70-2F', CAST(N'2023-01-29T17:43:00.123' AS DateTime), CAST(N'2023-01-29T17:43:00.123' AS DateTime))
INSERT [dbo].[User] ([UserId], [Name], [Surname], [PhoneNumber], [Email], [Password], [PasswordDateUpdated], [DateCreated]) VALUES (182, N'mm', N'mm', N'', N'm1@m1.com', N'56-FF-D3-19-F5-20-70-28-1D-8F-4C-3E-24-07-C2-99-0E-D2-B7-92-24-8D-8C-89-66-C5-89-D7-DF-5D-C7-D9', CAST(N'2023-01-29T17:48:44.950' AS DateTime), CAST(N'2023-01-29T17:48:44.950' AS DateTime))
INSERT [dbo].[User] ([UserId], [Name], [Surname], [PhoneNumber], [Email], [Password], [PasswordDateUpdated], [DateCreated]) VALUES (184, N'Aaa', N'bbb', N'333222111', N'pgago2@pjwstk.edu.pl', N'72-68-E2-D0-05-F2-BE-55-A9-12-85-70-A3-17-95-92-11-E4-F2-C3-C4-5A-0F-FF-1C-28-F9-2D-32-69-F5-4E', CAST(N'2023-01-29T19:42:12.767' AS DateTime), CAST(N'2023-01-29T19:42:12.767' AS DateTime))
INSERT [dbo].[User] ([UserId], [Name], [Surname], [PhoneNumber], [Email], [Password], [PasswordDateUpdated], [DateCreated]) VALUES (190, N'Goo', N'Piii', N'', N'bhjjbhbhj@hbbjh.com', N'BA-21-BF-26-3A-8D-C2-D9-29-F2-98-58-52-45-AA-4A-53-FB-EF-41-42-3A-D1-CF-7C-BE-6A-22-03-E8-F3-81', CAST(N'2023-02-01T21:52:52.973' AS DateTime), CAST(N'2023-02-01T21:52:52.973' AS DateTime))
INSERT [dbo].[User] ([UserId], [Name], [Surname], [PhoneNumber], [Email], [Password], [PasswordDateUpdated], [DateCreated]) VALUES (193, N'Piotr', N'Gago', N'333222222', N'pgago3@pjwstk.edu.pl', N'AD-D8-1E-4A-11-7D-72-04-41-D5-51-B7-73-38-3B-9E-48-27-AC-F4-E3-73-4A-A8-45-26-13-22-82-E3-91-B5', CAST(N'2023-02-04T09:58:46.307' AS DateTime), CAST(N'2023-02-04T09:58:46.307' AS DateTime))
INSERT [dbo].[User] ([UserId], [Name], [Surname], [PhoneNumber], [Email], [Password], [PasswordDateUpdated], [DateCreated]) VALUES (203, N'test', N'test', N'', N'ttttttt@nnt.com', N'BE-5F-5C-F4-0F-41-8D-28-1D-BE-BD-6D-7C-71-9B-2E-DB-CE-7D-83-21-A9-B3-29-42-59-DA-72-D9-87-A6-F3', CAST(N'2023-02-04T12:11:01.723' AS DateTime), CAST(N'2023-02-04T12:11:01.723' AS DateTime))
INSERT [dbo].[User] ([UserId], [Name], [Surname], [PhoneNumber], [Email], [Password], [PasswordDateUpdated], [DateCreated]) VALUES (204, N'bhbhhb', N'bhbh', N'', N'hh1bbh@njnjnj.com', N'BE-5F-5C-F4-0F-41-8D-28-1D-BE-BD-6D-7C-71-9B-2E-DB-CE-7D-83-21-A9-B3-29-42-59-DA-72-D9-87-A6-F3', CAST(N'2023-02-04T12:11:30.450' AS DateTime), CAST(N'2023-02-04T12:11:30.450' AS DateTime))
INSERT [dbo].[User] ([UserId], [Name], [Surname], [PhoneNumber], [Email], [Password], [PasswordDateUpdated], [DateCreated]) VALUES (205, N'bbhbh', N'bhbhb', N'', N'qwqq@njnj1.com', N'BE-5F-5C-F4-0F-41-8D-28-1D-BE-BD-6D-7C-71-9B-2E-DB-CE-7D-83-21-A9-B3-29-42-59-DA-72-D9-87-A6-F3', CAST(N'2023-02-04T12:11:53.230' AS DateTime), CAST(N'2023-02-04T12:11:53.230' AS DateTime))
INSERT [dbo].[User] ([UserId], [Name], [Surname], [PhoneNumber], [Email], [Password], [PasswordDateUpdated], [DateCreated]) VALUES (206, N'nnjnj', N'njjnj', N'', N'njj@njjnc.comd', N'BE-5F-5C-F4-0F-41-8D-28-1D-BE-BD-6D-7C-71-9B-2E-DB-CE-7D-83-21-A9-B3-29-42-59-DA-72-D9-87-A6-F3', CAST(N'2023-02-04T12:16:14.103' AS DateTime), CAST(N'2023-02-04T12:16:14.103' AS DateTime))
INSERT [dbo].[User] ([UserId], [Name], [Surname], [PhoneNumber], [Email], [Password], [PasswordDateUpdated], [DateCreated]) VALUES (208, N'mmmm', N'mm', N'', N'bhjhbhjbjh@jhbbhjjbh.com', N'BE-5F-5C-F4-0F-41-8D-28-1D-BE-BD-6D-7C-71-9B-2E-DB-CE-7D-83-21-A9-B3-29-42-59-DA-72-D9-87-A6-F3', CAST(N'2023-02-04T13:04:45.523' AS DateTime), CAST(N'2023-02-04T13:04:45.523' AS DateTime))
INSERT [dbo].[User] ([UserId], [Name], [Surname], [PhoneNumber], [Email], [Password], [PasswordDateUpdated], [DateCreated]) VALUES (209, N'bhjbhbj@2', N'bhhbj1212', N'', N'mmm@mmmm21.com', N'BE-5F-5C-F4-0F-41-8D-28-1D-BE-BD-6D-7C-71-9B-2E-DB-CE-7D-83-21-A9-B3-29-42-59-DA-72-D9-87-A6-F3', CAST(N'2023-02-04T13:08:50.183' AS DateTime), CAST(N'2023-02-04T13:08:50.183' AS DateTime))
INSERT [dbo].[User] ([UserId], [Name], [Surname], [PhoneNumber], [Email], [Password], [PasswordDateUpdated], [DateCreated]) VALUES (210, N'mmm', N'mmm', N'', N'mmm121@dssda.com', N'15-CF-E8-43-71-E4-8E-8D-CC-7C-A7-8F-FE-CC-B1-CE-91-B8-4F-26-C7-21-16-D7-47-3D-B1-E7-E2-09-48-AD', CAST(N'2023-02-04T14:40:09.290' AS DateTime), CAST(N'2023-02-04T14:40:09.290' AS DateTime))
INSERT [dbo].[User] ([UserId], [Name], [Surname], [PhoneNumber], [Email], [Password], [PasswordDateUpdated], [DateCreated]) VALUES (211, N'bhjjbhbh@bhjbhjbhj', N'jhbbhjbhjbhj', N'', N'jbhbjhbhjqbhjb@bhbjbhjbhj.com', N'15-CF-E8-43-71-E4-8E-8D-CC-7C-A7-8F-FE-CC-B1-CE-91-B8-4F-26-C7-21-16-D7-47-3D-B1-E7-E2-09-48-AD', CAST(N'2023-02-04T14:42:43.030' AS DateTime), CAST(N'2023-02-04T14:42:43.030' AS DateTime))
SET IDENTITY_INSERT [dbo].[User] OFF
GO
SET IDENTITY_INSERT [dbo].[VisitDate] ON 

INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (194, CAST(N'2022-12-31T23:00:00.000' AS DateTime), N'', 123)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (195, CAST(N'2023-01-01T23:00:00.000' AS DateTime), N'', 123)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (196, CAST(N'2023-01-28T23:41:32.000' AS DateTime), N'', 123)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (197, CAST(N'2023-01-29T23:41:32.000' AS DateTime), N'', 123)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (198, CAST(N'2023-01-30T23:41:32.000' AS DateTime), N'', 123)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (219, CAST(N'2023-01-03T00:49:37.777' AS DateTime), N'aaaa', 126)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (220, CAST(N'2023-01-04T00:49:37.777' AS DateTime), N'', 126)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (221, CAST(N'2023-01-05T00:49:37.777' AS DateTime), N'', 126)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (222, CAST(N'2023-01-06T00:49:37.777' AS DateTime), N'', 126)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (224, CAST(N'2023-01-30T00:00:00.000' AS DateTime), N'', 128)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (225, CAST(N'2023-01-31T00:00:00.000' AS DateTime), N'', 128)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (226, CAST(N'2023-01-09T00:00:00.000' AS DateTime), N'', 128)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (239, CAST(N'2023-01-11T15:43:25.000' AS DateTime), N'Day1', 133)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (240, CAST(N'2023-01-12T15:43:25.000' AS DateTime), N'', 133)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (241, CAST(N'2023-01-13T15:43:25.000' AS DateTime), N'', 133)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (242, CAST(N'2023-01-14T15:43:25.000' AS DateTime), N'', 133)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (243, CAST(N'2023-01-15T15:43:25.000' AS DateTime), N'', 133)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (244, CAST(N'2023-01-16T15:43:25.000' AS DateTime), N'', 133)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (245, CAST(N'2023-01-17T15:43:25.000' AS DateTime), N'', 133)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (246, CAST(N'2023-01-18T15:43:25.000' AS DateTime), N'', 133)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (250, CAST(N'2023-07-16T22:00:00.000' AS DateTime), N'Old Town', 135)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (251, CAST(N'2023-07-17T22:00:00.000' AS DateTime), N'', 135)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (252, CAST(N'2023-07-18T22:00:00.000' AS DateTime), N'', 135)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (253, CAST(N'2023-07-19T22:00:00.000' AS DateTime), N'', 135)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (254, CAST(N'2023-07-20T22:00:00.000' AS DateTime), N'', 135)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (255, CAST(N'2023-07-21T22:00:00.000' AS DateTime), N'', 135)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (258, CAST(N'2023-01-19T00:00:00.000' AS DateTime), N'', 138)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (259, CAST(N'2023-01-20T00:00:00.000' AS DateTime), N'', 138)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (260, CAST(N'2023-01-21T00:00:00.000' AS DateTime), N'', 138)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (261, CAST(N'2023-01-22T00:00:00.000' AS DateTime), N'', 138)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (263, CAST(N'2023-01-23T00:00:00.000' AS DateTime), N'', 140)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (264, CAST(N'2023-01-29T10:59:26.000' AS DateTime), N'', 141)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (265, CAST(N'2023-01-26T23:00:00.000' AS DateTime), N'', 142)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (266, CAST(N'2023-01-27T23:00:00.000' AS DateTime), N'', 142)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (267, CAST(N'2023-01-28T23:00:00.000' AS DateTime), N'', 142)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (268, CAST(N'2023-01-29T23:00:00.000' AS DateTime), N'', 142)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (269, CAST(N'2023-01-27T23:00:00.000' AS DateTime), N'', 143)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (270, CAST(N'2023-01-28T23:00:00.000' AS DateTime), N'', 143)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (271, CAST(N'2023-01-27T23:00:00.000' AS DateTime), N'test', 144)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (272, CAST(N'2023-01-30T23:00:00.000' AS DateTime), N'', 142)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (273, CAST(N'2023-02-01T23:00:00.000' AS DateTime), N'', 142)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (274, CAST(N'2023-02-01T23:00:00.000' AS DateTime), N'', 142)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (275, CAST(N'2023-02-02T23:00:00.000' AS DateTime), N'', 142)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (276, CAST(N'2023-01-28T00:00:00.000' AS DateTime), N'', 128)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (277, CAST(N'2023-01-29T00:00:00.000' AS DateTime), N'', 128)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (278, CAST(N'2023-01-24T00:00:00.000' AS DateTime), N'', 140)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (279, CAST(N'2023-01-25T00:00:00.000' AS DateTime), N'', 140)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (280, CAST(N'2023-01-26T00:00:00.000' AS DateTime), N'', 140)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (281, CAST(N'2023-01-27T00:00:00.000' AS DateTime), N'', 140)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (282, CAST(N'2023-01-28T00:00:00.000' AS DateTime), N'', 140)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (283, CAST(N'2023-01-23T00:00:00.000' AS DateTime), N'', 138)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (284, CAST(N'2023-01-24T00:00:00.000' AS DateTime), N'', 138)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (285, CAST(N'2023-01-25T00:00:00.000' AS DateTime), N'', 138)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (286, CAST(N'2023-01-26T00:00:00.000' AS DateTime), N'', 138)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (287, CAST(N'2023-01-27T00:00:00.000' AS DateTime), N'', 138)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (301, CAST(N'2023-02-03T00:39:24.000' AS DateTime), N'Narvil', 148)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (302, CAST(N'2023-02-04T00:39:24.000' AS DateTime), N'', 148)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (309, CAST(N'2023-02-04T00:58:53.310' AS DateTime), N'', 159)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (310, CAST(N'2023-02-05T00:58:53.310' AS DateTime), N'', 159)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (311, CAST(N'2023-02-06T00:58:53.310' AS DateTime), N'', 159)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (312, CAST(N'2023-02-07T00:58:53.310' AS DateTime), N'', 159)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (313, CAST(N'2023-02-08T00:58:53.310' AS DateTime), N'', 159)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (314, CAST(N'2023-02-09T00:58:53.310' AS DateTime), N'', 159)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (315, CAST(N'2023-02-10T00:58:53.310' AS DateTime), N'', 159)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (316, CAST(N'2023-02-11T00:58:53.310' AS DateTime), N'', 159)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (317, CAST(N'2023-02-12T00:58:53.310' AS DateTime), N'', 159)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (318, CAST(N'2023-02-13T00:58:53.310' AS DateTime), N'', 159)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (319, CAST(N'2023-02-14T00:58:53.310' AS DateTime), N'', 159)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (320, CAST(N'2023-02-15T00:58:53.310' AS DateTime), N'', 159)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (321, CAST(N'2023-02-16T00:58:53.310' AS DateTime), N'', 159)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (322, CAST(N'2023-02-17T00:58:53.310' AS DateTime), N'', 159)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (323, CAST(N'2023-02-18T00:58:53.310' AS DateTime), N'', 159)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (324, CAST(N'2023-02-19T00:58:53.310' AS DateTime), N'', 159)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (325, CAST(N'2023-02-20T00:58:53.310' AS DateTime), N'', 159)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (326, CAST(N'2023-02-21T00:58:53.310' AS DateTime), N'', 159)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (327, CAST(N'2023-02-22T00:58:53.310' AS DateTime), N'', 159)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (328, CAST(N'2023-02-23T00:58:53.310' AS DateTime), N'', 159)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (329, CAST(N'2023-02-24T00:58:53.310' AS DateTime), N'', 159)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (330, CAST(N'2023-02-25T00:58:53.310' AS DateTime), N'', 159)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (331, CAST(N'2023-02-26T00:58:53.310' AS DateTime), N'', 159)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (332, CAST(N'2023-02-27T00:58:53.310' AS DateTime), N'', 159)
INSERT [dbo].[VisitDate] ([VisitDateId], [Date], [Title], [TravelId]) VALUES (333, CAST(N'2023-02-28T00:58:53.310' AS DateTime), N'', 159)
SET IDENTITY_INSERT [dbo].[VisitDate] OFF
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [Unique_User_Email]    Script Date: 04.02.2023 21:37:07 ******/
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [Unique_User_Email] UNIQUE NONCLUSTERED 
(
	[Email] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Alert]  WITH CHECK ADD  CONSTRAINT [FK_Country_Alert] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Country] ([CountryId])
GO
ALTER TABLE [dbo].[Alert] CHECK CONSTRAINT [FK_Country_Alert]
GO
ALTER TABLE [dbo].[Country]  WITH CHECK ADD  CONSTRAINT [FK_Flag_Country] FOREIGN KEY([FlagId])
REFERENCES [dbo].[Flag] ([FlagId])
GO
ALTER TABLE [dbo].[Country] CHECK CONSTRAINT [FK_Flag_Country]
GO
ALTER TABLE [dbo].[Country]  WITH CHECK ADD  CONSTRAINT [FK_ServicePhone_Country] FOREIGN KEY([ServicePhoneId])
REFERENCES [dbo].[ServicePhone] ([ServicePhoneId])
GO
ALTER TABLE [dbo].[Country] CHECK CONSTRAINT [FK_ServicePhone_Country]
GO
ALTER TABLE [dbo].[DictionaryWord]  WITH CHECK ADD  CONSTRAINT [FK_Dictionary_DictionaryWord] FOREIGN KEY([DictionaryId])
REFERENCES [dbo].[Dictionary] ([DictionaryId])
GO
ALTER TABLE [dbo].[DictionaryWord] CHECK CONSTRAINT [FK_Dictionary_DictionaryWord]
GO
ALTER TABLE [dbo].[OweSinglePayment]  WITH CHECK ADD  CONSTRAINT [FK_Expense_OweSinglePayment] FOREIGN KEY([ExpenseId])
REFERENCES [dbo].[Expense] ([ExpenseId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OweSinglePayment] CHECK CONSTRAINT [FK_Expense_OweSinglePayment]
GO
ALTER TABLE [dbo].[Spot]  WITH CHECK ADD  CONSTRAINT [FK_Expense_Spot] FOREIGN KEY([ExpenseId])
REFERENCES [dbo].[Expense] ([ExpenseId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Spot] CHECK CONSTRAINT [FK_Expense_Spot]
GO
ALTER TABLE [dbo].[Spot]  WITH CHECK ADD  CONSTRAINT [FK_VisitDate_Spot] FOREIGN KEY([VisitDateId])
REFERENCES [dbo].[VisitDate] ([VisitDateId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Spot] CHECK CONSTRAINT [FK_VisitDate_Spot]
GO
ALTER TABLE [dbo].[Travel]  WITH CHECK ADD  CONSTRAINT [FK_Country_Travel] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Country] ([CountryId])
GO
ALTER TABLE [dbo].[Travel] CHECK CONSTRAINT [FK_Country_Travel]
GO
ALTER TABLE [dbo].[Travel]  WITH CHECK ADD  CONSTRAINT [FK_User_Travel] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[Travel] CHECK CONSTRAINT [FK_User_Travel]
GO
ALTER TABLE [dbo].[VisitDate]  WITH CHECK ADD  CONSTRAINT [FK_Travel_VisitDate] FOREIGN KEY([TravelId])
REFERENCES [dbo].[Travel] ([TravelId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[VisitDate] CHECK CONSTRAINT [FK_Travel_VisitDate]
GO
ALTER TABLE [dbo].[Country]  WITH CHECK ADD  CONSTRAINT [CODEAB_CHECK] CHECK  ((NOT [CodeAB] like '%[^A-Z]%'))
GO
ALTER TABLE [dbo].[Country] CHECK CONSTRAINT [CODEAB_CHECK]
GO
ALTER TABLE [dbo].[Country]  WITH CHECK ADD  CONSTRAINT [CODEABC_CHECK] CHECK  ((NOT [CodeABC] like '%[^A-Z]%'))
GO
ALTER TABLE [dbo].[Country] CHECK CONSTRAINT [CODEABC_CHECK]
GO
ALTER TABLE [dbo].[OweSinglePayment]  WITH CHECK ADD  CONSTRAINT [PERSONNAME_CHECK] CHECK  ((NOT [PersonName] like '%[0-9]%'))
GO
ALTER TABLE [dbo].[OweSinglePayment] CHECK CONSTRAINT [PERSONNAME_CHECK]
GO
ALTER TABLE [dbo].[Travel]  WITH CHECK ADD  CONSTRAINT [DESTINATION_CHECK] CHECK  ((NOT [Destination] like '%[^A-Z]%'))
GO
ALTER TABLE [dbo].[Travel] CHECK CONSTRAINT [DESTINATION_CHECK]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [EMAIL_CKECH] CHECK  (([Email] like '%__@__%.__%'))
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [EMAIL_CKECH]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [PHONE_CKECH] CHECK  ((NOT [PhoneNumber] like '%[^0-9]%'))
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [PHONE_CKECH]
GO
/****** Object:  StoredProcedure [dbo].[AddAlert]    Script Date: 04.02.2023 21:37:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[AddAlert]
@Content VARCHAR(100),
@CountryId INT
AS
Set NOCOUNT ON;

BEGIN TRY
	BEGIN TRAN;
	DECLARE @CreatedDate DATETIME
	DECLARE @ValidDate DATETIME
	SET @CreatedDate = GETDATE();
	SET @ValidDate = DATEADD(day,7,@CreatedDate)-- o tydzień
	INSERT INTO Alert(Content,CreatedDate,ValidDate,CountryId)
	VALUES (@Content,@CreatedDate,@ValidDate,@CountryId);
	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('Country connected with Alert must exist');
END CATCH;

GO
/****** Object:  StoredProcedure [dbo].[AddCountry]    Script Date: 04.02.2023 21:37:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[AddCountry]
@CodeABC VARCHAR(3),
@CodeAB VARCHAR(2),
@Name VARCHAR(30),
@Currency VARCHAR(3),
@FlagId INT,
@ServicePhoneId INT
AS
Set NOCOUNT ON;

BEGIN TRY
	BEGIN TRAN;
	INSERT INTO Country(CodeABC,CodeAB,Name,Currency,FlagId,ServicePhoneId)
	VALUES (@CodeABC,@CodeAB,@Name,@Currency,@FlagId,@ServicePhoneId);
	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('Flag and Service Phone that are connected with Alert must exist');
END CATCH;

GO
/****** Object:  StoredProcedure [dbo].[AddDictionaryWord]    Script Date: 04.02.2023 21:37:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[AddDictionaryWord]
@Word NVarchar(255),
@WordTranslated NVarchar(255),
@DictionaryId INT
AS 
Set NOCOUNT ON;
BEGIN TRY
	BEGIN TRAN;
	Insert into DictionaryWord(Word,WordTranslated,DictionaryId)
	values(@Word,@WordTranslated,@DictionaryId);
	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('Dictionary connected with DictionaryWord must exist');
END CATCH;

GO
/****** Object:  StoredProcedure [dbo].[AddOweSinglePayment]    Script Date: 04.02.2023 21:37:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[AddOweSinglePayment]
@PersonName VARCHAR(30),
@PaymentAmount DECIMAL,
@PaymentStatus BIT,
@PaymentDate DATETIME,
@IsPayer BIT,
@ExpenseId INT
AS
Set NOCOUNT ON;

BEGIN TRY
	BEGIN TRAN;
	INSERT INTO OweSinglePayment(PersonName, PaymentAmount, PaymentStatus, PaymentDate, IsPayer, ExpenseId)
	VALUES (@PersonName,@PaymentAmount,@PaymentStatus,@PaymentDate,@IsPayer,@ExpenseId);
	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('Expense connected with OweSinglePayment must exist');
END CATCH;

GO
/****** Object:  StoredProcedure [dbo].[AddSpot]    Script Date: 04.02.2023 21:37:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[AddSpot]
@Note VARCHAR(255),
@Adress VARCHAR(255),
@VisitDateId INT,
@ExpenseId INT,
@CoordinateX DECIMAL(10,7),
@CoordinateY DECIMAL(10,7),
@Name VARCHAR(255)
AS
Set NOCOUNT ON;
BEGIN TRY
	BEGIN TRAN;
	Insert into Spot(Note,Adress,VisitDateId,ExpenseId,CoordinateX,CoordinateY,"Name")
	values(@Note,@Adress,@VisitDateId,@ExpenseId,@CoordinateX,@CoordinateY,@Name)
	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('VisitDate and Expense connected with Spot must exist');
END CATCH;

GO
/****** Object:  StoredProcedure [dbo].[AddTravel]    Script Date: 04.02.2023 21:37:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[AddTravel]
@Name as Varchar(30),
@Destination as Varchar(255),
@StartDate as Date,
@EndDate as Date,
@Note as Varchar(100),
@PlannedBudget as decimal,
@UserId as int,
@CountryId as int,
@HotelName as Varchar(50),
@PickedCurrency as Varchar(3),
@HotelStreet Varchar(255),
@HotelBuildingNo int,
@HotelFlatNo int,
@HotelZipCode Varchar(255),
@HotelCity Varchar(255)
AS
Set NOCOUNT ON;
BEGIN TRY
	BEGIN TRAN;
	DECLARE @actDate DateTime;
	set @actDate = Convert(datetime, GETDATE())
	Insert into Travel(Name,Destination,StartDate,EndDate,Note,PlannedBudget,CreatedDate,UserId,CountryId,HotelName,PickedCurrency,HotelStreet,HotelBuildingNo,HotelFlatNo,HotelZipCode,HotelCity)
	values(@Name,@Destination,@StartDate,@EndDate,@Note,@PlannedBudget,@actDate,@UserId,@CountryId,@HotelName,@PickedCurrency,@HotelStreet,@HotelBuildingNo,@HotelFlatNo,@HotelZipCode,@HotelCity)
	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('User and Country connected with Travel must exist');
END CATCH;

GO
/****** Object:  StoredProcedure [dbo].[AddVisitDate]    Script Date: 04.02.2023 21:37:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[AddVisitDate]
@Date DateTime,
@Title VARCHAR(30),
@TravelId INT
AS
Set NOCOUNT ON;
BEGIN TRY
	BEGIN TRAN;
	Insert into VisitDate(Date,Title,TravelId)
	values(@Date,@Title,@TravelId)
	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('Travel connected with VisitDate must exist');
END CATCH;

GO
/****** Object:  StoredProcedure [dbo].[CheckBudget]    Script Date: 04.02.2023 21:37:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[CheckBudget]
@TravelId INT,
@Result float output
AS
BEGIN
Set NOCOUNT ON;
DECLARE @CostSum float;
DECLARE @OwePaymentSum float;

CREATE TABLE #Expenses(
	ExpenseId int
	)

Insert into #Expenses(ExpenseId)
	select s.ExpenseId from Spot s where VisitDateId IN 
	(Select VisitDateId From VisitDate v where TravelId = @TravelId)

	Select @OwePaymentSum = SUM(PaymentAmount) from OweSinglePayment ow
	Where ExpenseId IN 
	(SELECT t.ExpenseId FROM #Expenses t)

Select @CostSum=SUM(Cost) from Expense e
Where e.ExpenseId IN(
Select s.ExpenseId from Spot s
Where  VisitDateId IN 
(Select VisitDateId From VisitDate vd 
Where vd.TravelId like @TravelId));

Select @Result = coalesce(@CostSum + @OwePaymentSum, @CostSum, @OwePaymentSum, 0);-- @CostSum + @OwePaymentSum;

Select 'Result'= @Result;

Return;
END;

GO
/****** Object:  StoredProcedure [dbo].[FindCountry]    Script Date: 04.02.2023 21:37:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[FindCountry]
@Phrase as Varchar(255)
AS
BEGIN
Set NOCOUNT ON;
DECLARE @arg Varchar(255)
set @arg = '%'+@Phrase+'%'
Select 'CountryId'=CountryId,'CodeABC'=CodeABC,'CodeAB'=CodeAB,'Name'=LOWER (Name), 'Currency'=Currency, 'FlagId'=FlagId, 'ServicePhoneId'=ServicePhoneId from Country
Where Name like LOWER(@arg)
END;

GO
/****** Object:  StoredProcedure [dbo].[RecoveryPasswordChange]    Script Date: 04.02.2023 21:37:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[RecoveryPasswordChange]
@Email as Varchar(30),
@NEWPassword as Varchar(1024)
AS
BEGIN
Set NOCOUNT ON;
DECLARE @actDate DateTime;
set @actDate = Convert(datetime, GETDATE()) 
Update "User"
set PasswordDateUpdated = @actDate, Password = @NEWPassword
Where Email = @Email
END;

GO
/****** Object:  StoredProcedure [dbo].[RemoveTravel]    Script Date: 04.02.2023 21:37:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[RemoveTravel]
@TravelId INT
AS
Set NOCOUNT ON;
BEGIN TRY
	BEGIN TRAN;

	CREATE TABLE #TempExpenses(
	ExpenseId int
	)

	Insert into #TempExpenses(ExpenseId)
	select s.ExpenseId from Spot s where VisitDateId IN 
	(Select VisitDateId From VisitDate v where TravelId = @TravelId)

	DELETE FROM Travel WHERE TravelId=@TravelId;
	DELETE FROM Expense WHERE ExpenseId IN
	(SELECT t.ExpenseId FROM #TempExpenses t);

	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('Could not delete all travel info');
END CATCH;

GO
ALTER DATABASE [TraveloDb] SET  READ_WRITE 
GO
