alter table Country
drop constraint FK_Flag_Country

truncate table Flag

Insert into Flag(URL)
values('https://www.worldatlas.com/img/flag/af-flag.jpg');

Insert into Flag(URL)
values('https://www.worldatlas.com/img/flag/cm-flag.jpg');

Insert into Flag(URL)
values('https://www.worldatlas.com/img/flag/bg-flag.jpg');

Insert into Flag(URL)
values('https://www.worldatlas.com/img/flag/ca-flag.jpg');

Insert into Flag(URL)
values('https://www.worldatlas.com/img/flag/ck-flag.jpg');

Insert into Flag(URL)
values('https://www.worldatlas.com/img/flag/ni-flag.jpg');

Insert into Flag(URL)
values('https://www.worldatlas.com/img/flag/no-flag.jpg');

Insert into Flag(URL)
values('https://www.worldatlas.com/img/flag/ad-flag.jpg');


alter table Country
drop constraint FK_ServicePhone_Country

truncate table ServicePhone

Insert into ServicePhone(Number)
values('119');

Insert into ServicePhone(Number)
values('110');

Insert into ServicePhone(Number)
values('112');

Insert into ServicePhone(Number)
values('911');

Insert into ServicePhone(Number)
values('999');

Insert into ServicePhone(Number)
values('118');

alter table Travel
drop constraint FK_Country_Travel

alter table Alert
drop constraint FK_Country_Alert


truncate table Country

EXEC AddCountry 'AFG','Afghanistan','AFA',1,1

EXEC AddCountry 'AND','Andorra','EUR',8,2

EXEC AddCountry 'CMR','Cameroon','XAF',2,3

EXEC AddCountry 'BGR','Bulgaria','BGN',3,3

EXEC AddCountry 'CAN','Canada','CAD',4,4

EXEC AddCountry 'COK','Cook Islands','NZD',5,5

EXEC AddCountry 'NIC','Nicaragua','NIO',6,6

EXEC AddCountry 'NOR','Norway','NOK',7,3


Alter table Country WITH CHECK ADD CONSTRAINT FK_Flag_Country FOREIGN KEY (FlagId)
REFERENCES Flag (FlagId) ON UPDATE NO ACTION ON DELETE NO ACTION

Alter table Country WITH CHECK ADD CONSTRAINT FK_ServicePhone_Country FOREIGN KEY (ServicePhoneId)
REFERENCES ServicePhone (ServicePhoneId) ON UPDATE NO ACTION ON DELETE NO ACTION

Alter table Alert WITH CHECK ADD CONSTRAINT FK_Country_Alert FOREIGN KEY (CountryId)
REFERENCES Country (CountryId) ON UPDATE NO ACTION ON DELETE NO ACTION

Alter table Travel WITH CHECK ADD CONSTRAINT FK_Country_Travel FOREIGN KEY (CountryId)
REFERENCES Country (CountryId) ON UPDATE NO ACTION ON DELETE NO ACTION
