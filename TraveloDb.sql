CREATE DATABASE TraveloDb 

use TraveloDb
go

CREATE TABLE ServicePhone(
	ServicePhoneId INT IDENTITY (1,1) NOT NULL,
	Number INT NOT NULL,
	CONSTRAINT PK_ServicePhone PRIMARY KEY CLUSTERED (ServicePhoneId)
);

CREATE TABLE Flag(
	FlagId INT IDENTITY (1,1) NOT NULL,
	URL VARCHAR(50) NOT NULL,
	CONSTRAINT PK_Flag PRIMARY KEY CLUSTERED (FlagId)
);

CREATE TABLE "Users"(
	UserId INT IDENTITY (1,1) NOT NULL,
	"Name" VARCHAR(30) NOT NULL,
	Surname VARCHAR(60) NOT NULL,
	PhoneNumber VARCHAR(14),
	Email VARCHAR(30) NOT NULL,
	Password VARCHAR(64) NOT NULL,
	PasswordDateUpdated DateTime NOT NULL,
	DateCreated DateTime NOT NULL,
	LastLogin DateTime NOT NULL,
	CONSTRAINT PK_Users PRIMARY KEY CLUSTERED (UserId),
	CONSTRAINT NAME_CHECK CHECK (Name NOT LIKE '%[^A-Z]%'),
	CONSTRAINT SURNAME_CHECK CHECK (Surname NOT LIKE '%[^A-Z]%'),
	CONSTRAINT EMAIL_CKECH CHECK (Email LIKE '%__@__%.__%'),
	CONSTRAINT Unique_User_Email Unique(Email),
	CONSTRAINT PHONE_CKECH CHECK (PhoneNumber NOT LIKE '%[^0-9]%')
);

CREATE TABLE Travels(
	TravelId INT IDENTITY (1,1) NOT NULL,
	"Name" VARCHAR(30) NOT NULL,
	Destination VARCHAR(255) NOT NULL,
	StartDate Date NOT NULL,
	EndDate Date NOT NULL,
	IdFav BIT NULL,
	Note VARCHAR(100) NULL,
	ParticipatNumber INT NULL,
	PlannedBudget DECIMAL NULL,
	LinkUrl VARCHAR(255) NULL,
	LinkExpirationDate DateTime NULL,
	CreatedDate DateTime NOT NULL,
	UserId INT NULL,
	CountryId INT NOT NULL,
	CONSTRAINT DESTINATION_CHECK CHECK (Destination NOT LIKE '%[^A-Z]%'),
	CONSTRAINT PK_Travels PRIMARY KEY CLUSTERED (TravelId)
);

ALTER TABLE Travels WITH CHECK ADD CONSTRAINT FK_Users_Travels FOREIGN KEY (UserId)
REFERENCES Users (UserId)


CREATE TABLE Countries(
	CountryId INT IDENTITY (1,1) NOT NULL,
	Code VARCHAR(3) NOT NULL,
	Name VARCHAR(30) NOT NULL,
	Currency VARCHAR(14) NOT NULL,
	CONSTRAINT COUNTRY_NAME_CHECK CHECK (Name NOT LIKE '%[^A-Z]%'),
	CONSTRAINT CODE_CHECK CHECK (Code NOT LIKE '%[^A-Z]%'),
	CONSTRAINT CURRENCY_CHECK CHECK (Currency NOT LIKE '%[^A-Z]%'),
	CONSTRAINT PK_Countries PRIMARY KEY CLUSTERED (CountryId)
);

ALTER TABLE Travels WITH CHECK ADD CONSTRAINT FK_Countries_Travels FOREIGN KEY (CountryId)
REFERENCES Countries (CountryId)

CREATE TABLE Alerts(
	AlertId INT IDENTITY (1,1) NOT NULL,
	Content VARCHAR(100) NOT NULL,
	CreatedDate DateTime NOT NULL,
	ValidDate DateTime NULL,
	CONSTRAINT PK_Alerts PRIMARY KEY CLUSTERED (AlertId)
);

CREATE TABLE CountriesAlerts(
	CountryAlertId INT IDENTITY (1,1) NOT NULL,
	CountryId INT NULL,
	AlertId INT NULL,
	CONSTRAINT PK_CountriesAlerts PRIMARY KEY CLUSTERED (CountryAlertId)
);

ALTER TABLE CountriesAlerts WITH CHECK ADD CONSTRAINT FK_Countries_CountriesAlerts FOREIGN KEY (CountryId)
REFERENCES Countries (CountryId)

ALTER TABLE CountriesAlerts WITH CHECK ADD CONSTRAINT FK_Alerts_CountriesAlerts FOREIGN KEY (AlertId)
REFERENCES Alerts (AlertId)

CREATE TABLE Dictionaries(
	DictionaryId INT IDENTITY (1,1) NOT NULL,
	Name VARCHAR(30) NOT NULL,
	CONSTRAINT DICTIONARY_NAME_CHECK CHECK (Name NOT LIKE '%[^A-Z]%'),
	CONSTRAINT PK_Dictionaries PRIMARY KEY CLUSTERED (DictionaryId) 
);

CREATE TABLE DictionaryWords(
	DictionaryWordId INT IDENTITY (1,1) NOT NULL,
	Word VARCHAR(255) NOT NULL,
	WordTranslated VARCHAR(255) NOT NULL,
	DictionaryId INT NULL,
	CONSTRAINT WORD_CHECK CHECK (Word NOT LIKE '%[^A-Z]%'),
	CONSTRAINT WORDTRANSLATED_CHECK CHECK (WordTranslated NOT LIKE '%[^A-Z]%'),
	CONSTRAINT PK_DictionaryWords PRIMARY KEY CLUSTERED (DictionaryWordId)
);

ALTER TABLE DictionaryWords WITH CHECK ADD CONSTRAINT FK_Dictionaries_DictionaryWords FOREIGN KEY (DictionaryId)
REFERENCES Dictionaries (DictionaryId)

CREATE TABLE VisitDates(
	TravelDateId INT IDENTITY (1,1) NOT NULL,
	Date DateTime NOT NULL,
	Title VARCHAR(30) NOT NULL,
	TravelId INT NULL,
	CONSTRAINT PK_VisitDates PRIMARY KEY CLUSTERED (TravelDateId)
);

ALTER TABLE VisitDates WITH CHECK ADD CONSTRAINT FK_Travels_VisitDates FOREIGN KEY (TravelId)
REFERENCES Travels (TravelId)

CREATE TABLE Places(
	PlaceId INT IDENTITY (1,1) NOT NULL,
	Category VARCHAR(30) NULL,
	City VARCHAR(150) NULL,
	CityCode VARCHAR(10) NULL,
	CoordinateX DECIMAL NOT NULL,
	CoordinateY DECIMAL NOT NULL,
	Radius FLOAT NOT NULL,
	Type INT NOT NULL,
	CountryCode INT NOT NULL,
	CONSTRAINT CATEGORY_CHECK CHECK (Category NOT LIKE '%[^A-Z]%'),
	CONSTRAINT CITY_CHECK CHECK (City NOT LIKE '%[^A-Z]%'),
	CONSTRAINT PK_Places PRIMARY KEY CLUSTERED (PlaceId)
);

CREATE TABLE Spots(
	SpotId INT IDENTITY (1,1) NOT NULL,
	Note VARCHAR(255) NULL,
	"Order" INT NULL,
	Street VARCHAR(30) NOT NULL,
	BuildingNo VARCHAR(10) NOT NULL,
	FlatNo VARCHAR(10) NULL,
	ZipCode VARCHAR(10) NOT NULL,
	PlaceId INT NULL,
	TravelDateId INT NOT NULL,
	ExpenseId INT NULL,
	CONSTRAINT ZIPCODE_CHECK CHECK (ZipCode NOT LIKE '%[^0-9]%'),
	CONSTRAINT PK_Spots PRIMARY KEY CLUSTERED (SpotId)
);

ALTER TABLE Spots WITH CHECK ADD CONSTRAINT FK_Places_Spots FOREIGN KEY (PlaceId)
REFERENCES Places (PlaceId)

ALTER TABLE Spots WITH CHECK ADD CONSTRAINT FK_VisitDates_Spots FOREIGN KEY (TravelDateId)
REFERENCES VisitDates (TravelDateId)

CREATE TABLE Expenses(
	ExpenseId INT IDENTITY (1,1) NOT NULL,
	PersonPaymentId INT NOT NULL,
	Cost DECIMAL NOT NULL,
	CONSTRAINT PK_Expenses PRIMARY KEY CLUSTERED (ExpenseId)
);

ALTER TABLE Spots WITH CHECK ADD CONSTRAINT FK_Expenses_Spots FOREIGN KEY (ExpenseId)
REFERENCES Expenses (ExpenseId)

CREATE TABLE OweSinglePayments(
	OweSinglePaymentId INT IDENTITY (1,1) NOT NULL,
	PersonName VARCHAR(30) NOT NULL,
	PaymentAmount DECIMAL NOT NULL,
	PaymentStatus BIT NOT NULL,
	PaymentDate DateTime NOT NULL,
	IsPayer BIT NOT NULL,
	ExpenseId INT NULL,
	CONSTRAINT PERSONNAME_CHECK CHECK (PersonName NOT LIKE '%[^A-Z]%'),
	CONSTRAINT PK_OweSinglePayments PRIMARY KEY CLUSTERED (OweSinglePaymentId)
);

ALTER TABLE OweSinglePayments WITH CHECK ADD CONSTRAINT FK_Expenses_OweSinglePayments FOREIGN KEY (ExpenseId)
REFERENCES Expenses (ExpenseId)


CREATE TABLE SystemNotifications(
	SystemNotificationId INT IDENTITY (1,1) NOT NULL,
	Content VARCHAR(100) NOT NULL,
	CreatedDate DateTime NOT NULL,
	ValidDate DateTime NOT NULL,
	Title VARCHAR(30) NOT NULL,
	"Type" VARCHAR(30) NOT NULL,
	CONSTRAINT PK_SystemNotifications PRIMARY KEY CLUSTERED (SystemNotificationId)
);

use TraveloDb
go

Create Procedure FindCountry
@Phrase as Varchar(3)
AS
BEGIN
Set NOCOUNT ON;
DECLARE @arg Varchar(30)
set @arg = '%'+@Phrase+'%'
Select 'CountryId'=CountryId,'Code'=Code,'Name'=LOWER (Name), 'Currency'=Currency, 'FlagId'=FlagId, 'ServicePhoneId'=ServicePhoneId from Country
Where Name like LOWER(@arg)
END;

go

Create Procedure Registration
@Name as Varchar(30),
@Surname as Varchar(60),
@PhoneNumber as Varchar(14),
@Email as Varchar(30),
@Password as Varchar(64)
AS
BEGIN
Set NOCOUNT ON;
DECLARE @actDate DateTime;
set @actDate = Convert(datetime, GETDATE())
Insert into "Users"(Name,Surname,PhoneNumber,Email,Password,PasswordDateUpdated,DateCreated,LastLogin)
values(@Name,@Surname,@PhoneNumber,@Email,@Password,@actDate,@actDate,@actDate)
END;


go

Create Procedure ChangePassword
@Email as Varchar(30),
@Password as Varchar(64),
@NEW_Password as Varchar(64)
as
begin
Set NOCOUNT ON;
DECLARE @actDate DateTime;
set @actDate = Convert(datetime, GETDATE()) 
Update "Users"
set PasswordDateUpdated = @actDate, Password = @NEW_Password
Where Email = @Email And Password = @Password
end;

go

Create Procedure LoginUser
@Email as Varchar(30),
@Password as Varchar(64)
as
begin
Set NOCOUNT ON;
Select Email, Password from "Users"
Where Email = @Email And Password = @Password
end;

go

Create Procedure RecoveryPasswordChange
@Email as Varchar(30),
@NEWPassword as Varchar(64)
AS
BEGIN
Set NOCOUNT ON;
DECLARE @actDate DateTime;
set @actDate = Convert(datetime, GETDATE()) 
Update "Users"
set PasswordDateUpdated = @actDate, Password = @NEWPassword
Where Email = @Email
END;

go

use TraveloDb
go

Create Procedure CheckBudget
@TravelId INT
AS
BEGIN
Set NOCOUNT ON;
DECLARE @PlannedBudget float;
DECLARE @CostSum float;
Select @PlannedBudget = t.PlannedBudget from Travels t
						Where t.TravelId like @TravelId;

Select @CostSum=SUM(Cost) from Expenses e
Where e.ExpenseId IN(
Select s.ExpenseId from Spots s
Where  TravelDateId IN 
(Select TravelDateId From VisitDates vd 
Where vd.TravelId like @TravelId and GETDATE()<=vd."Date"));

Select @CostSum*100/@PlannedBudget;
END;

GO

use TraveloDb
go
Create Procedure AddCountry
@Code VARCHAR(3),
@Name VARCHAR(30),
@Currency VARCHAR(14),
@FlagId INT,
@ServicePhoneId INT
AS
Set NOCOUNT ON;

BEGIN TRY
	BEGIN TRAN;
	INSERT INTO Country(Code,Name,Currency,FlagId,ServicePhoneId)
	VALUES (@Code,@Name,@Currency,@FlagId,@ServicePhoneId);
	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('Flag and Service Phone that are connected with Alert must exist');
END CATCH;

GO

use TraveloDb
go
Create Procedure AddAlert
@Content VARCHAR(100),
@CountryId INT
AS
Set NOCOUNT ON;

BEGIN TRY
	BEGIN TRAN;
	DECLARE @CreatedDate DATETIME
	DECLARE @ValidDate DATETIME
	SET @CreatedDate = GETDATE();
	SET @ValidDate = DATEADD(day,7,@CreatedDate)
	INSERT INTO Alert(Content,CreatedDate,ValidDate,CountryId)
	VALUES (@Content,@CreatedDate,@ValidDate,@CountryId);
	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('Country connected with Alert must exist');
END CATCH;

GO

use TraveloDb
go
Create Procedure AddOweSinglePayment
@PersonName VARCHAR(30),
@PaymentAmount DECIMAL,
@PaymentStatus BIT,
@PaymentDate DATETIME,
@IsPayer BIT,
@ExpenseId INT
AS
Set NOCOUNT ON;

BEGIN TRY
	BEGIN TRAN;
	INSERT INTO OweSinglePayment(PersonName, PaymentAmount, PaymentStatus, PaymentDate, IsPayer, ExpenseId)
	VALUES (@PersonName,@PaymentAmount,@PaymentStatus,@PaymentDate,@IsPayer,@ExpenseId);
	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('Expense connected with OweSinglePayment must exist');
END CATCH;

GO

use TraveloDb
go
Create Procedure AddVisitDate
@Date DateTime,
@Title VARCHAR(30),
@TravelId INT
AS
Set NOCOUNT ON;
BEGIN TRY
	BEGIN TRAN;
	Insert into VisitDate(Date,Title,TravelId)
	values(@Date,@Title,@TravelId)
	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('Travel connected with VisitDate must exist');
END CATCH;

GO

use TraveloDb
go
Create Procedure AddTravel
@Name as Varchar(30),
@Destination as Varchar(255),
@StartDate as Date,
@EndDate as Date,
@IsFav as bit,
@Note as Varchar(100),
@ParticipantNumber as int,
@PlannedBudget as decimal,
@LinkUrl as Varchar(255),
@LinkExpirationDate as DateTime,
@UserId as int,
@CountryId as int,
@HotelName as Varchar(50)
AS
Set NOCOUNT ON;
BEGIN TRY
	BEGIN TRAN;
	DECLARE @actDate DateTime;
	set @actDate = Convert(datetime, GETDATE())
	Insert into Travel(Name,Destination,StartDate,EndDate,IsFav,Note,ParticipantNumber,PlannedBudget,LinkUrl,LinkExpirationDate,CreatedDate,UserId,CountryId,HotelName)
	values(@Name,@Destination,@StartDate,@EndDate,@IsFav,@Note,@ParticipantNumber,@PlannedBudget,@LinkUrl,@LinkExpirationDate,@actDate,@UserId,@CountryId,@HotelName)
	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('User and Country connected with Travel must exist');
END CATCH;

GO

use TraveloDb
go
Create Procedure AddSpot
@Note VARCHAR(255),
@Order INT,
@Street VARCHAR(30),
@BuildingNo VARCHAR(10),
@FlatNo VARCHAR(10),
@ZipCode VARCHAR(10),
@VisitDateId INT,
@ExpenseId INT
AS
Set NOCOUNT ON;
BEGIN TRY
	BEGIN TRAN;
	Insert into Spot(Note,"Order",Street,BuildingNo,FlatNo,ZipCode,VisitDateId,ExpenseId)
	values(@Note,@Order,@Street,@BuildingNo,@FlatNo,@ZipCode,@VisitDateId,@ExpenseId)
	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('VisitDate and Expense connected with Spot must exist');
END CATCH;

GO

Use master
go

CREATE LOGIN TraveloUserApi
WITH PASSWORD = '1231!#ASDF!a';

use TraveloDb
go
CREATE USER Api FOR LOGIN TraveloUserApi;

CREATE ROLE api_user;

GRANT INSERT, UPDATE, DELETE ON TraveloDb.dbo.Alert TO api_user;
GRANT INSERT, UPDATE, DELETE ON TraveloDb.dbo.Country TO api_user;
GRANT INSERT, UPDATE, DELETE ON TraveloDb.dbo.Dictionary TO api_user;
GRANT INSERT, UPDATE, DELETE ON TraveloDb.dbo.DictionaryWord TO api_user;
GRANT INSERT, UPDATE, DELETE ON TraveloDb.dbo.Expense TO api_user;
GRANT INSERT, UPDATE, DELETE ON TraveloDb.dbo.Flag TO api_user;
GRANT INSERT, UPDATE, DELETE ON TraveloDb.dbo.OweSinglePayment TO api_user;
GRANT INSERT, UPDATE, DELETE ON TraveloDb.dbo.ServicePhone TO api_user;
GRANT INSERT, UPDATE, DELETE ON TraveloDb.dbo.Spot TO api_user;
GRANT INSERT, UPDATE, DELETE ON TraveloDb.dbo.SystemNotification TO api_user;
GRANT INSERT, UPDATE, DELETE ON TraveloDb.dbo.Travel TO api_user;
GRANT INSERT, UPDATE, DELETE ON TraveloDb.dbo."User" TO api_user;
GRANT INSERT, UPDATE, DELETE ON TraveloDb.dbo.VisitDate TO api_user;

GRANT EXEC ON TraveloDb.dbo.CheckBudget TO api_user;
GRANT EXEC ON TraveloDb.dbo.FindCountry TO api_user;
GRANT EXEC ON TraveloDb.dbo.RecoveryPasswordChange TO api_user;

GRANT EXEC ON TraveloDb.dbo.AddAlert TO api_user;
GRANT EXEC ON TraveloDb.dbo.AddCountry TO api_user;
GRANT EXEC ON TraveloDb.dbo.AddOweSinglePayment TO api_user;
GRANT EXEC ON TraveloDb.dbo.AddSpot TO api_user;
GRANT EXEC ON TraveloDb.dbo.AddTravel TO api_user;
GRANT EXEC ON TraveloDb.dbo.AddVisitDate TO api_user;

REVOKE EXEC ON TraveloDb.dbo.AddAlert TO api_user;
REVOKE EXEC ON TraveloDb.dbo.AddCountry TO api_user;


REVOKE INSERT,UPDATE, DELETE ON TraveloDb.dbo.Dictionary TO api_user;
GRANT SELECT ON TraveloDb.dbo.Dictionary TO api_user;

REVOKE INSERT,UPDATE, DELETE ON TraveloDb.dbo.DictionaryWord TO api_user;
GRANT SELECT ON TraveloDb.dbo.DictionaryWord TO api_user;

REVOKE INSERT,UPDATE, DELETE ON TraveloDb.dbo.Flag TO api_user;
GRANT SELECT ON TraveloDb.dbo.Flag TO api_user;

REVOKE INSERT,UPDATE, DELETE ON TraveloDb.dbo.ServicePhone TO api_user;
GRANT SELECT ON TraveloDb.dbo.ServicePhone TO api_user;

REVOKE INSERT,UPDATE, DELETE ON TraveloDb.dbo.SystemNotification TO api_user;
GRANT SELECT ON TraveloDb.dbo.SystemNotification TO api_user;

REVOKE INSERT,UPDATE, DELETE ON TraveloDb.dbo.Alert TO api_user;
GRANT SELECT ON TraveloDb.dbo.Alert TO api_user;

REVOKE INSERT,UPDATE, DELETE ON TraveloDb.dbo.Country TO api_user;
GRANT SELECT ON TraveloDb.dbo.Country TO api_user;

GRANT SELECT ON TraveloDb.dbo.Travel TO api_user;
GRANT SELECT ON TraveloDb.dbo.VisitDate TO api_user;
GRANT SELECT ON TraveloDb.dbo.OweSinglePayment TO api_user;
GRANT SELECT ON TraveloDb.dbo.Expense TO api_user;
GRANT SELECT ON TraveloDb.dbo.Spot TO api_user;
GRANT SELECT ON TraveloDb.dbo."User" TO api_user;


ALTER ROLE api_user
ADD MEMBER Api;

EXEC sp_defaultdb 'TraveloUserApi','TraveloDb';

use TraveloDb
go

create procedure AddDictionaryWord
@Word NVarchar(255),
@WordTranslated NVarchar(255),
@DictionaryId INT
AS 
Set NOCOUNT ON;
BEGIN TRY
	BEGIN TRAN;
	Insert into DictionaryWord(Word,WordTranslated,DictionaryId)
	values(@Word,@WordTranslated,@DictionaryId);
	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('Dictionary connected with DictionaryWord must exist');
END CATCH;

GO

