--1
EXEC AddDictionaryWord 'Dzień dobry, jak dostać się do ...','Good morning, how to get to the ...',1
EXEC AddDictionaryWord 'Dzień dobry, jak dostać się do ...','Guten Tag, wie komme ich zum ...',2
EXEC AddDictionaryWord 'Dzień dobry, jak dostać się do ...','Bonjour, comment se rendre chez le ...',3
EXEC AddDictionaryWord 'Dzień dobry, jak dostać się do ...','Buenos dias como llegar a la ...',4
EXEC AddDictionaryWord 'Dzień dobry, jak dostać się do ...',N'こんにちは、...への行き方 ',5
EXEC AddDictionaryWord 'Dzień dobry, jak dostać się do ...',N'Добрый день, как пройти к  ...',6

--2
EXEC AddDictionaryWord 'Ile to kosztuje?','How much is it?',1
EXEC AddDictionaryWord 'Ile to kosztuje?','Wie viel kostet das?',2
EXEC AddDictionaryWord 'Ile to kosztuje?','Combien ça coûte?',3
EXEC AddDictionaryWord 'Ile to kosztuje?','¿Cuánto cuesta?',4
EXEC AddDictionaryWord 'Ile to kosztuje?',N'いくらですか？',5
EXEC AddDictionaryWord 'Ile to kosztuje?',N'Сколько это стоит?',6

--3
EXEC AddDictionaryWord 'Potrzebuje pomocy!','I need help!',1
EXEC AddDictionaryWord 'Potrzebuje pomocy!','Ich brauche Hilfe!',2
EXEC AddDictionaryWord 'Potrzebuje pomocy!',"J'ai besoin d'aide!",3
EXEC AddDictionaryWord 'Potrzebuje pomocy!','¡Necesito ayuda!',4
EXEC AddDictionaryWord 'Potrzebuje pomocy!',N'私は助けが必要です！',5
EXEC AddDictionaryWord 'Potrzebuje pomocy!',N'мне нужна помощь!',6

--4
EXEC AddDictionaryWord 'Czy mogę postawić tu namiot?','Can I put a tent here?',1
EXEC AddDictionaryWord 'Czy mogę postawić tu namiot?','Kann ich hier ein Zelt aufstellen?',2
EXEC AddDictionaryWord 'Czy mogę postawić tu namiot?','Puis-je mettre une tente ici?',3
EXEC AddDictionaryWord 'Czy mogę postawić tu namiot?','¿Puedo poner una carpa aquí?',4
EXEC AddDictionaryWord 'Czy mogę postawić tu namiot?',N'ここにテントを張ってもいいですか？',5
EXEC AddDictionaryWord 'Czy mogę postawić tu namiot?',N'Можно ли поставить здесь палатку?',6

--5
EXEC AddDictionaryWord 'Zgubiłem się','I got lost',1
EXEC AddDictionaryWord 'Zgubiłem się','Ich habe mich verlaufen',2
EXEC AddDictionaryWord 'Zgubiłem się','Je me suis perdu',3
EXEC AddDictionaryWord 'Zgubiłem się','Me perdí',4
EXEC AddDictionaryWord 'Zgubiłem się',N'迷子になりました',5
EXEC AddDictionaryWord 'Zgubiłem się',N'я потерял',6

--6
EXEC AddDictionaryWord 'Bankomat','Cash machine',1
EXEC AddDictionaryWord 'Bankomat','Geldautomat',2
EXEC AddDictionaryWord 'Bankomat','Distributeur de billets de banque',3
EXEC AddDictionaryWord 'Bankomat','Cajero automático',4
EXEC AddDictionaryWord 'Bankomat',N'現金自動預け払い機',5
EXEC AddDictionaryWord 'Bankomat',N'банкомат',6

--7
EXEC AddDictionaryWord 'Szpital','Hospital',1
EXEC AddDictionaryWord 'Szpital','Krankenhaus',2
EXEC AddDictionaryWord 'Szpital','Hôpital',3
EXEC AddDictionaryWord 'Szpital','Hospital',4
EXEC AddDictionaryWord 'Szpital',N'病院',5
EXEC AddDictionaryWord 'Szpital',N'Больница',6

--8
EXEC AddDictionaryWord 'Apteka','Pharmacy',1
EXEC AddDictionaryWord 'Apteka','Apotheke',2
EXEC AddDictionaryWord 'Apteka','Pharmacie',3
EXEC AddDictionaryWord 'Apteka','Farmacia',4
EXEC AddDictionaryWord 'Apteka',N'薬局',5
EXEC AddDictionaryWord 'Apteka',N'Аптека',6

--9
EXEC AddDictionaryWord 'Policja','Police',1
EXEC AddDictionaryWord 'Policja','Polizei',2
EXEC AddDictionaryWord 'Policja','La police',3
EXEC AddDictionaryWord 'Policja','Policía',4
EXEC AddDictionaryWord 'Policja',N'警察',5
EXEC AddDictionaryWord 'Policja',N'Полиция',6

--10
EXEC AddDictionaryWord 'Nocleg','Accommodation',1
EXEC AddDictionaryWord 'Nocleg','Unterkunft',2
EXEC AddDictionaryWord 'Nocleg','Logement',3
EXEC AddDictionaryWord 'Nocleg','Alojamiento',4
EXEC AddDictionaryWord 'Nocleg',N'宿泊施設',5
EXEC AddDictionaryWord 'Nocleg',N'Проживание',6

--11
EXEC AddDictionaryWord 'Dokąd jedziesz?','Where are you going?',1
EXEC AddDictionaryWord 'Dokąd jedziesz?','Wo gehst du hin?',2
EXEC AddDictionaryWord 'Dokąd jedziesz?','Où allez-vous?',3
EXEC AddDictionaryWord 'Dokąd jedziesz?','¿A dónde vas?',4
EXEC AddDictionaryWord 'Dokąd jedziesz?',N'どこに行くの？',5
EXEC AddDictionaryWord 'Dokąd jedziesz?',N'Куда ты идешь?',6

--12
EXEC AddDictionaryWord 'Autobus','Bus',1
EXEC AddDictionaryWord 'Autobus','Der Bus',2
EXEC AddDictionaryWord 'Autobus','Le bus',3
EXEC AddDictionaryWord 'Autobus','Autobús',4
EXEC AddDictionaryWord 'Autobus',N'バス',5
EXEC AddDictionaryWord 'Autobus',N'Автобус',6

--13
EXEC AddDictionaryWord 'Pociąg','Train',1
EXEC AddDictionaryWord 'Pociąg','Zug',2
EXEC AddDictionaryWord 'Pociąg','Former',3
EXEC AddDictionaryWord 'Pociąg','Tren',4
EXEC AddDictionaryWord 'Pociąg',N'訓練',5
EXEC AddDictionaryWord 'Pociąg',N'Тренироваться',6

--14
EXEC AddDictionaryWord 'Samolot','Plane',1
EXEC AddDictionaryWord 'Samolot','Flugzeug',2
EXEC AddDictionaryWord 'Samolot','Avion',3
EXEC AddDictionaryWord 'Samolot','Plano',4
EXEC AddDictionaryWord 'Samolot',N'飛行機',5
EXEC AddDictionaryWord 'Samolot',N'Самолет',6

--15
EXEC AddDictionaryWord 'Dworzec','Station',1
EXEC AddDictionaryWord 'Dworzec','Bahnhof',2
EXEC AddDictionaryWord 'Dworzec','La gare',3
EXEC AddDictionaryWord 'Dworzec','Estación',4
EXEC AddDictionaryWord 'Dworzec',N'駅',5
EXEC AddDictionaryWord 'Dworzec',N'Станция',6

--16
EXEC AddDictionaryWord 'Lotnisko','Airport',1
EXEC AddDictionaryWord 'Lotnisko','Flughafen',2
EXEC AddDictionaryWord 'Lotnisko','Aéroport',3
EXEC AddDictionaryWord 'Lotnisko','Aeropuerto',4
EXEC AddDictionaryWord 'Lotnisko',N'空港',5
EXEC AddDictionaryWord 'Lotnisko',N'аэропорт',6

--17
EXEC AddDictionaryWord 'Bilet','Ticket',1
EXEC AddDictionaryWord 'Bilet','Fahrkarte',2
EXEC AddDictionaryWord 'Bilet','Billet',3
EXEC AddDictionaryWord 'Bilet','Boleto',4
EXEC AddDictionaryWord 'Bilet',N'チケット',5
EXEC AddDictionaryWord 'Bilet',N'Проездной билет',6

--18
EXEC AddDictionaryWord 'Prysznic','Shower',1
EXEC AddDictionaryWord 'Prysznic','Dusche',2
EXEC AddDictionaryWord 'Prysznic','Douche',3
EXEC AddDictionaryWord 'Prysznic','Ducha',4
EXEC AddDictionaryWord 'Prysznic',N'シャワー',5
EXEC AddDictionaryWord 'Prysznic',N'Душ',6

--19
EXEC AddDictionaryWord 'Toaleta','Toilet',1
EXEC AddDictionaryWord 'Toaleta','Toilette',2
EXEC AddDictionaryWord 'Toaleta','Toilette',3
EXEC AddDictionaryWord 'Toaleta','Inodoro',4
EXEC AddDictionaryWord 'Toaleta',N'トイレ',5
EXEC AddDictionaryWord 'Toaleta',N'Туалет',6

--20
EXEC AddDictionaryWord 'WC','Toilets',1
EXEC AddDictionaryWord 'WC','Toiletten',2
EXEC AddDictionaryWord 'WC','Toilettes',3
EXEC AddDictionaryWord 'WC','Baños',4
EXEC AddDictionaryWord 'WC',N'トイレ',5
EXEC AddDictionaryWord 'WC',N'Туалеты',6

--21
EXEC AddDictionaryWord 'Papier toaletowy','Toilet paper',1
EXEC AddDictionaryWord 'Papier toaletowy','Toilettenpapier',2
EXEC AddDictionaryWord 'Papier toaletowy','Papier toilette',3
EXEC AddDictionaryWord 'Papier toaletowy','Papel higiénico',4
EXEC AddDictionaryWord 'Papier toaletowy',N'トイレットペーパー',5
EXEC AddDictionaryWord 'Papier toaletowy',N'Туалетная бумага',6

--22
EXEC AddDictionaryWord 'Biegunka','Diarrhea',1
EXEC AddDictionaryWord 'Biegunka','Durchfall',2
EXEC AddDictionaryWord 'Biegunka','Diarrhée',3
EXEC AddDictionaryWord 'Biegunka','Diarrea',4
EXEC AddDictionaryWord 'Biegunka',N'下痢',5
EXEC AddDictionaryWord 'Biegunka',N'Диарея',6

--23
EXEC AddDictionaryWord 'Sklep spożywczy','Grocery store',1
EXEC AddDictionaryWord 'Sklep spożywczy','Lebensmittelmarkt',2
EXEC AddDictionaryWord 'Sklep spożywczy','Épicerie',3
EXEC AddDictionaryWord 'Sklep spożywczy','Tienda de comestibles',4
EXEC AddDictionaryWord 'Sklep spożywczy',N'食料品店',5
EXEC AddDictionaryWord 'Sklep spożywczy',N'Продуктовый магазин',6

--24
EXEC AddDictionaryWord 'Woda do picia','Drinking water',1
EXEC AddDictionaryWord 'Woda do picia','Wasser trinken',2
EXEC AddDictionaryWord 'Woda do picia',"Boire de l'eau",3
EXEC AddDictionaryWord 'Woda do picia','Agua potable',4
EXEC AddDictionaryWord 'Woda do picia',N'水を飲んでいる',5
EXEC AddDictionaryWord 'Woda do picia',N'Питьевая вода',6

--25
EXEC AddDictionaryWord 'Pieniądze','Money',1
EXEC AddDictionaryWord 'Pieniądze','Geld',2
EXEC AddDictionaryWord 'Pieniądze','Argent',3
EXEC AddDictionaryWord 'Pieniądze','Dinero',4
EXEC AddDictionaryWord 'Pieniądze',N'お金',5
EXEC AddDictionaryWord 'Pieniądze',N'Деньги',6