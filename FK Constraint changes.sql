use TraveloDb
go

ALTER TABLE Alert
Drop Constraint FK_Country_Alert
GO

ALTER TABLE Country
Drop Constraint FK_Flag_Country
GO

ALTER TABLE Country
Drop Constraint FK_ServicePhone_Country
GO

ALTER TABLE DictionaryWord
Drop Constraint FK_Dictionary_DictionaryWord
GO

ALTER TABLE OweSinglePayment
Drop Constraint FK_Expense_OweSinglePayment
GO

ALTER TABLE Spot
Drop Constraint FK_Expense_Spot
GO

ALTER TABLE Spot
Drop Constraint FK_VisitDate_Spot
GO

ALTER TABLE Travel
Drop Constraint FK_Country_Travel
GO

ALTER TABLE Travel
Drop Constraint FK_User_Travel
GO

ALTER TABLE VisitDate
Drop Constraint FK_Travel_VisitDate
GO

Truncate table Expense
Truncate table OweSinglePayment
Truncate table SystemNotification
Truncate table Flag
Truncate table ServicePhone
Truncate table Country
Truncate table Alert
Truncate table "User"
Truncate table Travel
Truncate table VisitDate
Truncate table Spot


ALTER TABLE Country WITH CHECK ADD CONSTRAINT FK_ServicePhone_Country FOREIGN KEY (ServicePhoneId)
REFERENCES ServicePhone (ServicePhoneId) ON UPDATE NO ACTION ON DELETE NO ACTION

ALTER TABLE Country WITH CHECK ADD CONSTRAINT FK_Flag_Country FOREIGN KEY (FlagId)
REFERENCES Flag (FlagId) ON UPDATE NO ACTION ON DELETE NO ACTION

ALTER TABLE Alert WITH CHECK ADD CONSTRAINT FK_Country_Alert FOREIGN KEY (CountryId)
REFERENCES Country (CountryId) ON UPDATE NO ACTION ON DELETE NO ACTION

ALTER TABLE DictionaryWord WITH CHECK ADD CONSTRAINT FK_Dictionary_DictionaryWord FOREIGN KEY (DictionaryId)
REFERENCES Dictionary (DictionaryId) ON UPDATE NO ACTION ON DELETE NO ACTION

ALTER TABLE Travel WITH CHECK ADD CONSTRAINT FK_User_Travel FOREIGN KEY (UserId)
REFERENCES "User" (UserId) ON UPDATE NO ACTION ON DELETE NO ACTION

ALTER TABLE Travel WITH CHECK ADD CONSTRAINT FK_Country_Travel FOREIGN KEY (CountryId)
REFERENCES Country (CountryId) ON UPDATE NO ACTION ON DELETE NO ACTION

ALTER TABLE VisitDate WITH CHECK ADD CONSTRAINT FK_Travel_VisitDate FOREIGN KEY (TravelId)
REFERENCES Travel (TravelId) ON UPDATE NO ACTION ON DELETE NO ACTION

ALTER TABLE Spot WITH CHECK ADD CONSTRAINT FK_VisitDate_Spot FOREIGN KEY (VisitDateId)
REFERENCES VisitDate (VisitDateId) ON UPDATE NO ACTION ON DELETE NO ACTION

ALTER TABLE Spot WITH CHECK ADD CONSTRAINT FK_Expense_Spot FOREIGN KEY (ExpenseId)
REFERENCES Expense (ExpenseId) ON UPDATE NO ACTION ON DELETE NO ACTION

ALTER TABLE OweSinglePayment WITH CHECK ADD CONSTRAINT FK_Expense_OweSinglePayment FOREIGN KEY (ExpenseId)
REFERENCES Expense (ExpenseId) ON UPDATE NO ACTION ON DELETE NO ACTION

