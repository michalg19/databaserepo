/****** Object:  Database [TraveloDb]    Script Date: 04.02.2023 21:37:57 ******/
CREATE DATABASE [TraveloDb]  (EDITION = 'Standard', SERVICE_OBJECTIVE = 'S0', MAXSIZE = 250 GB) WITH CATALOG_COLLATION = SQL_Latin1_General_CP1_CI_AS;
GO
ALTER DATABASE [TraveloDb] SET COMPATIBILITY_LEVEL = 150
GO
ALTER DATABASE [TraveloDb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [TraveloDb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [TraveloDb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [TraveloDb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [TraveloDb] SET ARITHABORT OFF 
GO
ALTER DATABASE [TraveloDb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [TraveloDb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [TraveloDb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [TraveloDb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [TraveloDb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [TraveloDb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [TraveloDb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [TraveloDb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [TraveloDb] SET ALLOW_SNAPSHOT_ISOLATION ON 
GO
ALTER DATABASE [TraveloDb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [TraveloDb] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [TraveloDb] SET  MULTI_USER 
GO
ALTER DATABASE [TraveloDb] SET ENCRYPTION ON
GO
ALTER DATABASE [TraveloDb] SET QUERY_STORE = ON
GO
ALTER DATABASE [TraveloDb] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 100, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
/*** The scripts of database scoped configurations in Azure should be executed inside the target database connection. ***/
GO
-- ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 8;
GO
/****** Object:  User [Api]    Script Date: 04.02.2023 21:37:57 ******/
CREATE USER [Api] FOR LOGIN [TraveloUserApi] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  DatabaseRole [api_user]    Script Date: 04.02.2023 21:37:58 ******/
CREATE ROLE [api_user]
GO
sys.sp_addrolemember @rolename = N'api_user', @membername = N'Api'
GO
/****** Object:  Table [dbo].[Alert]    Script Date: 04.02.2023 21:37:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Alert](
	[AlertId] [int] IDENTITY(1,1) NOT NULL,
	[Content] [varchar](100) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ValidDate] [datetime] NULL,
	[CountryId] [int] NOT NULL,
 CONSTRAINT [PK_Alert] PRIMARY KEY CLUSTERED 
(
	[AlertId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 04.02.2023 21:37:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[CountryId] [int] IDENTITY(1,1) NOT NULL,
	[CodeABC] [varchar](3) NOT NULL,
	[CodeAB] [varchar](2) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Currency] [varchar](3) NOT NULL,
	[FlagId] [int] NOT NULL,
	[ServicePhoneId] [int] NOT NULL,
 CONSTRAINT [PK_Countries] PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dictionary]    Script Date: 04.02.2023 21:37:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dictionary](
	[DictionaryId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_Dictionary] PRIMARY KEY CLUSTERED 
(
	[DictionaryId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DictionaryWord]    Script Date: 04.02.2023 21:37:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DictionaryWord](
	[DictionaryWordId] [int] IDENTITY(1,1) NOT NULL,
	[Word] [nvarchar](255) NOT NULL,
	[WordTranslated] [nvarchar](255) NOT NULL,
	[DictionaryId] [int] NOT NULL,
 CONSTRAINT [PK_DictionaryWord] PRIMARY KEY CLUSTERED 
(
	[DictionaryWordId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Expense]    Script Date: 04.02.2023 21:37:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Expense](
	[ExpenseId] [int] IDENTITY(1,1) NOT NULL,
	[Cost] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_Expense] PRIMARY KEY CLUSTERED 
(
	[ExpenseId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Flag]    Script Date: 04.02.2023 21:37:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Flag](
	[FlagId] [int] IDENTITY(1,1) NOT NULL,
	[URL] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Flag] PRIMARY KEY CLUSTERED 
(
	[FlagId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OweSinglePayment]    Script Date: 04.02.2023 21:37:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OweSinglePayment](
	[OweSinglePaymentId] [int] IDENTITY(1,1) NOT NULL,
	[PersonName] [varchar](30) NOT NULL,
	[PaymentAmount] [decimal](18, 0) NOT NULL,
	[PaymentStatus] [bit] NOT NULL,
	[PaymentDate] [datetime] NOT NULL,
	[IsPayer] [bit] NOT NULL,
	[ExpenseId] [int] NOT NULL,
 CONSTRAINT [PK_OweSinglePayment] PRIMARY KEY CLUSTERED 
(
	[OweSinglePaymentId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ServicePhone]    Script Date: 04.02.2023 21:37:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServicePhone](
	[ServicePhoneId] [int] IDENTITY(1,1) NOT NULL,
	[Number] [varchar](12) NOT NULL,
 CONSTRAINT [PK_ServicePhone] PRIMARY KEY CLUSTERED 
(
	[ServicePhoneId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Spot]    Script Date: 04.02.2023 21:37:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Spot](
	[SpotId] [int] IDENTITY(1,1) NOT NULL,
	[Note] [varchar](255) NULL,
	[Adress] [varchar](255) NULL,
	[VisitDateId] [int] NOT NULL,
	[ExpenseId] [int] NOT NULL,
	[CoordinateX] [decimal](10, 7) NULL,
	[CoordinateY] [decimal](10, 7) NULL,
	[Name] [varchar](255) NULL,
 CONSTRAINT [PK_Spot] PRIMARY KEY CLUSTERED 
(
	[SpotId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SystemNotification]    Script Date: 04.02.2023 21:37:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SystemNotification](
	[SystemNotificationId] [int] IDENTITY(1,1) NOT NULL,
	[Content] [varchar](100) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ValidDate] [datetime] NOT NULL,
	[Title] [varchar](30) NOT NULL,
	[Type] [varchar](30) NOT NULL,
 CONSTRAINT [PK_SystemNotification] PRIMARY KEY CLUSTERED 
(
	[SystemNotificationId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Travel]    Script Date: 04.02.2023 21:37:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Travel](
	[TravelId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](30) NOT NULL,
	[Destination] [varchar](255) NOT NULL,
	[StartDate] [date] NOT NULL,
	[EndDate] [date] NOT NULL,
	[Note] [varchar](100) NULL,
	[PlannedBudget] [decimal](18, 0) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UserId] [int] NOT NULL,
	[CountryId] [int] NOT NULL,
	[HotelName] [varchar](50) NULL,
	[PickedCurrency] [varchar](3) NOT NULL,
	[HotelStreet] [varchar](255) NULL,
	[HotelBuildingNo] [int] NULL,
	[HotelFlatNo] [int] NULL,
	[HotelZipCode] [varchar](255) NULL,
	[HotelCity] [varchar](255) NULL,
 CONSTRAINT [PK_Travel] PRIMARY KEY CLUSTERED 
(
	[TravelId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 04.02.2023 21:37:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](30) NOT NULL,
	[Surname] [varchar](60) NOT NULL,
	[PhoneNumber] [varchar](14) NULL,
	[Email] [varchar](30) NOT NULL,
	[Password] [varchar](1024) NOT NULL,
	[PasswordDateUpdated] [datetime] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [Unique_User_Email] UNIQUE NONCLUSTERED 
(
	[Email] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VisitDate]    Script Date: 04.02.2023 21:37:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VisitDate](
	[VisitDateId] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Title] [varchar](30) NULL,
	[TravelId] [int] NOT NULL,
 CONSTRAINT [PK_VisitDate] PRIMARY KEY CLUSTERED 
(
	[VisitDateId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Alert]  WITH CHECK ADD  CONSTRAINT [FK_Country_Alert] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Country] ([CountryId])
GO
ALTER TABLE [dbo].[Alert] CHECK CONSTRAINT [FK_Country_Alert]
GO
ALTER TABLE [dbo].[Country]  WITH CHECK ADD  CONSTRAINT [FK_Flag_Country] FOREIGN KEY([FlagId])
REFERENCES [dbo].[Flag] ([FlagId])
GO
ALTER TABLE [dbo].[Country] CHECK CONSTRAINT [FK_Flag_Country]
GO
ALTER TABLE [dbo].[Country]  WITH CHECK ADD  CONSTRAINT [FK_ServicePhone_Country] FOREIGN KEY([ServicePhoneId])
REFERENCES [dbo].[ServicePhone] ([ServicePhoneId])
GO
ALTER TABLE [dbo].[Country] CHECK CONSTRAINT [FK_ServicePhone_Country]
GO
ALTER TABLE [dbo].[DictionaryWord]  WITH CHECK ADD  CONSTRAINT [FK_Dictionary_DictionaryWord] FOREIGN KEY([DictionaryId])
REFERENCES [dbo].[Dictionary] ([DictionaryId])
GO
ALTER TABLE [dbo].[DictionaryWord] CHECK CONSTRAINT [FK_Dictionary_DictionaryWord]
GO
ALTER TABLE [dbo].[OweSinglePayment]  WITH CHECK ADD  CONSTRAINT [FK_Expense_OweSinglePayment] FOREIGN KEY([ExpenseId])
REFERENCES [dbo].[Expense] ([ExpenseId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OweSinglePayment] CHECK CONSTRAINT [FK_Expense_OweSinglePayment]
GO
ALTER TABLE [dbo].[Spot]  WITH CHECK ADD  CONSTRAINT [FK_Expense_Spot] FOREIGN KEY([ExpenseId])
REFERENCES [dbo].[Expense] ([ExpenseId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Spot] CHECK CONSTRAINT [FK_Expense_Spot]
GO
ALTER TABLE [dbo].[Spot]  WITH CHECK ADD  CONSTRAINT [FK_VisitDate_Spot] FOREIGN KEY([VisitDateId])
REFERENCES [dbo].[VisitDate] ([VisitDateId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Spot] CHECK CONSTRAINT [FK_VisitDate_Spot]
GO
ALTER TABLE [dbo].[Travel]  WITH CHECK ADD  CONSTRAINT [FK_Country_Travel] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Country] ([CountryId])
GO
ALTER TABLE [dbo].[Travel] CHECK CONSTRAINT [FK_Country_Travel]
GO
ALTER TABLE [dbo].[Travel]  WITH CHECK ADD  CONSTRAINT [FK_User_Travel] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[Travel] CHECK CONSTRAINT [FK_User_Travel]
GO
ALTER TABLE [dbo].[VisitDate]  WITH CHECK ADD  CONSTRAINT [FK_Travel_VisitDate] FOREIGN KEY([TravelId])
REFERENCES [dbo].[Travel] ([TravelId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[VisitDate] CHECK CONSTRAINT [FK_Travel_VisitDate]
GO
ALTER TABLE [dbo].[Country]  WITH CHECK ADD  CONSTRAINT [CODEAB_CHECK] CHECK  ((NOT [CodeAB] like '%[^A-Z]%'))
GO
ALTER TABLE [dbo].[Country] CHECK CONSTRAINT [CODEAB_CHECK]
GO
ALTER TABLE [dbo].[Country]  WITH CHECK ADD  CONSTRAINT [CODEABC_CHECK] CHECK  ((NOT [CodeABC] like '%[^A-Z]%'))
GO
ALTER TABLE [dbo].[Country] CHECK CONSTRAINT [CODEABC_CHECK]
GO
ALTER TABLE [dbo].[OweSinglePayment]  WITH CHECK ADD  CONSTRAINT [PERSONNAME_CHECK] CHECK  ((NOT [PersonName] like '%[0-9]%'))
GO
ALTER TABLE [dbo].[OweSinglePayment] CHECK CONSTRAINT [PERSONNAME_CHECK]
GO
ALTER TABLE [dbo].[Travel]  WITH CHECK ADD  CONSTRAINT [DESTINATION_CHECK] CHECK  ((NOT [Destination] like '%[^A-Z]%'))
GO
ALTER TABLE [dbo].[Travel] CHECK CONSTRAINT [DESTINATION_CHECK]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [EMAIL_CKECH] CHECK  (([Email] like '%__@__%.__%'))
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [EMAIL_CKECH]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [PHONE_CKECH] CHECK  ((NOT [PhoneNumber] like '%[^0-9]%'))
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [PHONE_CKECH]
GO
/****** Object:  StoredProcedure [dbo].[AddAlert]    Script Date: 04.02.2023 21:37:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[AddAlert]
@Content VARCHAR(100),
@CountryId INT
AS
Set NOCOUNT ON;

BEGIN TRY
	BEGIN TRAN;
	DECLARE @CreatedDate DATETIME
	DECLARE @ValidDate DATETIME
	SET @CreatedDate = GETDATE();
	SET @ValidDate = DATEADD(day,7,@CreatedDate)-- o tydzień
	INSERT INTO Alert(Content,CreatedDate,ValidDate,CountryId)
	VALUES (@Content,@CreatedDate,@ValidDate,@CountryId);
	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('Country connected with Alert must exist');
END CATCH;

GO
/****** Object:  StoredProcedure [dbo].[AddCountry]    Script Date: 04.02.2023 21:37:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[AddCountry]
@CodeABC VARCHAR(3),
@CodeAB VARCHAR(2),
@Name VARCHAR(30),
@Currency VARCHAR(3),
@FlagId INT,
@ServicePhoneId INT
AS
Set NOCOUNT ON;

BEGIN TRY
	BEGIN TRAN;
	INSERT INTO Country(CodeABC,CodeAB,Name,Currency,FlagId,ServicePhoneId)
	VALUES (@CodeABC,@CodeAB,@Name,@Currency,@FlagId,@ServicePhoneId);
	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('Flag and Service Phone that are connected with Alert must exist');
END CATCH;

GO
/****** Object:  StoredProcedure [dbo].[AddDictionaryWord]    Script Date: 04.02.2023 21:37:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[AddDictionaryWord]
@Word NVarchar(255),
@WordTranslated NVarchar(255),
@DictionaryId INT
AS 
Set NOCOUNT ON;
BEGIN TRY
	BEGIN TRAN;
	Insert into DictionaryWord(Word,WordTranslated,DictionaryId)
	values(@Word,@WordTranslated,@DictionaryId);
	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('Dictionary connected with DictionaryWord must exist');
END CATCH;

GO
/****** Object:  StoredProcedure [dbo].[AddOweSinglePayment]    Script Date: 04.02.2023 21:37:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[AddOweSinglePayment]
@PersonName VARCHAR(30),
@PaymentAmount DECIMAL,
@PaymentStatus BIT,
@PaymentDate DATETIME,
@IsPayer BIT,
@ExpenseId INT
AS
Set NOCOUNT ON;

BEGIN TRY
	BEGIN TRAN;
	INSERT INTO OweSinglePayment(PersonName, PaymentAmount, PaymentStatus, PaymentDate, IsPayer, ExpenseId)
	VALUES (@PersonName,@PaymentAmount,@PaymentStatus,@PaymentDate,@IsPayer,@ExpenseId);
	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('Expense connected with OweSinglePayment must exist');
END CATCH;

GO
/****** Object:  StoredProcedure [dbo].[AddSpot]    Script Date: 04.02.2023 21:37:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[AddSpot]
@Note VARCHAR(255),
@Adress VARCHAR(255),
@VisitDateId INT,
@ExpenseId INT,
@CoordinateX DECIMAL(10,7),
@CoordinateY DECIMAL(10,7),
@Name VARCHAR(255)
AS
Set NOCOUNT ON;
BEGIN TRY
	BEGIN TRAN;
	Insert into Spot(Note,Adress,VisitDateId,ExpenseId,CoordinateX,CoordinateY,"Name")
	values(@Note,@Adress,@VisitDateId,@ExpenseId,@CoordinateX,@CoordinateY,@Name)
	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('VisitDate and Expense connected with Spot must exist');
END CATCH;

GO
/****** Object:  StoredProcedure [dbo].[AddTravel]    Script Date: 04.02.2023 21:37:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[AddTravel]
@Name as Varchar(30),
@Destination as Varchar(255),
@StartDate as Date,
@EndDate as Date,
@Note as Varchar(100),
@PlannedBudget as decimal,
@UserId as int,
@CountryId as int,
@HotelName as Varchar(50),
@PickedCurrency as Varchar(3),
@HotelStreet Varchar(255),
@HotelBuildingNo int,
@HotelFlatNo int,
@HotelZipCode Varchar(255),
@HotelCity Varchar(255)
AS
Set NOCOUNT ON;
BEGIN TRY
	BEGIN TRAN;
	DECLARE @actDate DateTime;
	set @actDate = Convert(datetime, GETDATE())
	Insert into Travel(Name,Destination,StartDate,EndDate,Note,PlannedBudget,CreatedDate,UserId,CountryId,HotelName,PickedCurrency,HotelStreet,HotelBuildingNo,HotelFlatNo,HotelZipCode,HotelCity)
	values(@Name,@Destination,@StartDate,@EndDate,@Note,@PlannedBudget,@actDate,@UserId,@CountryId,@HotelName,@PickedCurrency,@HotelStreet,@HotelBuildingNo,@HotelFlatNo,@HotelZipCode,@HotelCity)
	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('User and Country connected with Travel must exist');
END CATCH;

GO
/****** Object:  StoredProcedure [dbo].[AddVisitDate]    Script Date: 04.02.2023 21:37:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[AddVisitDate]
@Date DateTime,
@Title VARCHAR(30),
@TravelId INT
AS
Set NOCOUNT ON;
BEGIN TRY
	BEGIN TRAN;
	Insert into VisitDate(Date,Title,TravelId)
	values(@Date,@Title,@TravelId)
	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('Travel connected with VisitDate must exist');
END CATCH;

GO
/****** Object:  StoredProcedure [dbo].[CheckBudget]    Script Date: 04.02.2023 21:37:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[CheckBudget]
@TravelId INT,
@Result float output
AS
BEGIN
Set NOCOUNT ON;
DECLARE @CostSum float;
DECLARE @OwePaymentSum float;

CREATE TABLE #Expenses(
	ExpenseId int
	)

Insert into #Expenses(ExpenseId)
	select s.ExpenseId from Spot s where VisitDateId IN 
	(Select VisitDateId From VisitDate v where TravelId = @TravelId)

	Select @OwePaymentSum = SUM(PaymentAmount) from OweSinglePayment ow
	Where ExpenseId IN 
	(SELECT t.ExpenseId FROM #Expenses t)

Select @CostSum=SUM(Cost) from Expense e
Where e.ExpenseId IN(
Select s.ExpenseId from Spot s
Where  VisitDateId IN 
(Select VisitDateId From VisitDate vd 
Where vd.TravelId like @TravelId));

Select @Result = coalesce(@CostSum + @OwePaymentSum, @CostSum, @OwePaymentSum, 0);-- @CostSum + @OwePaymentSum;

Select 'Result'= @Result;

Return;
END;

GO
/****** Object:  StoredProcedure [dbo].[FindCountry]    Script Date: 04.02.2023 21:37:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[FindCountry]
@Phrase as Varchar(255)
AS
BEGIN
Set NOCOUNT ON;
DECLARE @arg Varchar(255)
set @arg = '%'+@Phrase+'%'
Select 'CountryId'=CountryId,'CodeABC'=CodeABC,'CodeAB'=CodeAB,'Name'=LOWER (Name), 'Currency'=Currency, 'FlagId'=FlagId, 'ServicePhoneId'=ServicePhoneId from Country
Where Name like LOWER(@arg)
END;

GO
/****** Object:  StoredProcedure [dbo].[RecoveryPasswordChange]    Script Date: 04.02.2023 21:37:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[RecoveryPasswordChange]
@Email as Varchar(30),
@NEWPassword as Varchar(1024)
AS
BEGIN
Set NOCOUNT ON;
DECLARE @actDate DateTime;
set @actDate = Convert(datetime, GETDATE()) 
Update "User"
set PasswordDateUpdated = @actDate, Password = @NEWPassword
Where Email = @Email
END;

GO
/****** Object:  StoredProcedure [dbo].[RemoveTravel]    Script Date: 04.02.2023 21:37:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[RemoveTravel]
@TravelId INT
AS
Set NOCOUNT ON;
BEGIN TRY
	BEGIN TRAN;

	CREATE TABLE #TempExpenses(
	ExpenseId int
	)

	Insert into #TempExpenses(ExpenseId)
	select s.ExpenseId from Spot s where VisitDateId IN 
	(Select VisitDateId From VisitDate v where TravelId = @TravelId)

	DELETE FROM Travel WHERE TravelId=@TravelId;
	DELETE FROM Expense WHERE ExpenseId IN
	(SELECT t.ExpenseId FROM #TempExpenses t);

	COMMIT;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK;
	PRINT('Could not delete all travel info');
END CATCH;

GO
ALTER DATABASE [TraveloDb] SET  READ_WRITE 
GO
